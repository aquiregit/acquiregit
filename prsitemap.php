<?php
 include 'connect.php';
  
   $base_url = "https://www.acquiremarketresearch.com/";

$xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       /*$url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);  */
/*PR.php*/

$qu="SELECT * FROM press_releases";
$result=$con->query($qu);

     if ($result->num_rows > 0) { 
                  
            while($row11 = $result->fetch_assoc()) {
              
   
                $pr1=$row11['pr_id'];
                parse_str('prid='.$pr1.'');
                
                $lprurl=utf8_encode(strtolower((str_replace(" ", "-",$row11["pr_title"] ))));
                $lprurl1= str_replace("(", "-",$lprurl);
                    $lprurl1= str_replace(")", "-",$lprurl);
                    $lprurl1= str_replace("?", "-",$lprurl1);
                    $lprurl1= str_replace("@", "-",$lprurl1);
                    $lprurl1= str_replace("!", "-",$lprurl1);
                    $lprurl1= str_replace("#", "-",$lprurl1);
                    $lprurl1= str_replace("$", "-",$lprurl1);
                    $lprurl1= str_replace("%", "-",$lprurl1);
                    $lprurl1= str_replace("^", "-",$lprurl1);
                    $lprurl1= str_replace("*", "-",$lprurl1);
                    $lprurl1= str_replace("'", "-",$lprurl1);
                    $lprurl1= str_replace("+", "-",$lprurl1);

                     $lprurl1=str_replace("&","and",$lprurl1);
                $lprurl1=str_replace("---","-",$lprurl1);
                 $lprurl1=str_replace("--","-",$lprurl1);
                //$finalStrlpr= "press-release/";

                $pr=$xml->createElement("url");
                $urlset->appendChild($pr);
                $loc=$xml->createElement("loc",$base_url."press-release/".$lprurl1."/".$row11['pr_id']);
                $pr->appendChild($loc);
   
                $lastmod=$xml->createElement("lastmod",$row11['published_date']);
                $pr->appendChild($lastmod);
                
                $prt=$xml->createElement("priority","0.9");
                $pr->appendChild($prt);
                
                $changefreq=$xml->createElement("changefreq","monthly");
                $pr->appendChild($changefreq);
                 
            }
           
                $xml->save("press-releases.xml");

        }  
 
function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
       }
?>