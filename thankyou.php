<?php
include 'header_file.php';
?>
		<?php
			error_reporting(E_ALL);
			require('ccavenue/Crypto.php');
			require("ccavenue/credential.php");
				
			$encResponse	= $_POST["encResp"];					
			$rcvdString		= decrypt($encResponse,WORKING_KEY);	
			$order_status	= "";
			$decryptValues	= explode('&', $rcvdString);
			$dataSize		= sizeof($decryptValues);

			$arrResponse = [];
			for($i = 0; $i < $dataSize; $i++) {
				$informations = [];
				$information  = explode('=',$decryptValues[$i]);
				$informations[$information[0]] = $information[1];
				$arrResponse[] = $informations;
			}

			function getResponseValueByKey($responseData, $find) {
				foreach ($responseData as $data) {
					if(isset($data[$find])) return $data[$find];
				}
				return false;
			}

// print_r($arrResponse); exit;
			$order_status 	= getResponseValueByKey($arrResponse, 'order_status');
			$cust_id 		= getResponseValueByKey($arrResponse, 'merchant_param1');

			if($order_status==="Success") {
				include 'emailFunctions.php';
				echo "<div class='bg-success status-msg'>";
				echo "Thank you for shopping with us. Your transaction #".$information[0]." is successful. We will be shipping your order to you soon.";
				echo "</div>";

				$sql		= "SELECT * FROM `customer` WHERE `customer_id` =".$cust_id;
				$result		= mysqli_query($con,$sql);
		        $customer 	= mysqli_fetch_assoc($result);

				$sql = "UPDATE `customer` SET `payment_status`= 'Success' WHERE `customer_id`= " .$cust_id;
				if(!mysqli_query($con,$sql)) {
					echo "<br>Error: Something went wrong.." . mysqli_error($con);
					exit;
				}

				$data['order_id'] 	= $customer['cust_report_id'];
				$data['prod_id'] 	= explode('-',$customer['cust_report_id'])[2];
				$data['prod_name'] 	= $customer['cust_report'];
				$data['amount'] 	= $customer['cust_price'];
				$data['name'] 		= $customer['cust_fullname'];
				$data['card_name'] 	= getResponseValueByKey($arrResponse, 'card_name'); //"xxx xxx 000";
				$data['company'] 	= $customer['cust_company'];

				$to = $from = 'sales@amr.com';
				$subject='Your order#'.$customer['cust_report_id'].' on http://amr.com is successful.';
				$htmlMail = getCustomerEmailTemplate($data);

				$response = json_decode( sendMail($customer['cust_email'], $subject, $from, $htmlMail), true );
				if($response['message'] == 'error') {
					echo "<br>Error 1: Failed to send email. " . $response['errors'][0]; 
					exit;
				}
				
				$htmlMail = getSelfEmailTemplate($data);
				$response = json_decode( sendMail($to, $subject, $from, $htmlMail), true );
				if($response['message'] == 'error') {
					echo "<br>Error 2: Failed to send email. " . $response['errors'][0]; 
					exit;
				}
			} else if($order_status==="Aborted") {
				echo "<div class='bg-warning status-msg'>";
				echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
				echo "</div>";
				
				$sql = "UPDATE `customer` SET `payment_status`= 'Aborted' WHERE `customer_id`= " .$cust_id;
				if(!mysqli_query($con,$sql)) {
					echo "<br>Error: Something went wrong.." . mysqli_error($con);
					exit;
				}
			} else if($order_status==="Failure") {
				echo "<div class='bg-danger status-msg'>";
				echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
				echo "</div>";
				
				$sql = "UPDATE `customer` SET `payment_status`= 'Failed' WHERE `customer_id`= " .$cust_id;
				if(!mysqli_query($con,$sql)) {
					echo "<br>Error: Something went wrong.." . mysqli_error($con);
					exit;
				}
			} else {
				echo "<br>Security Error. Illegal access detected";
			}
		?>
<?php include 'footer_file.php';?>