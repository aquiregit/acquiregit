<?php
$pagetitle = "AMR Reports | Acquire Market Research";
$desc      = "Reserach Reports | Acquire Market Research";
$key       = "AMR report, research report, Acquire Market Research Report , consulting services";
?>
<?php
error_reporting(E_WARNING);
include "header_file.php";
$data = $_SERVER['REQUEST_URI'];

$page1 = explode('/', $data);
$page1 = $page1[2];

$limit = 15;
if (isset($page1)) {
    $page = $page1;
} else {
    $page = 1;
}
;
$start_from = ($page - 1) * $limit;
?>
   <style type="text/css">
       #con1 span{font-size:30px;color:#0077b5;font-family:'Lato Black';padding-top:20px}#div_rp{padding-left:80px}@media screen and (min-width:980px) and (max-width:1024px){#div_rp{margin-left:0}}@media screen and (min-width:760px) and (max-width:980px){#div_rp{margin-left:-65px}}@media screen and (min-width:350px) and (max-width:760px){#div_rp{margin-left:-65px}}@media screen and (max-width:350px){#div_rp{margin-left:-65px}}#div_rp button{background-color:#0077b5;color:#fff;font-size:15px}#div_rp .report-panel{border-width:.5px;border-style:solid;border-color:#F2F1F1;padding-bottom:5px}#div_rp .report-panel:nth-child(even){background-color:#FFF}#div_rp .report-panel:nth-child(odd){background-color:#e0e0e0}#div_rp .published-report{line-height:normal!important;color:#063772;overflow:hidden;margin:0;font-size:14px!important;display:block;font-weight:600!important;padding-left:15px;padding-top:20px}#div_rp .report-date{color:#E6751E;font-size:14px;margin-left:45px;display:inline-block;padding-bottom:10px;padding-top:10px}#div_rp .report-pages,#div_rp .report-summary{font-size:14px;margin-left:25px;padding-bottom:10px;display:inline-block}#div_rp .report-pages{color:#E6751E}#div_rp .report-summary{color:#000;padding-right:5px}@media screen and (min-width:760px) and (max-width:980px){#div_rp .report-date{margin-left:25px}}@media screen and (min-width:350px) and (max-width:760px){#div_rp .report-date{margin-left:25px}}@media screen and (max-width:350px){#div_rp .report-date{margin-left:25px}}.pagination{display:inline-block;padding-left:100px}.pagination a{color:#000;float:left;padding:8px 16px;text-decoration:none;transition:background-color .3s;border:1px solid #ddd}.pagination li.active{background-color:#0077b5;color:#fff}.pagination li:hover:not(.active){background-color:#ddd}.w3-card,.w3-card-2{box-shadow:0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12)}.w3-card-4,.w3-hover-shadow:hover{box-shadow:0 4px 10px 0 rgba(0,0,0,.2),0 4px 20px 0 rgba(0,0,0,.19)}.w3-container{padding:.01em 16px}#all_report_card{width:70%;background-color:#e0e0e0}#all_report_card h3{border-bottom:1px solid gray;padding-bottom:10px}#all_report_card a{font-size:1em;color:#0077b5;text-decoration:none}#all_report_card img{height:150px}#div_rp #icon{font-size:20px;color:#fff;background-color:#7fba00}#con2{padding-left:20px}#all_report_card #icon2{font-size:15px;}#enq_form{width:70%}@media screen and (min-width:980px) and (max-width:1024px){#all_report_card,#enq_form{margin-left:50px}}#enq_form h4{border-bottom:1px solid gray;padding-bottom:7px;padding-top:5px}#enq_form p{font-size:12px}* Desktop */ @media screen and (min-width:980px) and (max-width:1024px){#pr_form input{width:137px}}@media screen and (min-width:760px) and (max-width:980px){#all_report_card,#enq_form{margin-left:50px}#pr_form input{width:137px}}@media screen and (min-width:350px) and (max-width:760px){#all_report_card,#enq_form{margin-left:25px;width:80%}#pr_form input{width:137px}}@media screen and (max-width:350px){#all_report_card,#enq_form{margin-left:25px;width:80%}#pr_form input{width:137px}}#error_ph,#error_name{color: Red; display: none;}#pr_form label{color:#0077b5; font-size: 16px;}
    </style>
<div class="container-fluid">
    <div class="container">
    </div>
    <br><br>
    <div class="container col-md-12">
        <p class="center"><span id="con1"><?php
echo $chk;
?> </span></p>
    </div>
    <div class="col-md-8" id="div_rp">
        <div class="more-report">
           <a href="<?php
echo $base_url;
?>industries"><button class="btn btn-info"><i class="fas fa-angle-double-left"></i>&nbsp;View All Categories&nbsp;</button></a>
        </div>
        <br>
        <?php
            $result = mysqli_query($con, "SELECT * from categories");
            while( $category = mysqli_fetch_assoc($result) ) {
                $categories[$category['cat_id']] = $category['fa_fa_font'];
            }
            
$sql    = "SELECT * FROM reports ORDER BY report_id DESC LIMIT $start_from, $limit ";
$result4 = $con4->query($sql);
$result = $con->query($sql);
$result2 = $con2->query($sql);
$result3 = $con3->query($sql);
if ($result4->num_rows > 0) {
    while ($row = $result4->fetch_assoc()) {
        
?>  
        <div class="report-panel">
            <div class="published-report">
        <?php
        $finalStrrp  = "industry-reports/";
        $urltitlerp  = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
        $urltitlerp  = str_replace("(", "-", $urltitlerp);
        $urltitlerp  = str_replace(")", "-", $urltitlerp);
        $urltitlerp  = str_replace("?", "-", $urltitlerp);
        $urltitlerp  = str_replace("&", "and", $urltitlerp);
        $urltitlerp  = str_replace("!", "-", $urltitlerp);
        $urltitlerp  = str_replace("@", "-", $urltitlerp);
        $urltitlerp  = str_replace("#", "-", $urltitlerp);
        $urltitlerp  = str_replace("$", "-", $urltitlerp);
        $urltitlerp  = str_replace("%", "-", $urltitlerp);
        $urltitlerp  = str_replace("^", "-", $urltitlerp);
        $urltitlerp  = str_replace("*", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("+", "-", $urltitlerp);
        $urltitlerp  = str_replace("/", "-", $urltitlerp);
        $urltitlerp  = str_replace(":", "-", $urltitlerp);
        $urltitlerp  = str_replace(".", "-", $urltitlerp);
        $urltitlerp  = str_replace("<", "-", $urltitlerp);
        $urltitlerp  = str_replace(">", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("|", "-", $urltitlerp);
        $urltitlerp  = str_replace("]", "-", $urltitlerp);
        $urltitlerp  = str_replace("[", "-", $urltitlerp);
        $urltitlerp  = str_replace("}", "-", $urltitlerp);
        $urltitlerp  = str_replace("{", "-", $urltitlerp);
        $urltitlerp  = str_replace(";", "-", $urltitlerp);
        $urltitlerp  = str_replace("_", "-", $urltitlerp);
        $urltitlerp  = str_replace("_x000D_", "-", $urltitlerp);
        $urltitlerp  = str_replace("--", "-", $urltitlerp);
        $urltitlerp1 = str_replace("---", "-", $urltitlerp);
        
        if(isset($categories[$row['cat_id']])) {
            $icon = $categories[$row['cat_id']];
        }
?>  
                <i class="fa <?=$icon;?>" id="icon"></i>  
                <a href="<?php
        echo $base_url;
?><?php
        echo $finalStrrp;
?><?php
        echo $urltitlerp1;
?>/<?php
        echo $row['report_id'];
?>">
                <?php
        $tempar = str_replace("?", " ", $row['report_title']);
        echo str_replace("_x000D_", " ", $tempar);
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
        echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-summary">
                <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
            </div>
            <br>
        </div>
         <?php
    }
    
}

?>

<!--  Next database reports -->

<?php
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        
?>  
        <div class="report-panel">
            <div class="published-report">
        <?php
        $finalStrrp  = "industry-reports/";
        $urltitlerp  = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
        $urltitlerp  = str_replace("(", "-", $urltitlerp);
        $urltitlerp  = str_replace(")", "-", $urltitlerp);
        $urltitlerp  = str_replace("?", "-", $urltitlerp);
        $urltitlerp  = str_replace("&", "and", $urltitlerp);
        $urltitlerp  = str_replace("!", "-", $urltitlerp);
        $urltitlerp  = str_replace("@", "-", $urltitlerp);
        $urltitlerp  = str_replace("#", "-", $urltitlerp);
        $urltitlerp  = str_replace("$", "-", $urltitlerp);
        $urltitlerp  = str_replace("%", "-", $urltitlerp);
        $urltitlerp  = str_replace("^", "-", $urltitlerp);
        $urltitlerp  = str_replace("*", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("+", "-", $urltitlerp);
        $urltitlerp  = str_replace("/", "-", $urltitlerp);
        $urltitlerp  = str_replace(":", "-", $urltitlerp);
        $urltitlerp  = str_replace(".", "-", $urltitlerp);
        $urltitlerp  = str_replace("<", "-", $urltitlerp);
        $urltitlerp  = str_replace(">", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("|", "-", $urltitlerp);
        $urltitlerp  = str_replace("]", "-", $urltitlerp);
        $urltitlerp  = str_replace("[", "-", $urltitlerp);
        $urltitlerp  = str_replace("}", "-", $urltitlerp);
        $urltitlerp  = str_replace("{", "-", $urltitlerp);
        $urltitlerp  = str_replace(";", "-", $urltitlerp);
        $urltitlerp  = str_replace("_", "-", $urltitlerp);
        $urltitlerp  = str_replace("_x000D_", "-", $urltitlerp);
        $urltitlerp  = str_replace("--", "-", $urltitlerp);
        $urltitlerp1 = str_replace("---", "-", $urltitlerp);
        
        if(isset($categories[$row['cat_id']])) {
            $icon = $categories[$row['cat_id']];
        }
?>  
                <i class="fa <?=$icon;?>" id="icon"></i>  
                <a href="<?php
        echo $base_url;
?><?php
        echo $finalStrrp;
?><?php
        echo $urltitlerp1;
?>/<?php
        echo $row['report_id'];
?>">
                <?php
        $tempar = str_replace("?", " ", $row['report_title']);
        echo str_replace("_x000D_", " ", $tempar);
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
        echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-summary">
                <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
            </div>
            <br>
        </div>
         <?php
    }
    
}

?>

 <!-- -->
 <?php
if ($result2->num_rows > 0) {
    while ($row = $result2->fetch_assoc()) {
        
?>  
        <div class="report-panel">
            <div class="published-report">
        <?php
        $finalStrrp  = "industry-reports/";
        $urltitlerp  = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
        $urltitlerp  = str_replace("(", "-", $urltitlerp);
        $urltitlerp  = str_replace(")", "-", $urltitlerp);
        $urltitlerp  = str_replace("?", "-", $urltitlerp);
        $urltitlerp  = str_replace("&", "and", $urltitlerp);
        $urltitlerp  = str_replace("!", "-", $urltitlerp);
        $urltitlerp  = str_replace("@", "-", $urltitlerp);
        $urltitlerp  = str_replace("#", "-", $urltitlerp);
        $urltitlerp  = str_replace("$", "-", $urltitlerp);
        $urltitlerp  = str_replace("%", "-", $urltitlerp);
        $urltitlerp  = str_replace("^", "-", $urltitlerp);
        $urltitlerp  = str_replace("*", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("+", "-", $urltitlerp);
        $urltitlerp  = str_replace("/", "-", $urltitlerp);
        $urltitlerp  = str_replace(":", "-", $urltitlerp);
        $urltitlerp  = str_replace(".", "-", $urltitlerp);
        $urltitlerp  = str_replace("<", "-", $urltitlerp);
        $urltitlerp  = str_replace(">", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("|", "-", $urltitlerp);
        $urltitlerp  = str_replace("]", "-", $urltitlerp);
        $urltitlerp  = str_replace("[", "-", $urltitlerp);
        $urltitlerp  = str_replace("}", "-", $urltitlerp);
        $urltitlerp  = str_replace("{", "-", $urltitlerp);
        $urltitlerp  = str_replace(";", "-", $urltitlerp);
        $urltitlerp  = str_replace("_", "-", $urltitlerp);
        $urltitlerp  = str_replace("_x000D_", "-", $urltitlerp);
        $urltitlerp  = str_replace("--", "-", $urltitlerp);
        $urltitlerp1 = str_replace("---", "-", $urltitlerp);
        
        if(isset($categories[$row['cat_id']])) {
            $icon = $categories[$row['cat_id']];
        }
?>  
                <i class="fa <?=$icon;?>" id="icon"></i>  
                <a href="<?php
        echo $base_url;
?><?php
        echo $finalStrrp;
?><?php
        echo $urltitlerp1;
?>/<?php
        echo $row['report_id'];
?>">
                <?php
        $tempar = str_replace("?", " ", $row['report_title']);
        echo str_replace("_x000D_", " ", $tempar);
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
        echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-summary">
                <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
            </div>
            <br>
        </div>
         <?php
    }
    
}

?>

<!-- -->

<?php
if ($result3->num_rows > 0) {
    while ($row = $result3->fetch_assoc()) {
        
?>  
        <div class="report-panel">
            <div class="published-report">
        <?php
        $finalStrrp  = "industry-reports/";
        $urltitlerp  = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
        $urltitlerp  = str_replace("(", "-", $urltitlerp);
        $urltitlerp  = str_replace(")", "-", $urltitlerp);
        $urltitlerp  = str_replace("?", "-", $urltitlerp);
        $urltitlerp  = str_replace("&", "and", $urltitlerp);
        $urltitlerp  = str_replace("!", "-", $urltitlerp);
        $urltitlerp  = str_replace("@", "-", $urltitlerp);
        $urltitlerp  = str_replace("#", "-", $urltitlerp);
        $urltitlerp  = str_replace("$", "-", $urltitlerp);
        $urltitlerp  = str_replace("%", "-", $urltitlerp);
        $urltitlerp  = str_replace("^", "-", $urltitlerp);
        $urltitlerp  = str_replace("*", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("+", "-", $urltitlerp);
        $urltitlerp  = str_replace("/", "-", $urltitlerp);
        $urltitlerp  = str_replace(":", "-", $urltitlerp);
        $urltitlerp  = str_replace(".", "-", $urltitlerp);
        $urltitlerp  = str_replace("<", "-", $urltitlerp);
        $urltitlerp  = str_replace(">", "-", $urltitlerp);
        $urltitlerp  = str_replace("'", "-", $urltitlerp);
        $urltitlerp  = str_replace("|", "-", $urltitlerp);
        $urltitlerp  = str_replace("]", "-", $urltitlerp);
        $urltitlerp  = str_replace("[", "-", $urltitlerp);
        $urltitlerp  = str_replace("}", "-", $urltitlerp);
        $urltitlerp  = str_replace("{", "-", $urltitlerp);
        $urltitlerp  = str_replace(";", "-", $urltitlerp);
        $urltitlerp  = str_replace("_", "-", $urltitlerp);
        $urltitlerp  = str_replace("_x000D_", "-", $urltitlerp);
        $urltitlerp  = str_replace("--", "-", $urltitlerp);
        $urltitlerp1 = str_replace("---", "-", $urltitlerp);
        
        if(isset($categories[$row['cat_id']])) {
            $icon = $categories[$row['cat_id']];
        }
?>  
                <i class="fa <?=$icon;?>" id="icon"></i>  
                <a href="<?php
        echo $base_url;
?><?php
        echo $finalStrrp;
?><?php
        echo $urltitlerp1;
?>/<?php
        echo $row['report_id'];
?>">
                <?php
        $tempar = str_replace("?", " ", $row['report_title']);
        echo str_replace("_x000D_", " ", $tempar);
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
        echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-summary">
                <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
            </div>
            <br>
        </div>
         <?php
    }
    
}

?>

<!-- -->
        <?php
$sql = "SELECT COUNT(*) FROM reports ";
$rs_result4     = mysqli_query($con4, $sql);
$rs_result      = mysqli_query($con, $sql);
$rs_result2     = mysqli_query($con2, $sql);
$rs_result3     = mysqli_query($con3, $sql);

if($rs_result4 > 0) {
        $row = mysqli_fetch_row($rs_result4);
        $total_records = $row[0];
}

if($rs_result > 0) {
        $row = mysqli_fetch_row($rs_result);
        $total_records = $total_records + $row[0];
}

if($rs_result2 > 0) {
        $row = mysqli_fetch_row($rs_result2);
        $total_records = $total_records + $row[0];
}

if($rs_result3 > 0) {
        $row = mysqli_fetch_row($rs_result3);
        $total_records = $total_records + $row[0];
}


$total_pages   = ceil($total_records / $limit);
$chk           = rtrim(str_replace(" ", "_", $chk));
$chk           = rtrim(str_replace("&", "And", $chk));
$current_page  = $page;
$str1          = "reports?";
$finalStr1     = str_replace('?', '/', $str1);
if ($page == 1) {
    $pagLink = "<div ><ul  class='pagination' style='display:inline' id='pg'><li style='display:inline' class='disabled'><a>Previous</a></li>";
} else {
    $pagLink = "<div ><ul  class='pagination' style='display:inline' id='pg'><li style='display:inline'><a href='$base_url$finalStr1" . ($page - 1) . "'>Previous</a></li>";
}

for ($i = max(1, $page - 4); $i <= min($page + 4, $total_pages); $i++) {
    
    $active = '';
    if (isset($page1) && $i == $page1) {
        $active = 'class="active"';
    }
    $pagLink .= "<li $active style='display:inline' ><a href='$base_url$finalStr1" . $i . "' >" . $i . "</a></li>";
    
}
;
if ($page < $total_pages) {
    echo $pagLink . "<li style='display:inline'><a href='$base_url$finalStr1" . ($page + 1) . "'>NEXT</a></li> </ul></div>";
}

else {
    if ($page > $total_pages) {
        echo '<script type="text/javascript"> window.location.href="https://www.acquiremarketresearch.com/" </script>';
    }
    echo $pagLink . "<li style='display:inline' class='disabled'><a>NEXT</a></li> </ul></div>";
}
?><br><br><br>
        <div><b>
            <?php
echo "Page No : " . $current_page . " / " . $total_pages;
?></b>
        </div>
    </div>
    <div class="col-md-4"  id="con2">
        <br><br><br>
        <div class="w3-card-4" id="all_report_card">
            <header class="w3-container w3">
            <h3><p class="center"><b>Happy To Assist You 24/7</b></p></h3>
            </header>
            <div class="w3-container center">
                
                    <img src="<?php
echo $base_url;
?>images/success-support.png" alt="Contact us" style="" class="img-responsive center"/>
                    <b>
                       <!-- <i class="fas fa-envelope" id="icon2"></i><a href="mailto:sales@acquiremarketresearch.com"></a>&nbsp;&nbsp;
                          <script type="text/javascript">
emailE=('sales@' + 'acquiremarketresearch.com')
document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
 
</script> -->            <i class="fas fa-envelope"></i>&nbsp;&nbsp;<strong id="icon2">sales@acquiremarketresearch.com</strong>
                        <div><br></div> 
                        <i class="glyphicon glyphicon-phone-alt"></i><!--a href="skype:+1-800-663-5579 call"-->&nbsp;<b id="icon2">+1-800-663-5579</b><!--/a-->
                    </b>
               
            </div>
        </div>
        <style>
            .quick{font-size:18px;}
        </style>
        <br><br>
        <div class="w3-card-4" id="enq_form">
            <header class="w3-container w3">
                <h4>
                    <p class="center">
                        <b>
                                    <b class="quick">To Make a quick Enquiry</b>
                            <p class="center">(By filling up the form below)</p>
                        </b>
                    </p>
                </h4>
            </header>
            <form action="https://www.acquiremarketresearch.com/market-analysis-thank-you/" method="POST">
                <div class="form-group" id="pr_form">
                    <div class="w3-container">
                        <label>Full Name*</label><br>
                        <input type="text" id="fname" name="firstname" placeholder="Full Name" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;" size="37"  required/><span id="error_name">* Enter Valid Name</span>
                        <br><br><label><b>Email*</b></label><br>
                        <input type="email" name="bemail" placeholder="Email" size="37" required />
                        <!--<br><br><label><b>Company Name*</b></label><br>
                        <input type="text" name="cname" placeholder="Company Name" size="31" required />
                        <br><br><label><b>Designation*</b></label><br>
                        <input type="text" name="desig" placeholder="Designation" size="31" required />
                    -->    <br><br><label><b>Phone*</b></label><br><input type="text" id="ph" name="ph" placeholder="Phone number"
                            onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="21" size="37" required /><span id="error_ph">* Input digits (0 - 9)</span>
                        <br><br><label><b>Research Requirements</b></label><br>
                        <textarea  col="3" rows="3" name="qur" placeholder="Type your query here" ></textarea>
                        <br><br>
                       <button type="submit" class="btn btn-primary center" name="submit">Submit</button>
                        <br><br>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br> 
<?php
function limit_words($string, $word_limit)
{
    $words = str_replace(",", " ", $string);
    $words = explode(" ", $words);
    
    return implode(" ", array_splice($words, 0, $word_limit));
}

?>
<script language='JavaScript' type='text/javascript'>
    var specialKeys = new Array();
    specialKeys.push(8);
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_ph").style.display = ret ? "none" : "inline";
        return ret;
    }
    
    function IsAlpha(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode > 64 && keyCode < 91) ||(keyCode==32)|| (keyCode > 96 && keyCode < 123) ||specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_name").style.display = ret ? "none" : "inline";
        return ret;
    }
    
</script>
<?php
include 'footer_file.php';
?>