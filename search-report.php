<?php
$pagetitle = "AMR Search Reports | Acquire Market Research";
$desc      = "Reserach Reports | Acquire Market Research";
$key       = "AMR report, research report, Acquire Market Research Report , consulting services";
?>
<?php
/*ini_set("display_errors", "1");*/
error_reporting(E_WARNING);
$url   = rtrim(str_replace("_", " ", $_GET['search']));
$title = $url;
$menu  = "REPORT";
include 'header_file.php';
$limit = 15;
if (isset($_GET['page']) & !empty($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$start_from = ($page - 1) * $limit;
?>
      <style type="text/css">
          #con1 #span1{font-size:18px;color:grey;font-family:Lato;padding-left:5px}#con1 #span2{font-size:18px;color:#0077b5;font-family:'Lato heavy'}#menu1 .report-panel{border-width:.5px;border-style:solid;border-color:#F2F1F1;padding-bottom:5px}#menu1 .report-panel:nth-child(odd){background-color:#FFF}#menu1 .report-panel:nth-child(even){background-color:#F5F5F5}#menu1 .published-report{line-height:normal!important;color:#063772;overflow:hidden;margin:0;font-size:14px!important;display:block;font-weight:600!important;padding-left:15px;padding-top:20px}#menu1 .report-date{color:#E6751E;font-size:14px;margin-left:45px;display:inline-block;padding-top:10px}#menu1 .report-industry,#menu1 .report-pages,#menu1 .report-summary{font-size:14px;margin-left:25px;display:inline-block}#menu1 .report-pages{color:#E6751E}#menu1 .report-industry{color:#E6751E;padding-bottom:10px}#menu1 .report-summary{color:#000;padding-bottom:10px}@media screen and (min-width:760px) and (max-width:980px){#menu1 .report-date{margin-left:25px}}@media screen and (min-width:350px) and (max-width:760px){#menu1 .report-date{margin-left:25px}}@media screen and (max-width:350px){#menu1 .report-date{margin-left:25px}}#menu1 #icon{font-size:20px;color:#fff;background-color:#7fba00}#menu1 #pcon{font-size:1.3em}#menu1 .pagination{display:inline-block;padding-left:100px}#menu1 .pagination a{color:#000;float:left;padding:8px 16px;text-decoration:none;transition:background-color .3s;border:1px solid #ddd}#menu1 .pagination a.active{background-color:#0077b5;color:#fff;border:1px solid #4CAF50}#menu1 .pagination a:hover:not(.active){background-color:#ddd}
       </style>
<div class="container" id="con1">
    <div class="container">
    </div>
    <br><br>
    <span id="span1"><?php
echo "Showing results for : ";
?> </span>
    <span id="span2"><?php
echo " ' " . $title . " ' ";
?></span>
    <div class="tab-content">
        <div id="menu1" class="tab-pane fade in active">
            <br>
            <style>
                
            </style>
            <?php
$sql = "SELECT DISTINCT report_id,report_title,published_date,pages,summary,catlog, time FROM categories,reports where categories.cat_id=reports.cat_id and categories.cat_name LIKE '%" . addslashes($title) ."%' or reports.report_title like '%" . addslashes($title) ."%' order by published_date DESC LIMIT $start_from, $limit";
// echo $sql; exit;
$result4 = $con4->query($sql);
$result = $con->query($sql);
$result2 = $con2->query($sql);
$result3 = $con3->query($sql);

if ($result4->num_rows > 0) {
    while ($row = $result4->fetch_assoc()) {
?>  
            <div class="report-panel">
                <div class="published-report">
                    <?php
                    $row['report_title'];
        //$srtitle        = utf8_encode(strtolower(str_replace(' ', '-', limit_words($row['report_title'], 8))));
        $srtitle    = prepareSlugUrl($row['time'], $row['report_title']);
        $srtitle        = str_replace("(", "-", $srtitle);
        $srtitle        = str_replace(")", "-", $srtitle);
        $srtitle        = str_replace("?", "-", $srtitle);
        $srtitle        = str_replace("&", "and", $srtitle);
        $srtitle        = str_replace("!", "-", $srtitle);
        $srtitle        = str_replace("@", "-", $srtitle);
        $srtitle        = str_replace("#", "-", $srtitle);
        $srtitle        = str_replace("$", "-", $srtitle);
        $srtitle        = str_replace("%", "-", $srtitle);
        $srtitle        = str_replace("^", "-", $srtitle);
        $srtitle        = str_replace("*", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("+", "-", $srtitle);
        $srtitle        = str_replace("/", "-", $srtitle);
        $srtitle        = str_replace(":", "-", $srtitle);
        $srtitle        = str_replace(".", "-", $srtitle);
        $srtitle        = str_replace("<", "-", $srtitle);
        $srtitle        = str_replace(">", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("|", "-", $srtitle);
        $srtitle        = str_replace("]", "-", $srtitle);
        $srtitle        = str_replace("[", "-", $srtitle);
        $srtitle        = str_replace("}", "-", $srtitle);
        $srtitle        = str_replace("{", "-", $srtitle);
        $srtitle        = str_replace(";", "-", $srtitle);
        $srtitle        = str_replace("_", "-", $srtitle);
        $srtitle        = str_replace("_x000D_", "-", $srtitle);
        $srtitle        = str_replace("---", "-", $srtitle);
        $srtitle        = str_replace("--", "-", $srtitle);
        $srtitle1       = str_replace("–", "-", $srtitle);
        $finalStrsearch = "industry-reports/";
?> 
                    <i class="fa fa-chart-line" id="icon"></i>  
                    <a href="<?php
        $base_url;
?><?php
        echo $finalStrsearch;
?><?php
        echo $srtitle1;
?>/<?php
        echo $row['report_id'];
?>">
                    <?php
        $tempsr = str_replace("_x000D_", " ", $row["report_title"]);
        $tempsr = str_replace("?", " ", $tempsr);
        echo $tempsr;
?>
                   </a>
                </div>
                <div class="report-date">
                    <i class="fas fa-calendar-alt"></i>
                    <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
               </div>
                <div class="report-pages">
                    <i class="fas fa-file-alt"></i>
                    <?php
        echo "Pages : " . $row["pages"] . "";
?>
               </div>
                <div class="report-industry">
                    <i class="fas fa-industry"></i>
                    <?php
        echo "Industry : " . $row["catlog"] . "";
?>
               </div>
                <br>
                <div class="report-summary">
                    <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
                </div>
                <br>
            </div>
            <?php
    }
    
} 

//
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
?>  
            <div class="report-panel">
                <div class="published-report">
                    <?php
                    $row['report_title'];
        //$srtitle        = utf8_encode(strtolower(str_replace(' ', '-', limit_words($row['report_title'], 8))));
        $srtitle    = prepareSlugUrl($row['time'], $row['report_title']);
        $srtitle        = str_replace("(", "-", $srtitle);
        $srtitle        = str_replace(")", "-", $srtitle);
        $srtitle        = str_replace("?", "-", $srtitle);
        $srtitle        = str_replace("&", "and", $srtitle);
        $srtitle        = str_replace("!", "-", $srtitle);
        $srtitle        = str_replace("@", "-", $srtitle);
        $srtitle        = str_replace("#", "-", $srtitle);
        $srtitle        = str_replace("$", "-", $srtitle);
        $srtitle        = str_replace("%", "-", $srtitle);
        $srtitle        = str_replace("^", "-", $srtitle);
        $srtitle        = str_replace("*", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("+", "-", $srtitle);
        $srtitle        = str_replace("/", "-", $srtitle);
        $srtitle        = str_replace(":", "-", $srtitle);
        $srtitle        = str_replace(".", "-", $srtitle);
        $srtitle        = str_replace("<", "-", $srtitle);
        $srtitle        = str_replace(">", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("|", "-", $srtitle);
        $srtitle        = str_replace("]", "-", $srtitle);
        $srtitle        = str_replace("[", "-", $srtitle);
        $srtitle        = str_replace("}", "-", $srtitle);
        $srtitle        = str_replace("{", "-", $srtitle);
        $srtitle        = str_replace(";", "-", $srtitle);
        $srtitle        = str_replace("_", "-", $srtitle);
        $srtitle        = str_replace("_x000D_", "-", $srtitle);
        $srtitle        = str_replace("---", "-", $srtitle);
        $srtitle        = str_replace("--", "-", $srtitle);
        $srtitle1       = str_replace("–", "-", $srtitle);
        $finalStrsearch = "industry-reports/";
?> 
                    <i class="fa fa-chart-line" id="icon"></i>  
                    <a href="<?php
        $base_url;
?><?php
        echo $finalStrsearch;
?><?php
        echo $srtitle1;
?>/<?php
        echo $row['report_id'];
?>">
                    <?php
        $tempsr = str_replace("_x000D_", " ", $row["report_title"]);
        $tempsr = str_replace("?", " ", $tempsr);
        echo $tempsr;
?>
                   </a>
                </div>
                <div class="report-date">
                    <i class="fas fa-calendar-alt"></i>
                    <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
               </div>
                <div class="report-pages">
                    <i class="fas fa-file-alt"></i>
                    <?php
        echo "Pages : " . $row["pages"] . "";
?>
               </div>
                <div class="report-industry">
                    <i class="fas fa-industry"></i>
                    <?php
        echo "Industry : " . $row["catlog"] . "";
?>
               </div>
                <br>
                <div class="report-summary">
                    <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
                </div>
                <br>
            </div>
            <?php
    }
    
} 
//next db
if ($result2->num_rows > 0) {
    while ($row = $result2->fetch_assoc()) {
?>  
            <div class="report-panel">
                <div class="published-report">
                    <?php
                    $row['report_title'];
        //$srtitle        = utf8_encode(strtolower(str_replace(' ', '-', limit_words($row['report_title'], 8))));
        $srtitle    = prepareSlugUrl($row['time'], $row['report_title']);
        $srtitle        = str_replace("(", "-", $srtitle);
        $srtitle        = str_replace(")", "-", $srtitle);
        $srtitle        = str_replace("?", "-", $srtitle);
        $srtitle        = str_replace("&", "and", $srtitle);
        $srtitle        = str_replace("!", "-", $srtitle);
        $srtitle        = str_replace("@", "-", $srtitle);
        $srtitle        = str_replace("#", "-", $srtitle);
        $srtitle        = str_replace("$", "-", $srtitle);
        $srtitle        = str_replace("%", "-", $srtitle);
        $srtitle        = str_replace("^", "-", $srtitle);
        $srtitle        = str_replace("*", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("+", "-", $srtitle);
        $srtitle        = str_replace("/", "-", $srtitle);
        $srtitle        = str_replace(":", "-", $srtitle);
        $srtitle        = str_replace(".", "-", $srtitle);
        $srtitle        = str_replace("<", "-", $srtitle);
        $srtitle        = str_replace(">", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("|", "-", $srtitle);
        $srtitle        = str_replace("]", "-", $srtitle);
        $srtitle        = str_replace("[", "-", $srtitle);
        $srtitle        = str_replace("}", "-", $srtitle);
        $srtitle        = str_replace("{", "-", $srtitle);
        $srtitle        = str_replace(";", "-", $srtitle);
        $srtitle        = str_replace("_", "-", $srtitle);
        $srtitle        = str_replace("_x000D_", "-", $srtitle);
        $srtitle        = str_replace("---", "-", $srtitle);
        $srtitle        = str_replace("--", "-", $srtitle);
        $srtitle1       = str_replace("–", "-", $srtitle);
        $finalStrsearch = "industry-reports/";
?> 
                    <i class="fa fa-chart-line" id="icon"></i>  
                    <a href="<?php
        $base_url;
?><?php
        echo $finalStrsearch;
?><?php
        echo $srtitle1;
?>/<?php
        echo $row['report_id'];
?>">
                    <?php
        $tempsr = str_replace("_x000D_", " ", $row["report_title"]);
        $tempsr = str_replace("?", " ", $tempsr);
        echo $tempsr;
?>
                   </a>
                </div>
                <div class="report-date">
                    <i class="fas fa-calendar-alt"></i>
                    <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
               </div>
                <div class="report-pages">
                    <i class="fas fa-file-alt"></i>
                    <?php
        echo "Pages : " . $row["pages"] . "";
?>
               </div>
                <div class="report-industry">
                    <i class="fas fa-industry"></i>
                    <?php
        echo "Industry : " . $row["catlog"] . "";
?>
               </div>
                <br>
                <div class="report-summary">
                    <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
                </div>
                <br>
            </div>
            <?php
    }
    
} 
//
if ($result3->num_rows > 0) {
    while ($row = $result3->fetch_assoc()) {
?>  
            <div class="report-panel">
                <div class="published-report">
                    <?php
                    $row['report_title'];
        //$srtitle        = utf8_encode(strtolower(str_replace(' ', '-', limit_words($row['report_title'], 8))));
        $srtitle    = prepareSlugUrl($row['time'], $row['report_title']);
        $srtitle        = str_replace("(", "-", $srtitle);
        $srtitle        = str_replace(")", "-", $srtitle);
        $srtitle        = str_replace("?", "-", $srtitle);
        $srtitle        = str_replace("&", "and", $srtitle);
        $srtitle        = str_replace("!", "-", $srtitle);
        $srtitle        = str_replace("@", "-", $srtitle);
        $srtitle        = str_replace("#", "-", $srtitle);
        $srtitle        = str_replace("$", "-", $srtitle);
        $srtitle        = str_replace("%", "-", $srtitle);
        $srtitle        = str_replace("^", "-", $srtitle);
        $srtitle        = str_replace("*", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("+", "-", $srtitle);
        $srtitle        = str_replace("/", "-", $srtitle);
        $srtitle        = str_replace(":", "-", $srtitle);
        $srtitle        = str_replace(".", "-", $srtitle);
        $srtitle        = str_replace("<", "-", $srtitle);
        $srtitle        = str_replace(">", "-", $srtitle);
        $srtitle        = str_replace("'", "-", $srtitle);
        $srtitle        = str_replace("|", "-", $srtitle);
        $srtitle        = str_replace("]", "-", $srtitle);
        $srtitle        = str_replace("[", "-", $srtitle);
        $srtitle        = str_replace("}", "-", $srtitle);
        $srtitle        = str_replace("{", "-", $srtitle);
        $srtitle        = str_replace(";", "-", $srtitle);
        $srtitle        = str_replace("_", "-", $srtitle);
        $srtitle        = str_replace("_x000D_", "-", $srtitle);
        $srtitle        = str_replace("---", "-", $srtitle);
        $srtitle        = str_replace("--", "-", $srtitle);
        $srtitle1       = str_replace("–", "-", $srtitle);
        $finalStrsearch = "industry-reports/";
?> 
                    <i class="fa fa-chart-line" id="icon"></i>  
                    <a href="<?php
        $base_url;
?><?php
        echo $finalStrsearch;
?><?php
        echo $srtitle1;
?>/<?php
        echo $row['report_id'];
?>">
                    <?php
        $tempsr = str_replace("_x000D_", " ", $row["report_title"]);
        $tempsr = str_replace("?", " ", $tempsr);
        echo $tempsr;
?>
                   </a>
                </div>
                <div class="report-date">
                    <i class="fas fa-calendar-alt"></i>
                    <?php
        echo "Published Date : " . $row["published_date"] . "";
?>
               </div>
                <div class="report-pages">
                    <i class="fas fa-file-alt"></i>
                    <?php
        echo "Pages : " . $row["pages"] . "";
?>
               </div>
                <div class="report-industry">
                    <i class="fas fa-industry"></i>
                    <?php
        echo "Industry : " . $row["catlog"] . "";
?>
               </div>
                <br>
                <div class="report-summary">
                    <?php
        echo substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350);
?>...
                </div>
                <br>
            </div>
            <?php
    }
    
} 

//

else {
    echo "<p id='pcon'><b>We would need you try another search term. Additionally please mention your specific requirements to us in the form below so we can get back to you with a customized solution that fits your needs.</b></p>";
    
    echo '<form action="https://www.acquiremarketresearch.com/market-analysis-thank-you/" style="font-size:1.4em;" method="POST"  id="contact_form">
            <div class="row">
                <div class="col-25">
                    <label for="fname">Full Name</label>
                </div>
                <div class="col-75">
                    <input type="text" id="firstname" name="firstname" placeholder="Full Name" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;"  required>
                </div>
                <span id="error_name" style="color: Red; display: none">* Please enter valid name</span>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">Business Email</label>
                </div>
                <div class="col-75">
                    <input type="email" id="bemail" name="bemail" placeholder="Please enter company email id" required/>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="cname">Company Name</label>
                </div>
                <div class="col-75">
                    <input type="text" id="cname" name="cname" placeholder="Company name">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="desig">Designation</label>
                </div>
                <div class="col-75">
                    <input type="text" id="desig" name="desig" placeholder="Title" required>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="ph">Phone Number</label>
                </div>
                <div class="col-75">
                    <div class="row">
                        
                        <div class="col-md-9">
                            <input type="text" id="ph" name="ph" placeholder="Phone number" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="15" required /><span id="error_no" style="color: Red; display: none">* Input digits (0 - 9)</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-25">
                    <label for="subject">Search Keyword</label>
                </div>
                <div class="col-75">
                    <textarea id="qur" name="qur"  placeholder="Type here..." style="height:200px;font-weight:bold;">' . $title . '</textarea>
                </div>
            </div>
            <br><br>
            
            <div class="row" style="padding:20px;">
                <input type="submit" id="sub-btn" value="Submit" style="width:25%; margin-right:300px;">
            </div>
        </form>';
}

?> <br>
            <style>
                
            </style>
            <?php
$sql = "SELECT COUNT(DISTINCT report_id,report_title,published_date,pages,summary) FROM categories,reports where categories.cat_id=reports.cat_id and categories.cat_name LIKE '%$title%' or reports.report_title like '%$title%'";

$rs_result     = mysqli_query($con, $sql);
$row           = mysqli_fetch_row($rs_result);
$total_records = $row[0];
$total_pages   = ceil($total_records / $limit);

$current_page = $page;
$url          = rtrim(str_replace(" ", "_", $url));

if ($page == 1) {
    $pagLink = "<div><ul  class='pagination' style='display:inline'><li style='display:inline' class='disabled'><a>Previous</a></li>";
} else {
    $pagLink = "<div><ul  class='pagination' style='display:inline'><li style='display:inline'><a href='search-report.php?search=" . $title . "&page=" . ($page - 1) . "' id='pp'>Previous</a></li>";
}

for ($i = max(1, $page - 4); $i <= min($page + 4, $total_pages); $i++) {
    
    
    $active = '';
    if (isset($_GET['page']) && $i == $_GET['page']) {
        $active = 'class="active"';
    }
    
    $pagLink .= "<li $active style='display:inline'>&nbsp;<a href='search-report.php?search=" . $title . "&page=" . $i . "' >" . $i . "&nbsp;</a></li>";
    
}
;
if ($page < $total_pages) {
    echo $pagLink . "<li style='display:inline'><a href='search-report.php?search=" . $title . "&page=" . ($page + 1) . "' id='np'>NEXT</a></li> </ul></div>";
} else {
    if ($page > $total_pages) {
        
    }
    echo $pagLink . "<li style='display:inline' class='disabled'><a>NEXT</a></li> </ul></div>";
}
?><br><br><br>
            <div><b>
                <?php
echo "Page No : " . $current_page . " / " . $total_pages;

?></b>
            </div>
        </div>
        <br><br><br><br>
    </div>
</div>
<?php
function limit_words($string, $word_limit)
{
    $words = str_replace(",", " ", $string);
    $words = explode(" ", $words);
    
    return implode(" ", array_splice($words, 0, $word_limit));
}

?>
<?php
include 'footer_file.php';
?>