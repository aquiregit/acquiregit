<div class="container-fluid" id="foot">
    <style type="text/css">
       #fb:hover,#lin:hover,#tw:hover{background-color:#fff}#lin:hover,#upbtn{color:#0077b5}#bg,#fb,#firstrow li a,#foot,#gp,#lin,#myBtn,#tw,#wp,#yt,#yt:hover{color:#fff}#foot{background-color:#0077b5;font-size:1.3em;padding-left: 0px;padding-right: 0px}#firstrow,#secondrow{border-bottom:1px solid gray}#firstrow h5{padding-left:40px}#firstrow ul{list-style-type:none}#tw:hover{color:#1dcaff}#gp:hover{color:#c00}#wp:hover{color:#464646}#fb:hover{color:#3B5998}#yt:hover{background-color:red}#bg:hover{color:#f57d00}#paycol{text-align:right;padding-bottom:10px}.social-icons{padding-left:8px}@media screen and (min-width:350px) and (max-width:760px){#sslimg{margin-left:-19px}#siteimg{margin-left:59px}}@media screen and (max-width:350px){#sslimg{margin-left:-19px}#siteimg{margin-left:59px}}#sslimg{padding-left:70px}#siteimg{padding-right:30px}#copyrightrow{font-size:0.8em;text-align:center;padding:5px;color:#ddd}#myBtn{display:none;position:fixed;bottom:20px;right:30px;border:none;outline:0;background-color:#e0e0e0;cursor:pointer;padding:15px;border-radius:10px}
      #pin:hover{color:#c8232c;background-color:#fff;}#rss:hover{color:#82c91e;}
        ul.social-icons {
            list-style-type:none;
        }
        ul.social-icons li {
            display:inline;
            padding-right: 7px;
            
        }
        .cnt{text-align:center;}
    </style>
    <footer id="#grad1">
        <div class="container">
            <div class="row" id="firstrow">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <br>
                    <h5><strong>Acquire Market Research</strong></h4>
                    <ul>
                        <li><a href="<?php echo $base_url;?>about"><i class="fas fa-angle-double-right"></i>&nbsp;&nbsp;About Us</a></li>
                        <li><a href="<?php echo $base_url;?>services"><i class="fas fa-angle-double-right"></i>&nbsp; Our Services</a></li>
                          <li><a href="<?php echo $base_url;?>support#payment_options"><i class="fas fa-angle-double-right"></i>&nbsp; Payment Options</a></li>
                        <li><a href="<?php echo $base_url;?>support#sub"><i class="fas fa-angle-double-right"></i>&nbsp; Subscription</a></li>
                        <li><a href="<?php echo $base_url;?>sitemap.xml"><i class="fas fa-angle-double-right"></i>&nbsp; Site Map</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <br>
                    <h5><strong>Help Desk/Support</strong></h4>
                    <ul>
                        <li><a href="<?php echo $base_url;?>terms-and-conditions#T&C"><i class="fas fa-angle-double-right"></i>&nbsp; Terms and Conditions</a></li>
                        <li><a href="<?php echo $base_url;?>terms-and-conditions#privacy_policy"><i class="fas fa-angle-double-right"></i>&nbsp; Privacy Policy</a></li>
                        <li><a href="<?php echo $base_url;?>support#how_to_order"><i class="fas fa-angle-double-right"></i>&nbsp; How To Order</a></li>
						 <li><a href="<?php echo $base_url;?>support#disclaimer"><i class="fas fa-angle-double-right"></i>&nbsp; Disclaimer</a></li>
						 <li><a href="<?php echo $base_url;?>terms-and-conditions#returnpollicy"><i class="fas fa-angle-double-right"></i>&nbsp; Return Policy</a></li>
                    </ul>
                </div>
                <div id="thirdcol" class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
                    <br>
                    <h5><strong> Stay Connected </strong> </h4>
                    <ul>
                        <!--li><a href="mailto:sales@acquiremarketresearch.com"><i class="fas fa-envelope"></i>&nbsp;&nbsp;
                           <a 
style="color:#fff;" href="javascript:location='mailto:\u0073\u0061\u006c\u0065\u0073\u0040\u0061\u0063\u0071\u0075\u0069\u0072\u0065\u006d\u0061\u0072\u006b\u0065\u0074\u0072\u0065\u0073\u0065\u0061\u0072\u0063\u0068\u002e\u0063\u006f\u006d';void 0"><script type="text/javascript">document.write('\u0073\u0061\u006c\u0065\u0073\u0040\u0061\u0063\u0071\u0075\u0069\u0072\u0065\u006d\u0061\u0072\u006b\u0065\u0074\u0072\u0065\u0073\u0065\u0061\u0072\u0063\u0068\u002e\u0063\u006f\u006d')</script></a></a></li-->
                        <li>
                            <i class="fas fa-envelope"></i>&nbsp;&nbsp;sales@acquiremarketresearch.com
                        </li>
                        
                        
                        <li><!--a href="Skype:1-800-663-5579 call" id="telno"--><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;&nbsp;+1-800-663-5579<!--/a--></li>
                       <div>
                         <ul class="social-icons">
                            <br><br>
<!--                            <a href="https://plus.google.com/u/0/114890009426819553074" target="_blank"><i class="fab fa-google-plus-square fa-3x" id="gp"></i></a>-->
                            <li><a href="https://acquireblog.blogspot.com/" target="_blank"><i class="fab fa-blogger fa-2x" id="bg"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/acquire-market-research" target="_blank"><i class="fab fa-linkedin fa-2x" id="lin"></i></a></li>
                            <li><a href="<?php echo $base_url;?>feed" target="_blank"><i class="fas fa-rss fa-2x" id="rss"></i></a></li>
                            
                            <li><a href="https://acquiremarketresearch.wordpress.com/" target="_blank"><i class="fab fa-wordpress fa-2x" id="wp"></i></a></li>
			                <!--li><a href="https://www.youtube.com/channel/UCrQ07_Ftor36onWcqcYiYAA" target="_blank"><i class="fab fa-youtube-square fa-2x" id="yt"></i></a></li-->
                            <li><a href="https://www.facebook.com/Acquire-Market-Research-336222877250500/" target="_blank"><i class="fab fa-facebook-square fa-2x" id="fb"></i></a></li>
                            <!--li><a href="https://www.pinterest.com/acquiremarketresearch/pins/" target="_blank"><i class="fab fa-pinterest-square fa-2x" id="pin"></i></a></li-->
                            <li><a href="https://twitter.com/Acquire_MR" target="_blank"><i class="fab fa-twitter-square fa-2x" id="tw"></i></a></li>
                    </ul>
                    </div>
                </div>
            </div>
            <!--div id="secondrow" class="">
                <div class="row">
                <!--    <div id="social" class="col-md-6 col-sm-12 col-sm-12">
                          <ul class="social-icons">
                            <br><br>
<!--                            <a href="https://plus.google.com/u/0/114890009426819553074" target="_blank"><i class="fab fa-google-plus-square fa-3x" id="gp"></i></a>-->
                            <!--<li><a href="https://twitter.com/Acquire_MR" target="_blank"><i class="fab fa-twitter-square fa-3x" id="tw"></i></a></li>

                            <li><a href="https://www.linkedin.com/company/acquire-market-research" target="_blank"><i class="fab fa-linkedin fa-3x" id="lin"></i></a></li>

                            <li><a href="https://acquiremarketresearch.wordpress.com/" target="_blank"><i class="fab fa-wordpress fa-3x" id="wp"></i></a></li>

			                <li><a href="https://www.youtube.com/channel/UCrQ07_Ftor36onWcqcYiYAA" target="_blank"><i class="fab fa-youtube-square fa-3x" id="yt"></i></a></li>
							
                            <li><a href="#" target="_blank"><i class="fab fa-facebook-square fa-3x" id="fb"></i></a></li>

                            <li><a href="https://acquireblog.blogspot.com/" target="_blank"><i class="fab fa-blogger fa-3x" id="bg"></i></a></li>
                            
                            </ul>
                    </div>-->
                    <!--<div id="paycol" class="col-md-6 col-sm-12 col-xs-12">
                        <img id="payimg" src="<?php echo $base_url;?>images/Payment_Logo.png" height= "130px" width= "500px" alt="regions reports" title="PayPal" class="img-responsive"/>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="row">
            <div id="copyrightrow" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
                    <span target="_blank" > Copyright &copy; <?php echo date("Y")?> Acquire Market Research and Consulting. All rights reserved.</span>
                </div>
            </div>
        </div>
</div>
<button type="button" onclick="topFunction()" id="myBtn" title="Go to top">
<i id="upbtn" class="fas fa-caret-up fa-2x" style=""></i></button>
</footer>
</div>
<script>
    function topFunction() {
      document.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
    $(window).scroll(function() {
    if (document.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("myBtn").style.display = "block";
    } else {
      document.getElementById("myBtn").style.display = "none";
    }
    }); 
</script>
</body>
<script type="text/javascript">
    $('#body').css('min-height',screen.height);
</script>
</html>