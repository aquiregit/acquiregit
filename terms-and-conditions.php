<?php
$pagetitle = "AMR Terms and Conditions | Acquire Market Research";
$desc      = " Terms and Conditions | Acquire Market Research";
$key       = "AMR  Terms and Conditions, privacy policy, Acquire Market Research Report , consulting services";
?>
<?php
include 'header_file.php';
?>

<style type="text/css">
    #privacy_policy span:nth-child(even),#tc span:nth-child(even){color:#87cefa;font-size:30px;font-weight:700}#privacy_policy span:nth-child(odd),#tc span:nth-child(odd){color:#0077b5;font-size:30px;font-weight:700}#para p,#privacy_policy #para2 p{text-align:justify;font-size:18px}#para p{text-justify:inter-word}#para .policy li{margin-bottom:20px;line-height:30px;padding-left:1em;color:#000}#para hr{margin-top:-10px;margin-bottom:-1px}#para h3{color:#0077b5;font-weight:700;text-align:center}#para #plink{color:#00f;font-weight:700}#privacy_policy{background-color:#e0e0e0;padding-top:30px}#privacy_policy #para2 p{text-justify:inter-word;font-family:Lato}#privacy_policy #para2 .policy li{margin-bottom:20px;line-height:25px;list-style-type:disc;padding-left:1em;color:#000}
</style>

<div class="container" id="T&C">
    <div class="row">
        <p class="lead"><br></p>
    </div>
    <div class="container">
        <div class="row">
            <br>
            <p class="center" id="tc">&nbsp;&nbsp;&nbsp;&nbsp;
                <span>Terms</span>&nbsp;&nbsp;
                <span>and</span>&nbsp;
                <span>Conditions</span>
            </p>
            <br>
            <div class="col-sm-12 about-us-text wow fadeInLeft" id="para">
            
                <h3>GENERAL</h3>
                <ul class="policy">
                    <li>
                        <p>
                            Post the order placement, the report will be delivered to you online/by email within 3-5 business days or sooner post the receipt of payment. However, in case of physical delivery of the print copy of the report, if requested for, we will courier the report within 3 working days from the date of the receipt of payment                        
                        </p>
                    </li>
                    <li>
                        <p>
                            You will be eligible to receiving 10% customization in the syndicated reports based on your specific requirement pertaining to the markets discussed in the report. However, your requirement<div id="returnpollicy"></div> complexity and duration to complete the final report will be purely judged by our research team and will share the final say. At Acquire Market Research, we ensure that you get the accurate market intelligence in our reports
                        </p>
                    </li>
                </ul>
                <h3>RETURN POLICY</h3>
                <ul class="policy ">
                    <li>
                        <p>We expect our buyers to be familiar with the nature of our industry and to be completely informed about the data sensitivity of the products and services we offer. So in any case, we regret to inform you that we are unable to admit returns of our products and services once they have been delivered to the concerned party.</p>
                    </li>
                    <li>
                        <p>For the same, we request you to go through all the available information about the report and then only place your valuable order. Further, you can be assured to get what we have promised you and there will be no compromise in our services.</p>
                    </li>
                </ul>
                <h3>FORMAT & DELIVERY</h3>
                <ul class="policy ">
                    <li>
                        <p>
                            At Acquire the data we provide is in the following format – PDF. In case of any other specific formats required, you can reach out to us and depend on the feasibility we can assist you further 
                        </p>
                    </li>
                    <li>
                        <p>
                            Delivery Period – 5 to 7 Business days
                        </p>
                    </li>
                    <li>
                        <p>
                            In case of any changes in requirements or specific updates from the client side, the delivery time frame can exceed the appropriate sign off 
                        </p>
                    </li>
                    <li>
                        <p>Secure Online Payment</p>
                    </li>
                    <li>
                        <p>At Acquire, be rest assured that we work with secured gateways and the information you provide will be treated with utmost responsibility and only for the purchases permitted by you</p>
                    </li>
                    <li>
                        <p>You can have access to your desired information in just these 4 clicks
                        <p id="plink">– Select Category >> Shop Report >> Verify Information >> Pay</p>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<br><br><!--/.T & C container-->
<div class="container-fluid" id="privacy_policy">
    <div class="row">
        <br>
        <p class="center">
            <span>Privacy</span>&nbsp;&nbsp;
            <span>Policy</span>
        </p>
        <br>
        <div class="container" >
            <div class="row">
                <div class="col-sm-12 about-us-text wow fadeInLeft" id="para2">
                    <ul class="policy ">
                        <li>
                            <p>
                                Acquire strategic information provision services are publications containing authentic and valuable market information made available to individuals, companies and educational institutions depending on the orders. Our client base acknowledges that when purchasing that Acquire strategic information services are for our customers’ internal use and not to be disclosed to public or third parties               
                            </p>
                        </li>
                        <li>
                            <p>Acquire would not be responsible for any inappropriate or incorrect information provided to us by participants, manufacturers or users                 
                            </p>
                        </li>
                        <li>
                            <p>No part of this information service may be provided, rented, resold or disclosed to non-customers without written permission
                            </p>
                        </li>
                        <li>
                            <p>We also use other research firms to help us undertake our market research services. These firms have their own websites and apps and have their own privacy and cookies policies which apply to visitors to their websites and users of their apps            
                            </p>
                        </li>
                        <li>
                            <p>All product names, logos, brands and process used in this site are property of the respective owners
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
</div>
<br><br>
<?php
include 'footer_file.php';
?>
