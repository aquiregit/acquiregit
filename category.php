<?php
error_reporting(E_WARNING);
// echo 11; exit;
include 'connect.php';

$data       = $_SERVER['REQUEST_URI'];
$t2         = explode('/', $data);
$cname1     = $t2[2];
$t2         = rtrim($cname1, "-market-reports");
$cname1     = str_replace("-", " ", $t2);
$cname1     = str_replace("and", "&", $cname1);
$sqldata    = "SELECT * FROM categories where cat_name LIKE '%" . $cname1 . "%' ";
$resultdata = mysqli_query($con, $sqldata);
if ($resultdata->num_rows > 0) {
    while ($rowdata = $resultdata->fetch_assoc()) {
        $pagetitle = $rowdata['cat_name'] . ' Market Research Reports';
        $desc      = $rowdata['meta_desc'];
        $key       = $rowdata['meta_keywords'];
        $intro     = $rowdata['cat_intro'];
    }
}
include "header_file.php";
$data = $_SERVER['REQUEST_URI'];

$t = (explode("/", $data));
$y     = $t[2];
$page1 = $t[3];
$t1           = rtrim($y, "-market-reports");
$cname        = str_replace("-", " ", $t1);
$cname        = str_replace("and", "&", $cname);
$temp         = $y;
$industryname = str_replace("-market-reports", "", $temp);
$industryname = str_replace("-", " ", $industryname);
$industryname = str_replace("and", "&", $industryname);
$limit        = 8;
if (isset($page1)) {
    $page = $page1;
} else {
    $page = 1;
}
;
$start_from = ($page - 1) * $limit;
?>
<div class="container-fluid" id="allcon">
    <div class="container">
    </div>
    <br><br>
    <style type="text/css">
      #cathead h2{color:#0077b5;font-weight:700;text-align:center}@media screen and (min-width:760px) and (max-width:980px){#div_rp{margin-left:-65px}}@media screen and (min-width:350px) and (max-width:760px){#div_rp{margin-left:-65px}}@media screen and (max-width:350px){#div_rp{margin-left:-65px}}#div_rp{padding-left:80px}.report-panel{border-width:.5px;border-style:solid;border-color:#F2F1F1;padding-bottom:10px;font-size:17px}.report-panel:nth-child(even){background-color:#FFF!important}.report-panel:nth-child(odd){background-color:#E0E0E0!important}.published-report{line-height:normal!important;color:#0077B5;overflow:hidden;margin:0;font-size:15px!important;display:block;font-weight:700;padding-left:15px;padding-top:20px}.report-date,.report-pages{color:#E6751E}.report-date{font-size:14px;margin-left:45px;display:inline-block;padding-bottom:10px;padding-top:10px}.report-des,.report-pages{font-size:14px;margin-left:25px;padding-bottom:10px;display:inline-block}.report-des{color:#000}@media screen and (min-width:760px) and (max-width:980px){.report-date{margin-left:25px}}@media screen and (min-width:350px) and (max-width:760px){.report-date{margin-left:25px}}@media screen and (max-width:350px){.report-date{margin-left:25px}}#published-reporti{font-size:20px;color:#fff;background-color:#7fba00}.pagination{display:inline-block;padding-left:100px}.pagination a{color:#000;float:left;padding:8px 16px;text-decoration:none;transition:background-color .3s}.pagination a.active{background-color:#0077b5;color:#fff}.pagination a:hover:not(.active){background-color:#ddd}.w3-card,.w3-card-2{box-shadow:0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12)}.w3-card-4,.w3-hover-shadow:hover{box-shadow:0 4px 10px 0 rgba(0,0,0,.2),0 4px 20px 0 rgba(0,0,0,.19)}.w3-container{padding:.01em 16px}#all_report_card{width:70%;background-color:#e0e0e0}#all_report_card h3{border-bottom:1px solid gray;padding-bottom:10px}#all_report_card a{color:#0B3C5D;font-size:1em}#all_report_card img{height:150px}#enq_form{width:70%}#enq_form h4{border-bottom:1px solid gray;padding-bottom:7px;padding-top:5px}#enqi{font-size:12px}#pr_form label{color:#0077b5;font-size:16px}@media screen and (min-width:980px) and (max-width:1024px){#all_report_card,#enq_form{margin-left:50px}#pr_form input{width:150px}}@media screen and (min-width:760px) and (max-width:980px){#all_report_card,#enq_form{margin-left:50px}#pr_form input{width:150px}}@media screen and (min-width:350px) and (max-width:760px){#all_report_card,#enq_form{margin-left:25px;width:80%}#pr_form input{width:150px}}@media screen and (max-width:350px){#all_report_card,#enq_form{margin-left:25px;width:80%}#pr_form input{width:150px}}#error_name,#error_ph{color:Red;display:none}#all_card{padding-left:30px;padding-top:50px}#imgi{font-size:15px;}
        .catintro {
            text-align: center;
            font-size: 18px;
            color: #000;
        }
    </style>
    <div id="cathead" class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
       <h2><?php
echo ucwords($industryname);
?></h2>
<p class="catintro"><?php echo $intro ?></p><br>
    </div>
  
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12" id="div_rp">
        <div class="more-report">
            <a href="<?php
echo $base_url;
?>industries" rel="dofollow"><button class="btn btn-primary"><i class="fas fa-angle-double-left"></i>&nbsp;View All Categories&nbsp;</button></a>
        </div>
        <br>
        
        <?php
$ql  = "SELECT cat_id FROM categories where cat_name LIKE '%" . $cname . "%' ";
$res = $con->query($ql);

if ($res->num_rows > 0) {
    while ($rw = $res->fetch_assoc()) {
    $chk = $rw['cat_id'];       
    }
}
?>
       <?php
         
        
        $sql = "SELECT * FROM reports,categories where reports.cat_id=categories.cat_id  and reports.cat_id='".$chk."' ORDER BY published_date DESC LIMIT $start_from, $limit";
        
        $result4 = $con4->query($sql);
        $result = $con->query($sql); 
        $result2 = $con2->query($sql);
         $result3 = $con3->query($sql);
    
        if ($result4->num_rows > 0) {
            while ($row = $result4->fetch_assoc()) {
                
?>
        <div class="report-panel">
            <div class="published-report">
                <i id="published-reporti" class="fa <?=$row['fa_fa_font'];?>"></i>  
                <?php
                $r1       = $row['report_id'];
                // $urltitle = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
                $urltitle = prepareSlugUrl($row['time'], $row['report_title']);
                parse_url('rid=' . $r1 . '');
               
                $urltitle  = str_replace("(", "-", $urltitle);
                $urltitle  = str_replace(")", "-", $urltitle);
                $urltitle  = str_replace("?", "-", $urltitle);
                $urltitle  = str_replace("&", "and", $urltitle);
                $urltitle  = str_replace("!", "-", $urltitle);
                $urltitle  = str_replace("@", "-", $urltitle);
                $urltitle  = str_replace("#", "-", $urltitle);
                $urltitle  = str_replace("$", "-", $urltitle);
                $urltitle  = str_replace("%", "-", $urltitle);
                $urltitle  = str_replace("^", "-", $urltitle);
                $urltitle  = str_replace("*", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("+", "-", $urltitle);
                $urltitle  = str_replace("/", "-", $urltitle);
                $urltitle  = str_replace(":", "-", $urltitle);
                $urltitle  = str_replace(".", "-", $urltitle);
                $urltitle  = str_replace("<", "-", $urltitle);
                $urltitle  = str_replace(">", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("|", "-", $urltitle);
                $urltitle  = str_replace("]", "-", $urltitle);
                $urltitle  = str_replace("[", "-", $urltitle);
                $urltitle  = str_replace("}", "-", $urltitle);
                $urltitle  = str_replace("{", "-", $urltitle);
                $urltitle  = str_replace(";", "-", $urltitle);
                $urltitle  = str_replace("_", "-", $urltitle);
                $urltitle  = str_replace("_x000D_", "-", $urltitle);
                $urltitle  = str_replace("--", "-", $urltitle);
                $urltitle1 = str_replace("---", "-", $urltitle);
                
                $finalStrsd = "industry-reports/";
?> 
                <a href="<?php
                echo $base_url;
?><?php
                echo $finalStrsd;
?><?php
                echo $urltitle1;
?>/<?php
                echo $row['report_id'];
?>" rel="dofollow">
                <?php
                $rtitleindex = str_replace("?", " ", $row['report_title']);
                $rtitleindex = utf8_encode(str_replace("_x000D_", " ", $rtitleindex));
                echo $rtitleindex;
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
                echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
                echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-des">
                <?php
                echo utf8_encode(substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350));
?>...
            </div>
            <br>
        </div>
        <?php
            }
        }
?>

<!-- -->
 <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                
?>
        <div class="report-panel">
            <div class="published-report">
                <i id="published-reporti" class="fa <?=$row['fa_fa_font'];?>"></i>  
                <?php
                $r1       = $row['report_id'];
                // $urltitle = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
                $urltitle = prepareSlugUrl($row['time'], $row['report_title']);
                parse_url('rid=' . $r1 . '');
               
                $urltitle  = str_replace("(", "-", $urltitle);
                $urltitle  = str_replace(")", "-", $urltitle);
                $urltitle  = str_replace("?", "-", $urltitle);
                $urltitle  = str_replace("&", "and", $urltitle);
                $urltitle  = str_replace("!", "-", $urltitle);
                $urltitle  = str_replace("@", "-", $urltitle);
                $urltitle  = str_replace("#", "-", $urltitle);
                $urltitle  = str_replace("$", "-", $urltitle);
                $urltitle  = str_replace("%", "-", $urltitle);
                $urltitle  = str_replace("^", "-", $urltitle);
                $urltitle  = str_replace("*", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("+", "-", $urltitle);
                $urltitle  = str_replace("/", "-", $urltitle);
                $urltitle  = str_replace(":", "-", $urltitle);
                $urltitle  = str_replace(".", "-", $urltitle);
                $urltitle  = str_replace("<", "-", $urltitle);
                $urltitle  = str_replace(">", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("|", "-", $urltitle);
                $urltitle  = str_replace("]", "-", $urltitle);
                $urltitle  = str_replace("[", "-", $urltitle);
                $urltitle  = str_replace("}", "-", $urltitle);
                $urltitle  = str_replace("{", "-", $urltitle);
                $urltitle  = str_replace(";", "-", $urltitle);
                $urltitle  = str_replace("_", "-", $urltitle);
                $urltitle  = str_replace("_x000D_", "-", $urltitle);
                $urltitle  = str_replace("--", "-", $urltitle);
                $urltitle1 = str_replace("---", "-", $urltitle);
                
                $finalStrsd = "industry-reports/";
?> 
                <a href="<?php
                echo $base_url;
?><?php
                echo $finalStrsd;
?><?php
                echo $urltitle1;
?>/<?php
                echo $row['report_id'];
?>" rel="dofollow">
                <?php
                $rtitleindex = str_replace("?", " ", $row['report_title']);
                $rtitleindex = utf8_encode(str_replace("_x000D_", " ", $rtitleindex));
                echo $rtitleindex;
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
                echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
                echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-des">
                <?php
                echo utf8_encode(substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350));
?>...
            </div>
            <br>
        </div>
        <?php
            }
        }
?>
<!-- -->
 <?php
        if ($result2->num_rows > 0) {
            while ($row = $result2->fetch_assoc()) {
                
?>
        <div class="report-panel">
            <div class="published-report">
                <i id="published-reporti" class="fa <?=$row['fa_fa_font'];?>"></i>  
                <?php
                $r1       = $row['report_id'];
                // $urltitle = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
                $urltitle = prepareSlugUrl($row['time'], $row['report_title']);
                parse_url('rid=' . $r1 . '');
               
                $urltitle  = str_replace("(", "-", $urltitle);
                $urltitle  = str_replace(")", "-", $urltitle);
                $urltitle  = str_replace("?", "-", $urltitle);
                $urltitle  = str_replace("&", "and", $urltitle);
                $urltitle  = str_replace("!", "-", $urltitle);
                $urltitle  = str_replace("@", "-", $urltitle);
                $urltitle  = str_replace("#", "-", $urltitle);
                $urltitle  = str_replace("$", "-", $urltitle);
                $urltitle  = str_replace("%", "-", $urltitle);
                $urltitle  = str_replace("^", "-", $urltitle);
                $urltitle  = str_replace("*", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("+", "-", $urltitle);
                $urltitle  = str_replace("/", "-", $urltitle);
                $urltitle  = str_replace(":", "-", $urltitle);
                $urltitle  = str_replace(".", "-", $urltitle);
                $urltitle  = str_replace("<", "-", $urltitle);
                $urltitle  = str_replace(">", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("|", "-", $urltitle);
                $urltitle  = str_replace("]", "-", $urltitle);
                $urltitle  = str_replace("[", "-", $urltitle);
                $urltitle  = str_replace("}", "-", $urltitle);
                $urltitle  = str_replace("{", "-", $urltitle);
                $urltitle  = str_replace(";", "-", $urltitle);
                $urltitle  = str_replace("_", "-", $urltitle);
                $urltitle  = str_replace("_x000D_", "-", $urltitle);
                $urltitle  = str_replace("--", "-", $urltitle);
                $urltitle1 = str_replace("---", "-", $urltitle);
                
                $finalStrsd = "industry-reports/";
?> 
                <a href="<?php
                echo $base_url;
?><?php
                echo $finalStrsd;
?><?php
                echo $urltitle1;
?>/<?php
                echo $row['report_id'];
?>" rel="dofollow">
                <?php
                $rtitleindex = str_replace("?", " ", $row['report_title']);
                $rtitleindex = utf8_encode(str_replace("_x000D_", " ", $rtitleindex));
                echo $rtitleindex;
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
                echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
                echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-des">
                <?php
                echo utf8_encode(substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350));
?>...
            </div>
            <br>
        </div>
        <?php
            }
        }
?>

<!-- -->
 <?php
        if ($result3->num_rows > 0) {
            while ($row = $result3->fetch_assoc()) {
                
?>
        <div class="report-panel">
            <div class="published-report">
                <i id="published-reporti" class="fa <?=$row['fa_fa_font'];?>"></i>  
                <?php
                $r1       = $row['report_id'];
                // $urltitle = utf8_encode(str_replace(' ', '-', limit_words(strtolower($row['report_title']), 8)));
                $urltitle = prepareSlugUrl($row['time'], $row['report_title']);
                parse_url('rid=' . $r1 . '');
               
                $urltitle  = str_replace("(", "-", $urltitle);
                $urltitle  = str_replace(")", "-", $urltitle);
                $urltitle  = str_replace("?", "-", $urltitle);
                $urltitle  = str_replace("&", "and", $urltitle);
                $urltitle  = str_replace("!", "-", $urltitle);
                $urltitle  = str_replace("@", "-", $urltitle);
                $urltitle  = str_replace("#", "-", $urltitle);
                $urltitle  = str_replace("$", "-", $urltitle);
                $urltitle  = str_replace("%", "-", $urltitle);
                $urltitle  = str_replace("^", "-", $urltitle);
                $urltitle  = str_replace("*", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("+", "-", $urltitle);
                $urltitle  = str_replace("/", "-", $urltitle);
                $urltitle  = str_replace(":", "-", $urltitle);
                $urltitle  = str_replace(".", "-", $urltitle);
                $urltitle  = str_replace("<", "-", $urltitle);
                $urltitle  = str_replace(">", "-", $urltitle);
                $urltitle  = str_replace("'", "-", $urltitle);
                $urltitle  = str_replace("|", "-", $urltitle);
                $urltitle  = str_replace("]", "-", $urltitle);
                $urltitle  = str_replace("[", "-", $urltitle);
                $urltitle  = str_replace("}", "-", $urltitle);
                $urltitle  = str_replace("{", "-", $urltitle);
                $urltitle  = str_replace(";", "-", $urltitle);
                $urltitle  = str_replace("_", "-", $urltitle);
                $urltitle  = str_replace("_x000D_", "-", $urltitle);
                $urltitle  = str_replace("--", "-", $urltitle);
                $urltitle1 = str_replace("---", "-", $urltitle);
                
                $finalStrsd = "industry-reports/";
?> 
                <a href="<?php
                echo $base_url;
?><?php
                echo $finalStrsd;
?><?php
                echo $urltitle1;
?>/<?php
                echo $row['report_id'];
?>" rel="dofollow">
                <?php
                $rtitleindex = str_replace("?", " ", $row['report_title']);
                $rtitleindex = utf8_encode(str_replace("_x000D_", " ", $rtitleindex));
                echo $rtitleindex;
?>
               </a>
            </div>
            <div class="report-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
                echo "Published Date : " . $row["published_date"] . "";
?>
           </div>
            <div class="report-pages">
                <i class="fas fa-file-alt"></i>
                <?php
                echo "Pages : " . $row["pages"] . "";
?>
           </div>
            <br>
            <div class="report-des">
                <?php
                echo utf8_encode(substr(strip_tags(str_replace("_x000D_", " ", $row['summary'])), 0, 350));
?>...
            </div>
            <br>
        </div>
        <?php
            }
        }
?>

<!-- -->
       <br>
      
        <?php
        $sql = "SELECT COUNT(*) FROM reports where cat_id='" . $chk . "'";
        $rs_result      = mysqli_query($con, $sql);
        $row = mysqli_fetch_row($rs_result);
        $total_records =$row[0];

        $total_pages   = ceil($total_records / $limit);
        $current_page  = $page;
        $c1            = rtrim(str_replace(" ", "-", $industryname));
        $c1            = rtrim(str_replace("&", "and", $c1));
        $str1      = "category?";
        $finalStr1 = str_replace('?', '/', $str1);
        if ($page == 1) {
            $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline;' class='disabled'><a>Previous</a></li>";
        } else {
            $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline'><a href='$base_url$finalStr1$c1-market-reports/" . ($page - 1) . "'>Previous</a></li>";
        }
        
        for ($i = max(1, $page - 4); $i <= min($page + 4, $total_pages); $i++) {
            
            $active = '';
            if (isset($page1) && $i == $page1) {
                $active = 'class="active"';
            }
            
            $pagLink .= "<li $active style='display:inline'>&nbsp;<a href='$base_url$finalStr1$c1-market-reports/" . $i . "'>" . $i . "&nbsp;</a></li>";
        }
        ;
        if ($page < $total_pages) {
            echo $pagLink . "<li style='display:inline'><a href='$base_url$finalStr1$c1-market-reports/" . ($page + 1) . "'>NEXT</a></li> </ul></div>";
        } else {
            if ($page > $total_pages) {
                echo '<script type="text/javascript"> window.location.href="https://www.acquiremarketresearch.com/" </script>';
            }
            
            echo $pagLink . "<li style='display:inline' class='disabled'><a>NEXT</a></li> </ul></div>";
            
        }
        
?><br><br><br>
        <div><b>
            <?php
        echo "Page No : " . $current_page . " / " . $total_pages;
        
?></b>
        </div>
        <?php
 //   }
//}
?>
   </div>
 <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" id="all_card">
       
        <div class="w3-card-4" id="all_report_card">
            <header class="w3-container w3">
                <h3>
                    <p class="center"><b>Happy To Assist You 24/7</b></p>
                </h3>
            </header>
            <div class="w3-container center">
                
                    <img src="<?php
echo $base_url;
?>images/success-support.png" alt="market research"class="img-responsive center"/>
                    <b>
                        <!--a href="mailto:sales@acquiremarketresearch.com"><i class="fas fa-envelope"></i>&nbsp;&nbsp;
                           <a href="javascript:location='mailto:\u0073\u0061\u006c\u0065\u0073\u0040\u0061\u0063\u0071\u0075\u0069\u0072\u0065\u006d\u0061\u0072\u006b\u0065\u0074\u0072\u0065\u0073\u0065\u0061\u0072\u0063\u0068\u002e\u0063\u006f\u006d';void 0"><script type="text/javascript">document.write('\u0073\u0061\u006c\u0065\u0073\u0040\u0061\u0063\u0071\u0075\u0069\u0072\u0065\u006d\u0061\u0072\u006b\u0065\u0074\u0072\u0065\u0073\u0065\u0061\u0072\u0063\u0068\u002e\u0063\u006f\u006d')</script></a>&nbsp;&nbsp;</a-->
                        <i class="fas fa-envelope" id="imgi"></i>&nbsp;&nbsp;<strong id="imgi">sales@acquiremarketresearch.com</strong>
                        
                        <div><br></div>
                        
                        <i id="imgi" class="glyphicon glyphicon-phone-alt"></i><!--a href="skype:+1-800-663-5579 call"-->&nbsp;<b id="imgi">+1-800-663-5579</b><!--/a-->
                    </b>
                
            </div>
        </div>
        <br><br>

<!-- latest report css -->
<style>
 .htwo
{
	font-size:18px;
	font-weight:700;
}

 .pr-widget-title{background:#0077b5;position:relative;font-size:18px;font-weight:700;color:#fff;text-transform:uppercase;padding-top:10px;padding-bottom:10px;padding-left:10px;}
   .pr-panel-bottom {
    border-bottom: 5px solid #0077b5;
    padding-bottom: 5px;
}
.pr-report {
    line-height: normal!important;
    color: #0077B5;
    overflow: hidden;
    margin: 0;
    font-size: .8em!important;
    display: block;
    font-weight: 700;
    padding-left: 15px;
    padding-top: 15px;
}
.pr-panel:nth-child(odd) {
    background-color: #FFF;
}

.pr-panel {
    border-width: .5px;
    border-style: solid;
    border-color: #F2F1F1;
    padding-bottom: 10px;
    font-size: 17px;
}
.pr-date {
    color: #E6751E;
    font-size: 14px;
    margin-left: 45px;
    display: inline-block;
    padding-top: 3px;
}
.pr-pages {
    color: #E6751E;
    padding-bottom: 10px;
}

.pr-des, .pr-pages {
    font-size: 14px;
    margin-left: 25px;
    display: inline-block;
} 
</style>


        <div class="col-md-9 col-lg-9 col-xs-12 col-sm-12">
                   <section id="reports">
                    <div class="pr-widget-title">
                        <h2 class="htwo"><b>Latest Published Reports</b></h2>
                    </div>
                    <div class="pr-panel-bottom"></div>
                    
                    <?php
                        $sql = "SELECT * FROM reports order by report_id DESC limit 5";
                        $result = $con->query($sql);
                        
                         if ($result->num_rows > 0) {                
                        while($row = $result->fetch_assoc()) {
                        ?>  
                    <div class="pr-panel">
                        <div class="pr-report">
                            <?php 
                               
                                $finalStr="industry-reports/";
                                $urltitle = prepareSlugUrl($row['time'], $row['report_title']);
                                $urltitle= str_replace(" ", "-",$urltitle);
                                $urltitle= str_replace("(", "-",$urltitle);
                                $urltitle= str_replace(")", "-",$urltitle);
                                $urltitle= str_replace("?", "-",$urltitle);
                                $urltitle= str_replace("!", "-",$urltitle);
                                $urltitle= str_replace("@", "-",$urltitle);
                                $urltitle= str_replace("#", "-",$urltitle);
                                $urltitle= str_replace("$", "-",$urltitle);
                                $urltitle= str_replace("%", "-",$urltitle);
                                $urltitle= str_replace("^", "-",$urltitle);
                                $urltitle= str_replace("*", "-",$urltitle);
                                $urltitle= str_replace("'", "-",$urltitle);
                                $urltitle= str_replace("+", "-",$urltitle);
                                $urltitle= str_replace("&","and",$urltitle);
                                $urltitle=str_replace("/","-",$urltitle);
                                $urltitle=str_replace(":","-",$urltitle);
                                $urltitle=str_replace(".","-",$urltitle);
                                $urltitle=str_replace("<","-",$urltitle);
                                $urltitle=str_replace(">","-",$urltitle);
                                $urltitle=str_replace("'","-",$urltitle);
                                $urltitle=str_replace("|","-",$urltitle);
                                $urltitle=str_replace("]","-",$urltitle);
                                $urltitle=str_replace("[","-",$urltitle);
                                $urltitle=str_replace("}","-",$urltitle);
                                $urltitle=str_replace("{","-",$urltitle);
                                $urltitle=str_replace(";","-",$urltitle);
                                $urltitle=str_replace("_","-",$urltitle);
                                $urltitle=str_replace("_x000D_","-",$urltitle);
            
                                $urltitle=str_replace("--","-",$urltitle);
                                $urltitle1=str_replace("---","-",$urltitle);
                                ?>  
                            <i id="reporti" class="fa fa-chart-line"></i>  
                            <a href="<?php echo $base_url;?><?php echo $finalStr;?><?php echo $urltitle1;?>/<?php echo $row['report_id'];?>">
                            <?php
                          $rtitleindex=  str_replace("?"," ",$row['report_title']); 
                        $rtitleindex=str_replace("_x000D_", " ", $rtitleindex); 
                        echo $rtitleindex;         
                                ?>
                            </a>
                        </div>
                        <div class="pr-date">
                            <i class="fas fa-calendar-alt"></i>
                            <?php
                                echo "Published Date : " .  $row["published_date"]. "";
                                ?>
                        </div>
                        <div class="pr-pages">
                            <i class="fas fa-file-alt"></i>
                            <?php
                                echo "Pages : ". $row["pages"]. "";
                                ?>
                        </div>
                        <br>
                        <div class="pr-des">
                            <?php
                                echo utf8_encode(substr(strip_tags(str_replace("_x000D_"," ",$row['summary'])),0,180));
                                ?>...<!--a href="<?php echo $base_url;?><?php echo $finalStr?><?php echo $urltitle1;?>/<?php echo $row['report_id'];?>">Acquire more information... </a-->
                        </div>
                        <br>
                    </div>
                    <?php 
                        }
                        }
                       
                       ?>
                    <div class="more-report margin-bottom10">
                        <br>
                      <?php  
                          
                            $finalStrreport= "reports/";
                            ?> 
                        <a href="<?php echo $base_url;?><?php echo $finalStrreport;?>1"><button class="btn btn-primary" id="vmp">View More Published Reports&nbsp;<i class="fas fa-angle-double-right"></i></button></a>
                    </div>
                    </section>
                </div>


        <!--<div class="w3-card-4" id="enq_form">
            <header class="w3-container w3">
                <h4>
                    <p class="center">
                        <b>
                            	To Make a quick Enquiry
                            <p id="enqi" class="center">(By filling up the form below)</p>
                        </b>
                    </p>
                </h4>
            </header>
            <form action="https://www.acquiremarketresearch.com/market-analysis-thank-you/" method="POST">
                
                <div class="form-group" id="pr_form">
                    <div class="w3-container">
                        <label>Full Name*</label><br>
                        <input type="text" id="fname" name="firstname" placeholder="Full Name" onchange="return IsAlpha(event);" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;" size="37"  required/><span id="error_name">* Enter Valid Name</span>
                        <br><br><label><b>Email*</b></label><br>
                        <input type="email" name="bemail" placeholder="Email" size="37" required />
                    <br><br><label><b>Company Name*</b></label><br>
                        <input type="text" name="cname" placeholder="Company Name" size="31" required />
                        <br><br><label><b>Designation*</b></label><br>
                        <input type="text" name="desig" placeholder="Designation" size="31" required />
                        
                        <br><br><label><b>Phone*</b></label><br><input type="text" id="ph" name="ph" placeholder="Phone number" onchange="return IsNumeric(event);" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="21" size="37" required /><span id="error_ph">* Input digits (0 - 9)</span>
                        <br><br><label><b>Research Requirements</b></label><br>
                        <textarea  col="3" rows="3" name="qur" placeholder="Type your query here"></textarea>
                        <br><br>
                        <button type="submit" class="btn btn-primary center" name="submit">Submit</button>
                        <br><br>
                    </div>
                </div>
            </form>
        </div>-->
    </div>
</div>
<br><br> 
<?php
function limit_words($string, $word_limit)
{
    $words = str_replace(",", " ", $string);
    $words = explode(" ", $words);
    
    return implode(" ", array_splice($words, 0, $word_limit));
}

?>
<script language='JavaScript' type='text/javascript'>
    var specialKeys = new Array();
    specialKeys.push(8);
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_ph").style.display = ret ? "none" : "inline";
        return ret;
    }
    
    function IsAlpha(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode > 64 && keyCode < 91) ||(keyCode==32)|| (keyCode > 96 && keyCode < 123) ||specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_name").style.display = ret ? "none" : "inline";
        return ret;
    }
</script>
<?php
include 'footer_file.php';
?>