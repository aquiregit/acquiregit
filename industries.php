<?php
    $pagetitle="AMR Industries | Acquire market Research";
    $desc="Acquire Market Research caters to varied industries worldwide with in-depth research reports and analytical data to business leaders for organizational growth";
    $key="Industry, Agriculture, Automobile &amp; Transportation, Chemical &amp; Material,Consumer Goods, Electronics &amp; Semiconductor, Energy &amp; Power, Food &amp;Beverages, Internet &amp; Communication, Machinery &amp; Equipment, Medical Care,Medical Devices &amp; Consumables, Pharma &amp; Healthcare, Service &amp; Software";
       include "header_file.php";  
       ?>
<div class="container"></div>
<div class="container">
    <br><br>
    <style type="text/css">
       #cat,#pop{font-size:30px;font-weight:700}#cat,#cattitle,#pop{font-weight:700}#pop{color:#0077b5}#cat{color:#87cefa}#category .well{background:linear-gradient(to top,#ccf5ff 0,#fff 100%)}#category .card-title #cattitle{font-size:1vw;font-color:#000;text-align:left-side}#fai{font-size:30px}#cattitle{size:22px}

         @media screen and (min-width:760px) and (max-width:980px){#category .card-title #cattitle{font-size: 2.5vw}}
       @media screen and (min-width:350px) and (max-width:760px){#category .card-title #cattitle{font-size:2.5vw}
       @media screen and (max-width:350px){#category .card-title #cattitle{font-size:2.5vw}
    </style>
    <p class="center">
        <span id="pop">Popular</span>&nbsp;&nbsp;
        <span id="cat">Categories</span><br>
    </p>
    <br> <br> 
    <?php
        $sql ="SELECT * FROM categories ORDER BY cat_name ASC";
        $result=mysqli_query($con,$sql);
        if ($result->num_rows > 0) {
          while($row=$result->fetch_assoc())
         {
            $flag1="<br>";
           $flag1="";$flag2="";$flag3="";
            $str=rtrim(str_replace("_x000D_","",$row['cat_name']));
           
            $url=rtrim(str_replace(" ","-",$str));
           $url=rtrim(str_replace("&","and",$url));
           $url=strtolower($url);
        ?>
    <div id="category" >
        <div class="well col-md-3 col-lg-3 col-sm-3 col-xs-6">
            <?php
                
                $finalStrin="category/";
                ?>
            <a href="<?php echo $base_url;?><?php echo $finalStrin;?><?php echo $url;?><?php echo "-market-reports";?>/1">
                <p class="center"><i id="fai" class="fas fa-2x  <?php if($row['fa_fa_font']!='') echo $row['fa_fa_font'];else echo 'fas fa-file'; ?>"></i></p>
                <div class="card-title">
                    <p class="center">
                        <t id="cattitle" class="text-center">
                            <?php echo $row['cat_name'] ?>
                        </t>
                    </p>
                </div>
            </a>
        </div>
    </div>
    <?php
        }
        }
        ?> 
</div>
<?php include'footer_file.php'; ?>