<?php include 'header_file.php'; ?>

<style>
@import url('https://fonts.googleapis.com/css?family=Noto+Sans:700');
@import url('https://fonts.googleapis.com/css?family=Lato');

.err_page
{
	width:100%;
  height:80%;
	margin:4% auto;
    margin-top:50px;
  font-family: 'Lato', sans-serif;
	background:#fff;
	text-align:center;
	display:flex;
	align-items: center;
}
.err_page_right
{
	width:100%;
}
.err_page_left
{
		width:100%;
}
.err_page h1
{
	font-family: 'Noto Sans', sans-serif;
	font-size:70pt;
	margin:0;
	color:#0077b5;
}
.err_page h4
{	color:#0077b5;
	font-size:14pt;
}
.err_page p
{
	font-size:14pt;
	color: #737373;
}
.err_btn
{
	background:#fff;
	border:2px solid #0077b5;
	padding:10px;

	border-radius:5px;
	box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
	cursor:pointer;
	font-size:13pt;
	transition:background 0.5s;
}
.err_btn:hover
{
	background:#0077b5;
  box-shadow: 0px 8px 10px rgba(0, 0, 0, 0.2);
	color:#fff;
}

@media screen and (max-width: 800px)
{
  .err_page
  {
     flex-direction:column;  
  }
  .err_page_left img
  {
       width:250px;
      height:auto;
  }
  .err_page h4
  {
    margin:0;
  }
}
</style>
<div class="err_page">
		<div class="err_page_left">
			<img src="<?php echo $base_url;?>images/err.png" width=360px height=250px/>
		</div>
		<div class="err_page_right">
			<h1>404</h1>
			<h4>OOPS. Looks like you landed on a wrong page</h4>
			<p>Don't worry. Since you're valuable to us we will bring you back to safety</p>
			<button class="err_btn" onclick="location.href = '<?php echo $base_url;?>';">Back to home</button>
		</div>
		</div>
		
<?php include 'footer_file.php'; ?>