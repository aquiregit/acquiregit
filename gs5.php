<?php
   include 'connect.php';
   $base_url = "https://www.acquiremarketresearch.com/";

  /*category.php*/
   $qu="SELECT * FROM categories";
   $result=$con->query($qu);
   
        if ($result->num_rows > 0) 
        {                
          while($row = $result->fetch_assoc()) 
          {
               switch ($row['cat_name']) 
               {
                   case 'Automobile & Transportation':$quauto="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                                     $resultauto=$con->query($quauto);
                                     $tiauto="";
                              if ($resultauto->num_rows > 0) 
                              {      
                                $xml=new DOMDocument("1.0","utf-8");
                                $xml->formatOutput=true;
                                 /*urlset*/
           
                               $urlset=$xml->createElement("urlset");
                               $xml->appendChild($urlset);
                               $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
           
                                    /*index.php*/
                               $url=$xml->createElement("url");
                                $urlset->appendChild($url);
                               $loc=$xml->createElement("loc",$base_url);
                               $url->appendChild($loc);
                               $prt=$xml->createElement("priority","1.0");
                               $url->appendChild($prt);
             
                                  while($rowauto = $resultauto->fetch_assoc()) 
                                  {
                                    $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowauto['report_title']),8)));
                                    $urltitle= str_replace("(", "-",$urltitle);
                                    $urltitle= str_replace(")", "-",$urltitle);
                                    $urltitle= str_replace("?", "-",$urltitle);
                                    $urltitle= str_replace("!", "-",$urltitle);
                                    $urltitle= str_replace("@", "-",$urltitle);
                                    $urltitle= str_replace("#", "-",$urltitle);
                                    $urltitle= str_replace("$", "-",$urltitle);
                                    $urltitle= str_replace("%", "-",$urltitle);
                                    $urltitle= str_replace("^", "-",$urltitle);
                                    $urltitle= str_replace("*", "-",$urltitle);
                                    $urltitle= str_replace("'", "-",$urltitle);
                                    $urltitle= str_replace("+", "-",$urltitle);
                                    $urltitle= str_replace("&","and",$urltitle);
                                    $urltitle=str_replace("/","-",$urltitle);
                                    $urltitle=str_replace(":","-",$urltitle);
                                    $urltitle=str_replace(".","-",$urltitle);
                                    $urltitle=str_replace("<","-",$urltitle);
                                    $urltitle=str_replace(">","-",$urltitle);
                                    $urltitle=str_replace("'","-",$urltitle);
                                    $urltitle=str_replace("|","-",$urltitle);
                                    $urltitle=str_replace("]","-",$urltitle);
                                    $urltitle=str_replace("[","-",$urltitle);
                                    $urltitle=str_replace("}","-",$urltitle);
                                    $urltitle=str_replace("{","-",$urltitle);
                                    $urltitle=str_replace(";","-",$urltitle);
                                    $urltitle=str_replace("_","-",$urltitle);
                                    $urltitle=str_replace("_x000D_","-",$urltitle);
                                    
                                   
                            
                                    $urltitle= str_replace("--","-",$urltitle);
                                    $urltitle1= str_replace("---","-",$urltitle);
                                    
                                    $reportauto=$xml->createElement("url");
                                     $urlset->appendChild($reportauto);
                                     $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowauto['report_id']);
                                     $reportauto->appendChild($loc);
                     
                                     $prt=$xml->createElement("priority","0.9");
                                     $reportauto->appendChild($prt);
                                     $tiauto=str_replace("&","and", $rowauto['catlog']);
                                      $tiauto=str_replace(" ","-", $tiauto);
                                      $tiauto=strtolower($tiauto);
                                
                                 if($rowauto['catlog']=='Automobile & Transportation')            
                                         $xml->save($tiauto."-market-reports.xml");
                                  }
                              }
                                                    break;
                case 'Consumer Goods':$qucg="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                $resultcg=$con->query($qucg);
                $ticg="";
                if ($resultcg->num_rows > 0) 
                {  $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);              
                  while($rowcg = $resultcg->fetch_assoc()) 
                  {
                    $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowcg['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);
                    
                    $reportcg=$xml->createElement("url");
                   $urlset->appendChild($reportcg);
                   $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowcg['report_id']);
                   $reportcg->appendChild($loc);
   
                   $prt=$xml->createElement("priority","0.9");
                   $reportcg->appendChild($prt);
                    $ticg=str_replace("&","and", $rowcg['catlog']);
                    $ticg=str_replace(" ","-", $ticg);
                    $ticg=strtolower($ticg);
              
               if($rowcg['catlog']=='Consumer Goods')            
                       $xml->save($ticg."-market-reports.xml");
                  }
                }
                        break;

            case 'Electronics & Semiconductor':$ques="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                $resultes=$con->query($ques);
                $ties="";
                if ($resultes->num_rows > 0) 
                {  $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);              
                  while($rowes = $resultes->fetch_assoc()) 
                  {
                    $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowes['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);
                    
                    $reportes=$xml->createElement("url");
                   $urlset->appendChild($reportes);
                   $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowes['report_id']);
                   $reportes->appendChild($loc);
   
                   $prt=$xml->createElement("priority","0.9");
                   $reportes->appendChild($prt);
                    $ties=str_replace("&","and", $rowes['catlog']);
                    $ties=str_replace(" ","-", $ties);
                    $ties=strtolower($ties);
              
               if($rowes['catlog']=='Electronics & Semiconductor')            
                       $xml->save($ties."-market-reports.xml");
                }
              }
                                                 break;
         
            case 'IT & Telecom':$qufb="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
               $resultfb=$con->query($qufb);
                $tifb="";
                if ($resultfb->num_rows > 0) 
                {                
                    while($rowfb = $resultfb->fetch_assoc()) 
                    {
                      $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowfb['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);

                    $reportfb=$xml->createElement("url");
                   $urlset->appendChild($reportfb);
                   $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowfb['report_id']);
                   $reportfb->appendChild($loc);
   
                   $prt=$xml->createElement("priority","0.9");
                   $reportfb->appendChild($prt);
                   $tifb=str_replace("&","and", $rowfb['catlog']);
                   $tifb=str_replace(" ","-", $tifb);
                   $tifb=strtolower($tifb);
                    if($rowfb['catlog']=='IT & Telecom')            
                          $xml->save($tifb."-market-reports.xml");
                    }
                }
                                break;
          
            case 'Pharma & Healthcare':$quph="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                      $resultph=$con->query($quph);
                    $tiph="";
                    if ($resultph->num_rows > 0) 
                   { $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);               
                    while($rowph = $resultph->fetch_assoc()) 
                    {
                      $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowph['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);

                    $reportph=$xml->createElement("url");
                   $urlset->appendChild($report);
                   $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowph['report_id']);
                   $reportph->appendChild($loc);
   
                   $prt=$xml->createElement("priority","0.9");
                   $reportph->appendChild($prt);
                   $tiph=str_replace("&","and", $rowph['catlog']);
                    $tiph=str_replace(" ","-", $tiph);
                    $tiph=strtolower($tiph);
                   if($rowph['catlog']=='Pharma & Healthcare')            
                       $xml->save($tiph."-market-reports.xml");
                    }
                  }
                        break;
          }
        }

}
function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
       }
?>