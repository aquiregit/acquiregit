<?php
$pagetitle = "Support Service | Acquire Market Research
    ";
$desc      = "Support Service | Acquire Market Research";
$key       = "AMR support, support services, Acquire market research support
    ";
include 'header_file.php';
?>
<style>
   #how_to_order p,#payment_options p,#support_cont p{text-align:justify;text-justify:inter-word;font-family:Lato;font-size:18px}#how_to_order li,#payment_options li,#support_cont li{margin-bottom:20px;line-height:25px;padding-left:1em;color:#000;font-size:18px}#how_to_order #con1,#payment_options #con2{padding-top:30px}#con1 span:nth-child(odd),#payment_options #con2 span:nth-child(odd){color:#87cefa;font-size:30px;font-weight:700}#con1 span:nth-child(even),#payment_options #con2 span:nth-child(even),#support_cont span{color:#0077b5;font-size:30px;font-weight:700}#con1 #plink{color:#00f;font-weight:700}#con1 #plink li{color:#87CEFA}#con1 #psec{color:#0077B5;font-weight:700}#how_to_order #con1 a,#payment_options #con2 a,#support_cont a{color:#00f}#hrline{border-width:4px}
</style>
<div class="container col-md-12">
    <br>
</div>
<div class="center wow fadeInDown">
    
    <p class="lead"></p>
</div>
<div class="container-fluid" id="how_to_order"><br><br>
    <div class="container" id="con1">
        <h3 class="center">
           <span>How</span>&nbsp;
                <span>To</span>&nbsp;
                <span>Order</span>
            
            <img src="https://www.acquiremarketresearch.com/images/shopping_cart.gif" alt="AMR support, support services, Acquire market research support" height=90px; width=90px; class="img-responsive center"/>
        </h3>
        <p><b>Secure Online Payment</b></p>
        <p>At Acquire, be rest assured that we work with secured gateways and the information you provide will be treated with utmost responsibility and only for the purchases permitted by you.</p>
        <p>You can have access to your desired information in just these 4 clicks –</p>
        <p id="plink">Browse Category >> Select Report >> Verify Details >> Pay</p>
        <p>You can place the order by:</p>
        <li><b>Email</b></li>
        <li><b>Call</b></li>
        <p>In case you are placing an order on company behalf, we expect you to have complete authorization to make the purchase decision.</p>
        <p id="psec">If you are ordering by Email</p>
        <p>We will require you to post all your requirements and queries on <a href="mailto:sales@acquiremarketresearch.com" id="salea"></a>&nbsp;&nbsp;
                          <script type="text/javascript">
emailE=('sales@' + 'acquiremarketresearch.com')
document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
 
</script> and most importantly remember to include all your details including billing & delivery address along with the preferred mode of payment. Our executives will promptly get in touch with you for further guidance in less than 24 hours.</p>
        <p id="psec">If you are ordering by phone</p>
        <p>Call us at 1-800-663-5579 (24-hour helpline) to place an order instantly or any queries related to the purchase.</p>
    </div>
</div><hr id="hrline">
<br>
<div class="container-fluid" id="payment_options">
    <div class="container" id="con2"><br><br>
        <h3 class="center">
            <span>Secure</span>&nbsp;
                <span>Payment</span>&nbsp;
                <span>Options</span><img src="https://www.acquiremarketresearch.com/images/payment_icon.gif" alt="AMR support, support services, Acquire market research support" height=100px; width=100px; class="img-responsive center"/> 
           
        </h3>
        <p>We offer a choice of payment modes made available to you are as follows:</p>
        <li>We accept major Credit cards - AMEX, Master Card, Visa</li>
        <li>We accept Bank Transfers - Transfers in USD can be made directly into our bank account</li>
        <p>Acquire MARKET RESEARCH will directly send the invoice to customers purchasing reports, by email and payments have to be made in full before the dispatch of the respective report is initiated from our side.</p>
        <p>We would also like to notify that currency exchange rates to fluctuate and are subject to change from time to time.</p>
        <br>
    </div>
</div>
<div id="sub"><hr id="hrline">
<div class="container" id="support_cont">
    <br>
    <h3 class="center">
        <span>Subscription</span><img src="https://www.acquiremarketresearch.com/images/hand.gif" alt="AMR support, support services, Acquire market research support" height=90px; width=90px; class="img-responsive center"/>
    </h3>
<p><b>Report Subscription form:</b></p>
<p>Acquire Market Research Subscription service offers you all the access to high profile market reports on various industries and the most trending package of publications worldwide companies to Acquire Market Research’s Library of on-going market data, analysis, and forecasts. Acquire Market Research's Subscription Service is designed especially for organizations like yours that need a comprehensive and well-rounded view of your respective industry and maximum insight into upcoming future trends. Each subscription is customized according to your organization's size, budget, and needs and includes access to newly published information. Clients can choose for themselves to access Acquire Market Research’s complete report catalog, select reports across multiple categories, or include particular reports in one category as part of their subscription with us.</p>
<p>E-mail us at sales@acquiremarketresearch.com
<!--<script type="text/javascript">
emailE=('sales@' + 'acquiremarketresearch.com')
document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
 </script>--> to discuss with us your own Custom Report Subscription Package.</p>
<p>For students looking to access our market research reports, please ask your Librarian to fill out the form along with the documents.</p>
<p>Report Subscription Benefits listed below :</p>
<ul>
    <li>Unlimited, enterprise-wide extensive access to over one thousand research reports in all varied market categories</li>
    <li>Flexibility to choose reports in the same category or across multiple industry areas depending upon you need</li>
    <li>A very Cost-effective method to adhere to your market research budget</li>
</ul>
</div>
</div>
<div id="disclaimer">
<hr id="hrline">
<br>
<div class="container-fluid" id="payment_options">
    <br>
<div class="container" id="support_cont">
    <h3 class="center">
        <span>Disclaimer</span><img src="https://www.acquiremarketresearch.com/images/disclaimer.gif" alt="AMR support, support services, Acquire market research support" height=60px; width=60px; class="img-responsive center"/>
    </h3>
    <p>The reports sold by Acquire Market Research are solely for internal use and as per the requirement of the client. In any case, they shall not be circulated for publication purposes or distribution to third parties without authorized written permission from Acquire Market Research.</p>
    <p>The client will be sent a preview of the report before purchasing and are requested to be sure of what specifically they require.</p>
    <p>The report or part of it shall not be resold on the client’s behalf or circulated, lent, rented or disclosed to non-customers of Acquire Market Research under any circumstances.</p>
    <p>Any kind of tampering with the report for photocopying, transferring to third party for changes or in any other form, recording or reproduction in any way should not be done without permission from the publisher.</p>
    <p>Any link contained in Acquire Market Research’s website that is related to the third party is not endorsed or promoted by Acquire Market Research in any way. Our clients are advised to check the authenticity of these links before having any correspondence with them without Acquire Market Research’s knowledge. These links are to be used only for informational purposes. Acquire Market Research shall not be liable to disclaim any compensation for loss or damage arising out of dealings with these links without Acquire Market Research’s consent.</p>
    <p>For any information related to permission or questions please contact:<a href="mailto:sales@acquiremarketresearch.com" id="salea"></a>&nbsp;&nbsp;
                          <script type="text/javascript">
emailE=('sales@' + 'acquiremarketresearch.com')
document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
 </script></p>
</div>
</div>
</div>
<br><br>
<?php
include 'footer_file.php';
?>