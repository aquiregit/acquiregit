<?php
$pagetitle = "AMR Press Release | Acquire Market Research";
$desc      = "Press Releases are Public statements Tracking Latest Industries and Markets in Terms of USD, Volume and CAGR Globally for Different Market Categories like Agriculture, Automobile and Transportation, Chemical and Material, Consumer Goods, Electronics and Semi-Conductor, Energy and Power, Food and Beverages, Internet and communication, Machinery and Equipment, Medical Care, Medical Devices and Consumables, Pharmacy and Healthcare, Service and Software at Acquire Market Research.";
$key       = "Press Releases, Market Research Press Releases, Media Press Release, Acquire Press Release, News";
?>
<?php
include 'header_file.php';
$data  = $_SERVER['REQUEST_URI'];
$page1 = explode('/', $data);
$page1 = $page1[2];
$limit = 15;
if (isset($page1)) {
    $page = $page1;
} else {
    $page = 1;
}
;
$start_from = ($page - 1) * $limit;
?>
<style> 
/*.imgcontainer{position:absolute;margin-top:130px;height:350px}*/.typewriter p{overflow:hidden;border-right:.15em solid transparent;white-space:nowrap;margin:0 auto;letter-spacing:.15em;text-align:center;animation:typing 3.5s steps(40,end),blink-caret .75s step-end;padding-top:30px;font-size:3em;font-weight:700}@keyframes typing{from{width:0}to{width:100%}}@keyframes blink-caret{100%,from,to{border-color:transparent}}@media screen and (min-width:1025px){.imgcontainer{position:static;margin-top:-16px;height:350px;z-index:1}}#ig{min-width:100%}@media screen and (min-width:981px) and (max-width:1024px){#div_rp{margin-top:-20px}}@media screen and (min-width:760px) and (max-width:980px){#div_rp{margin-left:-65px;margin-top:250px}}@media screen and (min-width:350px) and (max-width:760px){#div_rp{margin-left:-65px;margin-top:250px}}@media screen and (max-width:350px){#div_rp{z-index:1;margin-left:-65px;margin-top:250px}#pr_panel{margin-top:200px!important;padding-top:0!important;vertical-align:top}.typewriter{margin-bottom:-300px}}.pr-panel{border-width:.5px;border-style:solid;border-color:#F2F1F1;padding-bottom:5px;height:250px}.pr-panel:nth-child(odd){background-color:#e0e0e0}#icon,.pr-panel:nth-child(even){background-color:#fff}.pr-report{line-height:normal!important;color:#0077b5;overflow:hidden;margin:0;font-size:14px!important;display:block;font-weight:600!important;padding-left:15px;padding-top:15px}.pr-date,.pr-tags{font-size:14px;padding-bottom:10px}.pagination,.pr-date,.pr-img,.pr-tags{display:inline-block}.pr-date{color:#E6751E;padding-top:10px}.pr-img{margin-left:25px;float:left;margin-top:10px;border:2px solid #8c8c8c;margin-right:10px}.pr-tags{color:#0077b5;padding-left:20px;font-weight:700}@media screen and (min-width:760px) and (max-width:980px){.pr-date{margin-left:25px}.pr-panel{height:500px}}@media screen and (min-width:350px) and (max-width:760px){.pr-date{margin-left:25px}.pr-panel{height:500px}}@media screen and (max-width:350px){.pr-date{margin-left:25px}.pr-panel{height:500px}}#con1 #all_report_card img,#prig{height:150px}#icon{font-size:20px;color:#7fba00}#prig{width:250px}.pr-des{font-size:14px;text-align:justify;padding-right:10px}.pagination{padding-left:100px}.pagination a{color:#000;float:left;padding:8px 16px;text-decoration:none;transition:background-color .3s;border:1px solid #ddd}.pagination a.active{background-color:#0077b5;color:#fff;border:1px solid #4CAF50}.pagination a:hover:not(.active){background-color:#ddd}#con1{padding-left:30px}#con1 .w3-card,.w3-card-2{box-shadow:0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12)}#con1 .w3-card-4,.w3-hover-shadow:hover{box-shadow:0 4px 10px 0 rgba(0,0,0,.2),0 4px 20px 0 rgba(0,0,0,.19)}#con1 .w3-container{padding:.01em 16px}#enq_form h4,.pr-tags{padding-top:5px}#con1 #all_report_card{width:70%;background-color:#e0e0e0}#con1 #all_report_card h3{border-bottom:1px solid gray;padding-bottom:10px}#con1 #all_report_card a{color:#0B3C5D;font-size:1em}#icon2{font-size:15px;}#con1 a{color:#0077b5;text-decoration:none}#enq_form{width:70%}#enq_form h4{border-bottom:1px solid gray;padding-bottom:7px}#enq_form #ptag{font-size:12px}@media screen and (min-width:980px) and (max-width:1024px){#con1 #all_report_card{margin-top:-20px;margin-left:50px}#enq_form{margin-left:50px}#pr_form input{width:150px}}@media screen and (min-width:760px) and (max-width:980px){#con1 #all_report_card,#enq_form{margin-left:50px}#pr_form input{width:150px}}@media screen and (min-width:350px) and (max-width:760px){#con1 #all_report_card{margin-top:10px;width:80%;margin-left:25px}#enq_form{width:80%;margin-left:25px}#pr_form input{width:150px}}@media screen and (max-width:350px){#con1 #all_report_card{margin-top:10px;width:80%}#enq_form{width:80%}#pr_form input{width:150px}}#pr_form label{color:#0077b5;font-size:16px}#error_name,#error_ph{color:Red;display:none}#div_rp{padding-left: 80px;}
/*New css for title */
.pr-report {margin-top:15px;}
.pr-report a {font-weight:bold;}
.whyusrd {
  color: #0077b5;
  text-align: left;
  padding-left: 20px;
     font-size: 14px;
}
.center1{border-bottom:1px solid black;text-align:center;}
</style>
<div class="imgcontainer container-fluid">
<img src="<?php
echo $base_url;
?>images/press_releases.jpg"  alt="PR Banner" class="img-responsive" id="ig">
<div class="typewriter"><p>Press Releases</p><br></div>
<div class="container-fluid" id="pr_panel">
    <br>
    <div class="container col-md-8" id="div_rp">
        <?php
$sql    = "SELECT * FROM press_releases order by published_date DESC LIMIT $start_from, $limit";
$result = $con->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        
?>  
        <div class="pr-panel">
            <div class="pr-report">
                <i class="fas fa-file-alt" id="icon"></i>
                <?php
        $prtitle     = utf8_encode((str_replace(" ", "-", strtolower($row["pr_title"]))));
        $prtitle1    = str_replace("&", "and", $prtitle);
        $prtitle1    = str_replace("(", "-", $prtitle1);
        $prtitle1    = str_replace(")", "-", $prtitle1);
        $prtitle1    = str_replace("?", "-", $prtitle1);
        $prtitle1    = str_replace("!", "-", $prtitle1);
        $prtitle1    = str_replace("@", "-", $prtitle1);
        $prtitle1    = str_replace("#", "-", $prtitle1);
        $prtitle1    = str_replace("$", "-", $prtitle1);
        $prtitle1    = str_replace("%", "-", $prtitle1);
        $prtitle1    = str_replace("^", "-", $prtitle1);
        $prtitle1    = str_replace("*", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("+", "-", $prtitle1);
        $prtitle1    = str_replace("/", "-", $prtitle1);
        $prtitle1    = str_replace(":", "-", $prtitle1);
        $prtitle1    = str_replace(".", "-", $prtitle1);
        $prtitle1    = str_replace("<", "-", $prtitle1);
        $prtitle1    = str_replace(">", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("|", "-", $prtitle1);
        $prtitle1    = str_replace("]", "-", $prtitle1);
        $prtitle1    = str_replace("[", "-", $prtitle1);
        $prtitle1    = str_replace("}", "-", $prtitle1);
        $prtitle1    = str_replace("{", "-", $prtitle1);
        $prtitle1    = str_replace(";", "-", $prtitle1);
        $prtitle1    = str_replace("_", "-", $prtitle1);
        $prtitle1    = str_replace("_x000D_", "-", $prtitle1);
        $prtitle1    = str_replace("---", "-", $prtitle1);
        $prtitle1    = str_replace("--", "-", $prtitle1);
        $finalStrprv = "press-release/";
?>
               <a href="<?php echo $base_url;?><?php
        echo $finalStrprv;
?><?php
        echo $prtitle1;
?>/<?php
        echo $row['pr_id'];
?>/" rel="dofollow"><?php
        echo "" . utf8_encode($row["pr_title"]) . "";
?></a>
            </div><br>
            <div class="pr-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
        echo "Published date : " . $row["published_date"] . "";
?>
           </div>
            <div class="pr-img" style="width: 100px; border:0px;">
            <?php
        if ($row["pr-image"] != "") {
            echo "<img src='https://www.acquiremarketresearch.com/images/" . $row["pr-image"] . "' alt='PR Banner' id='prig'/>";
        } else {
            echo "<img src='https://www.acquiremarketresearch.com/images/Report_Icon1-4.jpg'  alt='PR Banner' class='img-responsive' style='min-width: 100%;padding-right: 10px;'/>";
        }
?>

   </div>
            <div class="pr-des">
                <?php
        echo utf8_encode(substr(strip_tags(stripslashes(str_replace("_x000D_", " ", $row['pr_desc']))), 0, 349));
        
?>...<!--a href="<?php
        echo $base_url;
?><?php
        echo $finalStrprv;
?><?php
        echo $prtitle1;
?>/<?php
        echo $row['pr_id'];
?>">Acquire more information...</a-->
            </div>
            <br>
          <!--  <div class="pr-tags" >
                <?php
    //    echo "Tags : " . $row['tags'] . "";
?>
           </div> -->
            <br>
        </div>
        <?php
    }
    
}

?> <br>
        <?php
$sql = "SELECT COUNT(*) FROM press_releases";

$rs_result     = mysqli_query($con, $sql);
$row           = mysqli_fetch_row($rs_result);
$total_records = $row[0];
$total_pages   = ceil($total_records / $limit);

$current_page = $page;
$strprs       = "press-releases?";
$finalStrprs  = str_replace('?', '/', $strprs);

if ($page == 1) {
    $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline' class='disabled'><a>Previous</a></li>";
} else {
    $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline'><a href='$base_url$finalStrprs" . ($page - 1) . "' id='pp'>Previous</a></li>";
}

for ($i = max(1, $page - 4); $i <= min($page + 4, $total_pages); $i++) {
    $active = '';
    if (isset($page1) && $i == $page1) {
        $active = 'class="active"';
    }
    
    $pagLink .= "<li $active style='display:inline'>&nbsp;<a href='$base_url$finalStrprs" . $i . "' >" . $i . "&nbsp;</a></li>";
    
}
;
if ($page < $total_pages) {
    echo $pagLink . "<li style='display:inline'><a href='$base_url$finalStrprs" . ($page + 1) . "' id='np'>NEXT</a></li> </ul></div>";
} else {
    if ($page > $total_pages) {
        echo '<script type="text/javascript"> window.location.href="https://www.acquiremarketresearch.com/" </script>';
    }
    echo $pagLink . "<li style='display:inline' class='disabled'><a>NEXT</a></li> </ul></div>";
}
?><br><br><br>
        <div><b>
            <?php
echo "Page No : " . $current_page . " / " . $total_pages;

?></b>
        </div>
    </div>
    <div class="container col-md-4"  id="con1">
        <div class="w3-card-4" id="all_report_card">
            <header class="w3-container w3"  >
            <h3><p class="center"><b>Happy To Assist You 24/7</b></p></h3>
            </header>
            <div class="w3-container center">
                    <img src="<?php
echo $base_url;
?>images/success-support.png" alt="Contact us" style="" class="img-responsive center"/>
                    <b> 
                       <!-- <i class="fas fa-envelope" id="icon2"></i><a href="mailto:sales@acquiremarketresearch.com"></a>&nbsp;&nbsp;
                          <script type="text/javascript">
                    
emailE=('sales@' + 'acquiremarketresearch.com')
document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')
 
</script> -->           <i class="fas fa-envelope"></i>&nbsp;&nbsp;<strong id="icon2">sales@acquiremarketresearch.com</strong>
                        <div><br></div>
                        <i class="glyphicon glyphicon-phone-alt" id="icon2"></i><!--a href="skype:+1-800-663-5579 call"-->&nbsp;<b id="icon2">+1-800-663-5579</b><!--/a-->
                    </b>
               
            </div>
        </div>
        <br><br>
        
        
        <div class="col-md-8 modal-content" id="securepay">
     
			<header class="w3-container w3"  id="con3">
                <h3>
                    <p class="center1"><b>Our Market Research Reports Include</b></p>
                </h3>
            </header>
    <p class="whyusrd">
						<i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Market snapshot<br>
						 <i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Market Segmentation<br>
						 <i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Value Chain Analysis<br>
						 <i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Growth Dynamics<br>
						 <i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Market Opportunities<br>
						<i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Regulatory Overview<br>
						 <i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Technology Evolution<br>
						<i class="fa fa-caret-right" style="font-size:20px"></i>&nbsp;&nbsp;Innovation & Sustainability<br>
						 </p>
						 
</div> 

        <!--<div class="w3-card-4" id="enq_form">
            <header class="w3-container w3">
                <h4>
                    <p class="center">
                        <b>
                          	To Make a quick Enquiry
                            <p class="center" id="ptag">(By filling up the form below)</p>
                        </b>
                    </p>
                </h4>
            </header>
            <form action="https://www.acquiremarketresearch.com/market-analysis-thank-you/"  method="POST">
                <div class="form-group" id="pr_form">
                    <div class="w3-container">
                        <label>Full Name*</label><br>
                        <input type="text" id="fname" name="firstname" placeholder="Full Name" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;" size="37"  required/><span id="error_name">* Enter Valid Name</span>
                        <br><br><label><b>Email*</b></label><br>
                        <input type="email" name="bemail" placeholder="Email" size="37" required />
                    <br><br><label><b>Company Name*</b></label><br>
                        <input type="text" name="cname" placeholder="Company Name" size="31" required />
                        <br><br><label><b>Designation*</b></label><br>
                        <input type="text" name="desig" placeholder="Designation" size="31" required />
                    <br><br><label><b>Phone*</b></label><br><input type="text" id="ph" name="ph" placeholder="Phone number"  onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="21" size="37" required /><span id="error_ph">* Input digits (0 - 9)</span>
                        <br><br><label><b>Research Requirements</b></label><br>
                        <textarea  col="3" rows="3" name="qur" placeholder="Type your query here" ></textarea>
                        <br><br>
                        <button type="submit" class="btn btn-primary center" name="submit">Submit</button>
                        <br><br>
                    </div>
                </div>
            </form>
        </div>-->
    </div>
</div>
<br><br>     
<script language='JavaScript' type='text/javascript'>
    var specialKeys = new Array();
    specialKeys.push(8);
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode==43) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_ph").style.display = ret ? "none" : "inline";
        return ret;
    }
    
    function IsAlpha(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode > 64 && keyCode < 91) || (keyCode==32)||(keyCode > 96 && keyCode < 123) ||specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_name").style.display = ret ? "none" : "inline";
        return ret;
    }
    
</script>
<?php
include 'footer_file.php';
?>