<?php include 'header_file.php';
    $limit = 15;  
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit; 
    ?>
<style>
    .imgcontainer {
    position: absolute;
    margin-top: 130px;
    height:350px;
    }
    .typewriter p{
    overflow: hidden; /* Ensures the content is not revealed until the animation */
    border-right: .15em solid transparent; /* The typwriter cursor */
    white-space: nowrap; /* Keeps the content on a single line */
    margin: 0 auto; /* Gives that scrolling effect as the typing happens */
    letter-spacing: .15em; /* Adjust as needed */
    text-align:center;
    animation: 
    typing 3.5s steps(40, end),
    blink-caret .75s step-end ;
    padding-top: 30px;
    font-size: 4em;
    font-weight: bold;
    }
    /* The typing effect */
    @keyframes typing {
    from { width: 0 }
    to { width: 100% }
    }
    /* The typewriter cursor effect */
    @keyframes blink-caret {
    from, to { border-color: transparent }
    100% { border-color: transparent; }
    }   
    /* Desktop */
    @media screen and (min-width: 980px) and (max-width: 1024px)
    {
    .imgcontainer
    {
    margin-top: 200px;
    }
    }
    /* Tablet*/ 
    @media screen and (min-width: 760px) and (max-width: 980px)
    {    
    .imgcontainer
    {
    margin-top: 0px;
    }
    }
    @media screen and (min-width: 760px) and (max-width: 980px)
    {
    .news-panel
    {
    margin-top: 0px!important;
    padding-top: 0px!important;
    vertical-align: top;
    }
    }
    @media screen and (min-width: 760px) and (max-width: 980px)
    {    
    .typewriter{
    margin-bottom: -300px;
    }
    }
    /* Mobile HD */
    @media screen and (min-width: 350px) and (max-width: 760px)
    {
    .imgcontainer
    {
    margin-top: 0px;
    }
    }
    @media screen and (min-width: 350px) and (max-width: 760px)
    {
    .news_panel
    {
    margin-top: 0px!important;
    padding-top: 0px!important;
    vertical-align: top;
    }
    }
    @media screen and (min-width: 350px) and (max-width: 760px)
    {
    .typewriter{
    margin-bottom: -300px;
    }
    }
    /* Mobile LD */
    @media screen and (max-width: 350px)
    {  
    .imgcontainer
    {
    margin-top: 0px;
    }
    }
    @media screen and (max-width: 350px)
    {
    .news-panel
    {
    margin-top: 0px!important;
    padding-top: 0px!important;
    vertical-align: top;
    }
    }
    @media screen and (max-width: 350px)
    {
    .typewriter{
    margin-bottom: -300px;
    }
    }
</style>
<div class="imgcontainer container-fluid">
    <img src="images/News.png" alt="market research" class="img-responsive" />
    <div class="typewriter">
        <p>News</p>
    </div>
</div>
<style type="text/css">
    /* Tablet */
    @media screen and (min-width: 760px) and (max-width: 980px)
    {
    #div_rp
    {
    margin-left: -65px;
    margin-top: -400px;
    }
    }
    /* Mobile HD */
    @media screen and (min-width: 350px) and (max-width: 760px)
    {
    #div_rp
    {
    margin-left: -65px;
    margin-top: -400px;
    }
    }
    /* Mobile LD */
    @media screen and (max-width: 350px)
    {
    #div_rp
    {      
    margin-left: -65px;
    margin-top: -400px;
    }
    }
</style>
<div class="container-fluid" style="padding-top: 550px;" id="pr_panel">
    <br><br><br>
    <div class="container col-md-8" style="padding-left: 80px;" id="div_rp">
        <style>
            .news-panel
            {
            border-width:0.5px;  
            border-style:solid;
            border-color:#F2F1F1;
            padding-bottom:5px;
            }
            .news-panel:nth-child(odd){background-color: #FFFFFF}  
            .news-panel:nth-child(even){background-color:#e0e0e0}
            .news-report
            {
            line-height: normal!important;
            color: #0077b5;
            overflow: hidden;
            margin: 0;
            font-size: 14px!important;
            display: block;
            font-weight: 600!important;
            padding-left: 15px;
            padding-top:15px;
            }
            .news-date 
            {
            color:#E6751E;
            font-size: 14px;
            margin-left: 45px;
            display: inline-block;
            padding-top: 10px;
            }
            .news-des {
            color: #000000;
            font-size: 14px;
            margin-left: 25px;
            display: inline-block;
            padding-top: 10px;
            margin-right: 15px;
            text-align: justify;
            }
            /* Tablet */
            @media screen and (min-width: 760px) and (max-width: 980px)
            {
            .news-date 
            {
            margin-left: 25px;
            }
            }
            /* Mobile HD */
            @media screen and (min-width: 350px) and (max-width: 760px)
            {
            .news-date 
            {
            margin-left: 25px;
            }
            }
            /* Mobile LD */
            @media screen and (max-width: 350px)
            {
            .news-date 
            {
            margin-left: 25px;
            }
            }
        </style>
        <?php
            $sql = "SELECT * FROM news order by published_date DESC LIMIT $start_from, $limit";
            $result = $con->query($sql);
            
             if ($result->num_rows > 0) {                
            while($row = $result->fetch_assoc()) {
            ?>  
        <div class="news-panel">
            <div class="news-report">
                <i class="fas fa-file-alt" style="font-size:20px;color:#7fba00; background-color:white;"></i> 
                <a href="News-Description.php?news_no=<?php echo $row["news_id"];?>&news_name=<?php echo (str_replace(" ", "_",$row["news_title"] )) ?>">
                <?php
                    echo "" . $row["news_title"]. "";           
                    ?>
                </a>
            </div>
            <div class="news-date">
                <i class="fas fa-calendar-alt"></i>
                <?php
                    echo "Published date : " .  $row["published_date"]. "";
                     ?>
            </div>
            <div class="news-des">
                <?php
                    echo stripslashes(str_replace("_x000D_"," ",$row['news_desc']));          
                    ?>...<a href="News-Description.php?news_no=<?php echo $row["news_id"];?>&news_name=<?php echo (str_replace(" ", "_",$row["news_title"] )) ?>">Read More</a>
            </div>
            <div class="news-report">
                <?php
                    echo "Tags : ".$row['tags'] ."";     
                    ?>
            </div>
        </div>
        <?php 
            }
            
            } 
            
            ?> 
        <style>
            .pagination {
            display: inline-block;
            padding-left: 100px;
            }
            .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            }
            .pagination a.active   {
            background-color: #0077b5;
            color: white;
            border: 1px solid #4CAF50;
            }
            .pagination a:hover:not(.active) {background-color: #ddd;}
        </style>
        <script type="text/javascript" src="flaviusmatis-simplePagination.js-e32c66e/jquery.js"></script>
        <script type="text/javascript" src="flaviusmatis-simplePagination.js-e32c66e/jquery.simplePagination.js"></script>
        <link type="text/css" rel="stylesheet" href="flaviusmatis-simplePagination.js-e32c66e/simplePagination.css"/>
        <?php  
            $sql = "SELECT COUNT(*) FROM news";  
            
            $rs_result = mysqli_query($con,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);  
            
            $current_page=$page;
            
            
            
            $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline'><a href='News.php?page=".($page-1)."' id='pp'>Previous</a></li>"; 
            
            for ($i=max(1,$page-4); $i<=min($page+4,$total_pages); $i++) {  
                      
                    
                      $pagLink .= "<li style='display:inline'>&nbsp;<a href='News.php?page=".$i."' >".$i."&nbsp;</a></li>";
                      
            }; 
                              
                 echo $pagLink . "<li style='display:inline'><a href='News.php?page=".($page+1)."' id='np'>NEXT</a></li> </ul></div>";  
               
            
            ?><br><br><br>
        <div><b>
            <?php
                echo "Page No : ".$current_page." / ".$total_pages;
                ?></b>
        </div>
    </div>
    <div class="container col-md-4"  style="padding-left: 30px;">
        <style>
            .w3-card,.w3-card-2{
            box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
            }
            .w3-card-4,.w3-hover-shadow:hover{
            box-shadow:0 4px 10px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19);
            }
            .w3-container{
            padding:0.01em 16px;
            }
            #all_report_card
            {
            width:70%;
            background-color:#e0e0e0;
            }
            #all_report_card h3
            {
            border-bottom: 1px solid gray; padding-bottom:10px;
            }
            #all_report_card a
            {
            color:#0B3C5D;
            font-size:1em;
            }
            #all_report_card img
            {
            height:150px;
            }
            /* Desktop */
            @media screen and (min-width: 980px) and (max-width: 1024px)
            {
            #all_report_card
            {
            margin-left: 50px;
            }
            }
            /* Tablet */
            @media screen and (min-width: 760px) and (max-width: 980px)
            {
            #all_report_card
            {
            margin-left: 50px;
            }
            }
            /* Mobile HD*/ 
            @media screen and (min-width: 350px) and (max-width: 760px)
            {
            #all_report_card 
            {
            margin-top: 10px;
            width: 100%;
            }
            }
            /* Mobile LD */
            @media screen and (max-width: 350px)
            {
            #all_report_card 
            {
            margin-top: 10px;
            width: 100%;
            }
            }
        </style>
        <div class="w3-card-4" style="" id="all_report_card">
            <header class="w3-container w3"  >
                <h3 style="">
                    <center><b>Happy To Assist You 24/7</b></center>
                </h3>
            </header>
            <div class="w3-container">
                <center>
                    <img src="images/success-support.png" alt="market research reports" style="" class="img-responsive"/>
                    <b>
                        <a  href="mailto: sales@accuintelmarketresearch.com" style=""><i class="fas fa-envelope" style="color:#0B3C5D"></i>&nbsp;sales@accuintelmarketresearch.com</a>
                        <div><br></div>
                        <i class="glyphicon glyphicon-phone-alt" style="color:#0B3C5D"></i><a href="skype:+1-800-663-5579 call" style="">&nbsp;+1-800-663-5579</a>
                    </b>
                </center>
            </div>
        </div>
        <br><br>
        <!--Add this to col-md-4-->
        <style type="text/css">
            #enq_form{
            width:70%;
            }
            /* Desktop */
            @media screen and (min-width: 980px) and (max-width: 1024px)
            {
            #enq_form
            {
            margin-left: 50px;
            }
            }
            /* Tablet */
            @media screen and (min-width: 760px) and (max-width: 980px)
            {
            #enq_form
            {
            margin-left: 50px;
            }
            }
            /* Mobile HD*/ 
            @media screen and (min-width: 350px) and (max-width: 760px)
            {
            #enq_form
            {
            width: 100%;
            }
            }
            /* Mobile LD */
            @media screen and (max-width: 350px)
            {
            #enq_form
            {
            width: 100%;
            }
            }
        </style>
        <div class="w3-card-4" id="enq_form">
            <header class="w3-container w3">
                <h4 style="border-bottom: 1px solid gray; padding-bottom:7px;padding-top:5px;">
                    <center>
                        <b>
                            To Get Free Market Analysis Sample
                            <p style="font-size:12px;">(By filling up the form below)</p>
                        </b>
                    </center>
                </h4>
            </header>
            <form action="enquiry_form_datasave.php" onsubmit="alert('Thank you! Our team will contact you shortly.')" method="POST">
                <style type="text/css">
                    /* Desktop */
                    @media screen and (min-width: 980px) and (max-width: 1024px)
                    {
                    #pr_form input
                    {
                    width:150px;
                    }
                    }
                    /* Tablet */
                    @media screen and (min-width: 760px) and (max-width: 980px)
                    {
                    #pr_form input
                    {
                    width:150px;
                    }
                    }
                    @media screen and (min-width: 350px) and (max-width: 760px)
                    {
                    #pr_form input
                    {
                    width:150px;
                    }
                    }
                    /* Mobile LD */
                    @media screen and (max-width: 350px)
                    {
                    #pr_form input
                    {
                    width:150px;
                    }
                    }
                </style>
                <div class="form-group" id="pr_form">
                    <div class="w3-container">
                        <label style="color:#0077b5; font-size: 16px;">Full Name*</label><br>
                        <input type="text" id="fname" name="firstname" placeholder="Full Name" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;" size="31"  required><span id="error_name" style="color: Red; display: none">* Enter Valid Name<br></span>
                        <br><br><label style="color:#0077b5; font-size: 16px;"><b>Business Email*</b></label><br>
                        <input type="email" name="bemail" placeholder="Email" size="31" required />
                        <br><br><label style="color:#0077b5; font-size: 16px;"><b>Company Name*</b></label><br>
                        <input type="text" name="cname" placeholder="Company Name" size="31" required />
                        <br><br><label style="color:#0077b5; font-size: 16px;"><b>Designation*</b></label><br>
                        <input type="text" name="desig" placeholder="Designation" size="31" required />
                        <br><br><label style="color:#0077b5; font-size: 16px;"><b>Phone*</b></label><br><input type="text" id="ph" name="ph" placeholder="Phone number"   onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="21" size="31" required /><span id="error_ph" style="color: Red; display: none">* Input digits (0 - 9)</span>
                        <br><br><label style="color:#0077b5; font-size: 16px;"><b>Research Requirements*</b></label><br>
                        <textarea  col="3" rows="3" name="qur" placeholder="Type your query here" required ></textarea>
                        <br><br>
                        <center><button type="submit" class="btn btn-primary" name="submit">Submit</button></center>
                        <br><br>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--End of form-->
</div>
<br><br>
<script language='JavaScript' type='text/javascript'>
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_ph").style.display = ret ? "none" : "inline";
        return ret;
    }
    
    function IsAlpha(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode > 64 && keyCode < 91) || (keyCode==32)||(keyCode > 96 && keyCode < 123) ||specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error_name").style.display = ret ? "none" : "inline";
        return ret;
    }
    
</script>
<?php include 'footer_file.php';?>