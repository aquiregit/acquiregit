<?php

include 'connect.php';
//$base_url="http://localhost/acquire/";
$base_url = "https://www.acquiremarketresearch.com/";

$xml=new DOMDocument("1.0","utf-8");
$xml->formatOutput=true;
/*sitemapindex*/

 $urlset=$xml->createElement("sitemapindex");
$xml->appendChild($urlset);
$xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");

/*index.php*/
$url=$xml->createElement("sitemap");
 $urlset->appendChild($url);
$loc=$xml->createElement("loc",$base_url."main_pages.xml");
$url->appendChild($loc);

/*category.php*/
$qu="SELECT * FROM categories";
$result=$con->query($qu);

     if ($result->num_rows > 0) {                
            while($row = $result->fetch_assoc()) {
                $ti=str_replace("&","and", $row['cat_name']);
                $ti=str_replace(" ","-", $ti);
                $ti=strtolower($ti);
                $category=$xml->createElement("sitemap");
                $urlset->appendChild($category);
                $name=$xml->createElement("loc",$base_url.$ti."-market-reports.xml");
                $category->appendChild($name);
            }
        }


/*pr.php*/
$url=$xml->createElement("sitemap");
 $urlset->appendChild($url);
$loc=$xml->createElement("loc",$base_url."press-releases.xml");
$url->appendChild($loc);

/*blog.php*/
$url=$xml->createElement("sitemap");
 $urlset->appendChild($url);
$loc=$xml->createElement("loc",$base_url."blogs.xml");
$url->appendChild($loc);

$xml->save("sitemap.xml");
 
function limit_words($string, $word_limit)
    {
        $words=str_replace(","," ",$string);  
        $words = explode(" ",$words);
    
        return implode(" ", array_splice($words, 0, $word_limit));
    }

?>