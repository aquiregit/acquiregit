<?php
  ini_set('max_execution_time', 900);
    include 'connect.php';
   $base_url = "https://www.acquiremarketresearch.com/";

  $qufb="SELECT * FROM reports WHERE catlog='Food & Beverages'";
      $resultfb=$con->query($qufb);
      $tifb="";
      if($resultfb->num_rows > 0)
      {
        $xml=new DOMDocument("1.0","utf-8");
        $xml->formatOutput=true;
            /*urlset*/
          $urlset=$xml->createElement("urlset");
          $xml->appendChild($urlset);
          $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
            /*index.php*/
          $url=$xml->createElement("url");
          $urlset->appendChild($url);
          $loc=$xml->createElement("loc",$base_url);
          $url->appendChild($loc);
          $prt=$xml->createElement("priority","1.0");
          $url->appendChild($prt);
          while($rowfb = $resultfb->fetch_assoc()) 
         {
            $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowfb['report_title']),8)));
            $urltitle= str_replace("--","-",$urltitle);
           $urltitle= str_replace("---","-",$urltitle);
             $urltitle= str_replace("(", "-",$urltitle);
              $urltitle= str_replace(")", "-",$urltitle);
              $urltitle= str_replace("?", "-",$urltitle);
                                    $urltitle= str_replace("!", "-",$urltitle);
                                    $urltitle= str_replace("@", "-",$urltitle);
                                    $urltitle= str_replace("#", "-",$urltitle);
                                    $urltitle= str_replace("$", "-",$urltitle);
                                    $urltitle= str_replace("%", "-",$urltitle);
                                    $urltitle= str_replace("^", "-",$urltitle);
                                    $urltitle= str_replace("*", "-",$urltitle);
                                    $urltitle= str_replace("'", "-",$urltitle);
                                    $urltitle= str_replace("+", "-",$urltitle);
                                    $urltitle= str_replace("&","and",$urltitle);
                                    $urltitle=str_replace("/","-",$urltitle);
                                    $urltitle=str_replace(":","-",$urltitle);
                                    $urltitle=str_replace(".","-",$urltitle);
                                    $urltitle=str_replace("<","-",$urltitle);
                                    $urltitle=str_replace(">","-",$urltitle);
                                    $urltitle=str_replace("'","-",$urltitle);
                                    $urltitle=str_replace("|","-",$urltitle);
                                    $urltitle=str_replace("]","-",$urltitle);
                                    $urltitle=str_replace("[","-",$urltitle);
                                    $urltitle=str_replace("}","-",$urltitle);
                                    $urltitle=str_replace("{","-",$urltitle);
                                    $urltitle=str_replace(";","-",$urltitle);
                                    $urltitle=str_replace("_","-",$urltitle);
                                    $urltitle=str_replace("_x000D_","-",$urltitle);
                                    $urltitle= str_replace("--","-",$urltitle);
                                    $urltitle1= str_replace("---","-",$urltitle);
                                     $reportfb=$xml->createElement("url");
                                      $urlset->appendChild($reportfb);
                                     $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowfb['report_id']);
                                     $reportfb->appendChild($loc);
                     
                                     $prt=$xml->createElement("priority","0.9");
                                     $reportfb->appendChild($prt);
                                     $tifb=str_replace("&","and", $rowfb['catlog']);
                                      $tifb=str_replace(" ","-", $tifb);
                                      $tifb=strtolower($tifb);
                                      
         }
$xml->save($tifb."-market-reports.xml");
      } 
     function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
}
?>