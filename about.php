<?php 
    $pagetitle="About Us | Acquire Market Research";
    $desc="About Us | Acquire Market Research";
    $key="About us";
    include 'header_file.php';?>
<div class="container">
    <div class="row">
        <p></p>
    </div>
</div>
<style>.imgcontainer{position:relative;text-align:center;color:#fff;margin-top:-27px;}#menu1 .colored-text,#menu1 p t{color:#0077b5;font-size:22px;}.imgcontainer img{width:100%;height:400px}.centered{position:absolute;top:50%;left:50%;text-shadow:2px 2px black;font-size:70px;transform:translate(-50%,-50%)}#abtcon li,#menu1 p t{font-size:15px}@media screen and (min-width:1024px){.imgcontainer{background-image:url(images/photo-1414919823178-e9d9d0afd0ac.jpg)}}@media screen and (min-width:980px) and (max-width:1024px){.imgcontainer{background-image:url(images/photo-1414919823178-e9d9d0afd0ac.jpg)}}@media screen and (min-width:760px) and (max-width:980px){.imgcontainer{background-image:url(images/photo-1414919823178-e9d9d0afd0ac.jpg)}}@media screen and (min-width:350px) and (max-width:760px){.imgcontainer{background-image:url(images/photo-1414919823178-e9d9d0afd0ac.jpg)}}@media screen and (max-width:350px){.imgcontainer{background-image:url(images/photo-1414919823178-e9d9d0afd0ac.jpg)}}#pclasscontent{text-align:justify;text-justify:inter-word;font-style:normal;color:#000;margin-left:150px;margin-right:150px;margin-top:30px;font-size:18px}@media only screen and (max-width:767px){#pclasscontent{margin-left:0;margin-right:0;margin-top:0;text-align:justify;text-justify:inter-word}}
    .cntr{text-align:center;font-weight:bold;font-size:22px;}
    #fn {color:#0077b5;font-size:22px;}
</style>
<div class="imgcontainer">
    <img src="<?php echo $base_url;?>images/About_Us2.jpg" alt="market research">
    <div class="centered">
        <p>Who We Are ?</p>
    </div>
</div>
<div class="container" id="abtcon">
    <br>
        <!--ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#menu1">About Us</a></li>
        </ul-->
      <div class="tab-content">
        <div id="menu1" class="tab-pane fade in active">
            <!--h2>
                <span class="colored-text"><p class="center">ACQUIRE MARKET RESEARCH</p></span>
            </h2-->
            <p class="center" id="fn">
                    "We understand the integral role data plays in the growth of business empires."
            </p>
            <p id="pclasscontent" class="center">
                    Simplified information that applied right from day to day lives to complex decisions is what a good research methodology proves to be. At Acquire Market Research we constantly strive for innovation in the techniques and the quality of analysis that goes into our data, because we are aware of the cascading impact that right and wrong information can have on a global level from overall businesses to people. Serving one of the most sensitive fields of data accumulation and analysis we understand that information travels & impacts everywhere; therefore we undertake this huge responsibility and cater to the provision of accurate, detailed research and data generation. 
                <br><br>
               <p class="cntr">"When it comes to research and consulting, trust Acquire Market Research to be your backbone!"</p>
            </p>
        </div>
    </div>
    <br><br>
</div>
<?php include 'footer_file.php';?>