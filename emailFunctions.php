<?php
	function getCustomerEmailTemplate($data) {
		$html = "";
		$html .= '<p>Thank you! Your order summary is as follows.</p>';
		$html .= '<p>Auto-generated Order ID/ Transaction ID/ Receipt ID &ndash; '.$data['order_id'].'</p>';
		$html .= '<p>Prod ID &amp; Prod Name &ndash; '.$data['prod_id'] .' & '. $data['prod_name'].'</p>';
		$html .= '<p>Payment Amount Transacted &ndash; '.number_format( $data['amount'], 2 ).'</p>';
		$html .= '<p>Card Holder &ndash; '.$data['name'].'</p>';
		$html .= '<p>Card Name. &ndash; '.$data['card_name'].'</p>';
		$html .= '<p>Company &ndash; '.$data['company'].'</p>';
		$html .= '<p>&nbsp;</p>';
		$html .= '<p>Dear '.$data['name'].',</p>';
		$html .= '<p>Greetings!</p>';
		$html .= '<p>Thank you for your interest in MR prod for '.$data['prod_id'] .' &ndash; '. $data['prod_name'].'. This email is to confirm that your message has been received by us. The '. $data['prod_name'].'. will be dispatched shortly. Please refer to <a href="https://www.amr.com/terms-and-conditions/#privacy_policy">https://www.amr.com/terms-and-conditions/#privacy_policy</a>, alternatively you are free to contact us for more delivery details.</p>';
		$html .= '<p>Thank You</p>';
		$html .= '<p>Best regards,</p>';
		$html .= '<p>Team AMR</p>';
		$html .= '<p><strong>AMR</strong></p>';
		$html .= '<p>Telephone: +1800 663 5579</p>';
		$html .= '<p>Email: sales@amr.com</p>';
		$html .= '<p>Website: www.amr.com</p>';
		$html .= '<p>*** This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited. Sharing the contents of this email with any third party is not allowed***</p>';

		return $html;
	}

	function getSelfEmailTemplate($data) {
		$html = "";
		$html .= '<p><strong><u>Direct Pay</u></strong></p>';
		$html .= '<p>Auto-generated Order ID/ Transaction ID/ Receipt ID &ndash; '.$data['order_id'].'</p>';
		$html .= '<p>Prod ID &amp; Prod Name &ndash; '.$data['prod_id'] .' &ndash; '. $data['prod_name'].'</p>';
		$html .= '<p>Payment Amount Transacted &ndash; '.number_format( $data['amount'], 2 ).'</p>';
		$html .= '<p>Card Holder &ndash; '.$data['name'].'</p>';
		$html .= '<p>Card Name. &ndash; '.$data['card_name'].'</p>';
		$html .= '<p>Company &ndash; '.$data['company'].'</p>';

		return $html;
	}

	function sendMail($to, $subject, $from, $html) {
	    $url 	= 'https://api.sendgrid.com/api/mail.send.json';
	    $user 	= 'acquiremarketresearch@123';
	    $pass	= 'acquire@123';
	    $params = array(
	        'api_user'  => $user,
	        'api_key'   => $pass,
	        'to'        => $to,
	        'subject'   => $subject,
	        'html'      => $html,
	        'from'      => $from
	    );
	    
	    $session = curl_init($url);
	    curl_setopt($session, CURLOPT_POST, true);
	    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
	    curl_setopt($session, CURLOPT_HEADER, false);
	    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
	    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($session);
	    curl_close($session);
	    return $response;
    }
    
    print_r(sendMail('sales@acquiremarketresearch.com', "Test", "sales@acquiremarketresearch.com", "Test"));
?>