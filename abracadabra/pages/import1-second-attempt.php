<?php
  $fa="fa-envelop";
  $maintitle="Report";
  $title="Create Report";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTS";
    include "header.php";
 
  $istable=1;
  $emailsms=1;

  ?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
<?php
if(isset($_POST["Import"]))
{
		 $filename=$_FILES["file"]["tmp_name"];
		 if($_FILES["file"]["size"] > 0)
		 {

		  	$file = fopen($filename, "r");$i=1;

	         while (($emapData = fgetcsv($file, 99999999, ",")) !== FALSE)
	         {
	            if($i!=1)
                {
                    $industry_id=""; 
                        $sql=mysqli_query($con,"SELECT * from categories where cat_name like '%".$emapData[10]."%' ");
                        $rows=mysqli_num_rows($sql);
                        if($rows>0)
                        {
                            $row=mysqli_fetch_array($sql);
                            $industry_id=$row['cat_id']; 
                        }
                        else
                        {
                            $sql=mysqli_query($con,"INSERT into categories(cat_name) values ('".$emapData[10]."')");
                            $industry_id=mysqli_insert_id(); 
                        }
                    
                        $query=mysqli_query($con,"SELECT * from reports where report_title='".$emapData[0]."' ");
                        $rows2=mysqli_num_rows($query);
                        if($rows2>0)
                        {
                        }
                        else
                        {
                          try
                          {
                            $result = upload_reports($con, $emapData);
                          }                            
                          catch(Exception $e)
                          {
                            $arr[] = $emapData[0];
                            if(!empty($arr))
                            {
                              foreach ($arr as $key => $value)
                              {
                                echo $value. 'Caught exception: ',  $e->getMessage(), "\n";
                                echo "Retrying once for failed reports ".PHP_EOL;

                                $result = upload_reports($con, $emapData);

                              }
                            }
                          }
                        }
						
				    if(! $result )
				    {
                           echo "<script type=\"text/javascript\">
							    alert(\"Invalid File:Please Upload CSV File.\");
							    window.location = \"reports.php\"
						    </script>";
				    }
                }$i++;
                unset($emapData);

	         }
	         fclose($file);
	         
	         echo "<script type=\"text/javascript\">
						alert(\"CSV File has been successfully Imported.\");
						window.location = \"reports.php\"
					</script>";

		 }
	}	 

?>		 
            
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  function upload_reports($con, $emapData)
  {
    try
    {
      $result=mysqli_query($con,"INSERT INTO `reports`(`report_title`,`pages`,`no_taf`,`singleuser_price`,`enterpriseuser_price`,`published_date`,`summary`,`file_name`,`toc`,`taf`,`catlog`,`cat_id`,`publisher_code`,`meta_keywords`,`meta_desc`) VALUES ('".$emapData[0]."','".$emapData[1]."','".$emapData[2]."','".$emapData[3]."','".$emapData[4]."','".$emapData[5]."','".$emapData[6]."','".$emapData[7]."','".$emapData[8]."','".$emapData[9]."','".$emapData[10]."',".$industry_id.",'".$emapData[11]."','".$emapData[12]."','".$emapData[13]."' )");
      return $result;
    }
    catch(Exception $e)
    {
      echo 'Caught exception in upload_reports: ',  $e->getMessage(), "\n";
    }
  }
  include "footer.php";
?>

