<?php
$fa="fa-user";
$maintitle="Add ID Cards";
$title="Add ID Card";
$mainmenu="ADMINISTRATOR";
$menu="CARDOVER";
include "header.php";
  $istable=1;
?>
<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"> View Delegates
          </h3>
        </div>
        <div class="box-body">
          <!----------------------------------------------------------------------------> 
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:1%;">#</th>
                    <th style="width:30%;">Delegate Type</th>
                    <th style="width:10%;">Delegate Fees</th>
                    <th style="width:10%;">Total Delegate </th>
                    <th style="width:3%;">Total Collection</th>
                  </tr>
                </thead>
                <tbody>

<?php
                $cnt=1;
                $query=@mysql_query("select * from delegate_master order by del_title");
                while($row2=@mysql_fetch_array($query))
                {
                    $query2=@mysql_query("select sum(del_fee),count(*) from registration_form as r, delegate_master as d, imageupload as i, prefix_master as p where p.pf_id=r.pf_id and d.del_id=r.del_id and i.reg_id=r.reg_id and r.del_id=".$row2['del_id']."");
                    $row3=@mysql_fetch_array($query2);
                    $totalfees+=$row2['del_fee'];
                    $toataldelegates+=$row3[1];
                    $totalcollection+=$row3[0];

?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $row2['del_title'];?></td>
                    <td><?php echo $row2['del_fee'];?></td>

                    <td> <?php echo $row3[1];?></td>
                    <td><i class="fa fa-inr" ></i> <?php echo $row3[0];?></td>
                </tr>

<?php
                    $cnt++;
                }
?>
                </tbody>
                <tfooter>
                  <tr>
                    <th style="width:1%;"> </th>
                    <th style="width:30%;"> </th>
                    <th style="width:10%;"><i class="fa fa-inr"></i> <?php echo  $totalfees;?></th>
                    <th style="width:10%;"><?php echo  $toataldelegates;?> Delegates</th>
                    <th style="width:3%;"><i class="fa fa-inr"></i> <?php echo  $totalcollection;?></th>
                  </tr>

                </tfooter>
             </table> 

          <!---------------------------------------------------------------------------->  
        </div>
     </div>
  </div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
