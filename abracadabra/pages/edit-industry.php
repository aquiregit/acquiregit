<?php
  $fa="fa-envelop";
  $maintitle="Industry";
  $title="Create Industry";
  $mainmenu="ADMINISTRATOR";
  $menu="INDUSTRY";
    include "header.php";
 
  $istable=1;
  $emailsms=1;
  $sqlindustry=mysqli_query($con,"SELECT * from categories where cat_id=".$_GET['id']." ");
  $editdatda=mysqli_fetch_array($sqlindustry);    
   //echo "sdfd".$editdatda[0]; 

  ?>
   <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <link rel="stylesheet" href=../"plugins/select2/select2.css">
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<form action="ajax-editindust.php"  id="emailvalidefrm" method="POST" onsubmit="alert('Industry Edited Successfully');">
                <div class="form-group">
                    <label>Industry Id</label>
                <input type="text" class="form-control" placeholder="Industry Id" id="industid" value="<?php echo $editdatda['cat_id'];?>" name="industid" readonly>
              </div>
      
              <div class="form-group">
                    <label>Industry Name</label>
                <input class="form-control" placeholder="Industry" id="Industry" value="<?php echo $editdatda['cat_name'];?>" name="Industry" required >
                <span id="msgindusrty" class="alert hide">Name should not be blank</span>
              
              </div>
              <div class="form-group">
                        
                        <label>Industry Icon</label>
                            <!--<select id="select" name="threads-icon" class="fa-select"></select>-->
                            <input type="text" class="form-control" name="Industry-Icon" value="<?php echo $editdatda['fa_fa_font'];?>"><a href="https://fontawesome.com/icons?d=gallery" target="blank"><b style="font-size:1.4em;color:blue;"><u>Click on this to insert a font awsome icon and paste it on the textbox</u></b></a>

              
              </div>
              <div class="form-group">
                    <label>Meta Keywords</label>
                <input class="form-control" placeholder="Industry Keywords" id="keywords" value="<?php echo $editdatda['meta_keywords'];?>" name="keywords" required>
                <span id="msgindusrty" class="alert hide">Keywords should not be blank</span>
  </div>
<div class="form-group">
                    <label>Meta Description</label>
                <input class="form-control" placeholder="Industry Description" id="description" value="<?php echo $editdatda['meta_desc'];?>" name="description" required>
                <span id="msgindusrty" class="alert hide">Description should not be blank</span>
  </div>
 

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css'>

<style>
#icon {
  margin: 0 0 10px;
}

.chosen-container {
  text-align: left; 
}

</style>
              
            <!-- /.box-body -->
                  <small id="msgsuccess"  class="alert hide" style="color:#00A41E;">
                       <t class="text-center" ><strong>Success!</strong> Industry Updated.</t><br>
                  </small>
                  <small id="msgrepeat"  class="alert hide"  >
                       <t  class="pull-left"><strong>Warning!</strong> Industry May be Repeated.</t><br>
                  </small>

            <div class="box-footer">
              <div class="pull-right">
                <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
               <input type="submit" class="btn btn-primary" id="editblog" name="editindust" data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To Edit A Industry&nbsp;&nbsp;">


              </div>
              <!--<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
            </div>
            <!-- /.box-footer -->
          </div>
          </form>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js'></script>
<script>

</script>

