<?php
 session_start();
 include '../lib/connect.php';
 $sql=mysqli_query($con,"select * from user where user_id=".$_SESSION['u_id']." ");
$temp=mysqli_fetch_array($sql);
 if($temp['role']=="SUPERADMIN"){
 $mainmenu="SUPERADMIN";
// $maintitle="SUPERADMIN";
 }
else if ($temp['role']=="SEO"){
$mainmenu="SEO";
//$maintitle="SEO";
}
else if ($temp['role']=="CONTENT"){
$mainmenu="CONTENT";
//$maintitle="CONTENT";
}
$home=0;
 $istable=1;
  $emailsms=1;
  $upload=1;
  $maintitle="Administrator";
  $menu=" ";
  ?>
<?php 
$_SESSION['timeout'] = time();
  //include "../lib/connect.php";
   include "../lib/common.php";
    date_default_timezone_set('Asia/Kolkata');

    $query=mysqli_query($con,"select * from user where user_id=".$_SESSION['u_id']." ");
	
    
    $details=mysqli_fetch_array($query);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;
      //if($subtitle=="")
      //{
      	//echo "";
      //}
      //else
      //{
      	// echo " | ".$subtitle;
      //}
      ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<style>
/* Preloader */
#preloader {
	position: fixed;
	top:0;
	left:0;
	right:0;
	bottom:0;
	background-color:#fff; /* change if the mask should be a color other than white */
	z-index:99; /* makes sure it stays on top */
}
.btn{
    font-weight:600 !important;
}
#status {
	width:200px;
	height:200px;
	position:absolute;
	left:50%; /* centers the loading animation horizontally on the screen */
	top:50%; /* centers the loading animation vertically on the screen */
	background-image:url(../loader/quiz-loading.gif); /* path to your loading animation */
	background-repeat:no-repeat;
	background-position:center;
	margin:-100px 0 0 -100px; /* is width and height divided by two */
}
.star{
    color:red;
}

.alert{
    color:red;
}

.header{
    font-size:1em !important;
}
@media (max-width: 767px) {
.resphide{
    display:none;
}

</style>
<!-- jQuery Plugin -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Preloader -->
<script type="text/javascript">
	//<![CDATA[
		$(window).on('load', function() { // makes sure the whole site is loaded 
			$('#status').fadeOut(); // will first fade out the loading animation 
            $('#preloader').delay(450).fadeOut('slow'); // will fade out the white DIV that covers the website. 
            $('body').delay(350).css({'overflow':'visible'});
		})
	//]]>
</script> 

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	 <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'>
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">


    <script type="text/JavaScript" src='../../validation.js'></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
<div id="preloader">
	<div id="status">&nbsp;</div>
</div>

    <div class="wrapper">
    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b><i class="fa fa-home" aria-hidden="true"></i></b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><i class="fa fa-home" aria-hidden="true" style="font-size:1.5em;"></i>&nbsp;Admin</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu" id="menu_update">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less--><!--
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">	
              <i class="fa fa-commenting"></i>
              <span class="label label-success"></span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">
                  <div class="clearfix">
                    <span class="pull-left">SMS</span>
                    <small class="pull-right"> <?php echo $sms['remainingSMS']." / ".$sms['totalSMS'];?></small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-red progress-bar-striped active" role="progressbar" style="width: <?php echo $SMSper;?>%;"></div>
                  </div>


                </li>
              </ul>
            </li>-->
            <!-- Notifications: style can be found in dropdown.less --><!--
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-warning"></span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">
                      <div class="clearfix">
                        <span class="pull-left">Email</span>
                        <small class="pull-right"> <?php echo $email['remainingEMAIL']." / ".$email['totalEMAIL'];?></small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red progress-bar-striped active" role="progressbar" style="width: <?php echo $EMAILper;?>%;"></div>
                      </div>


                </li>
              </ul>
            </li>-->
            <!-- Tasks: style can be found in dropdown.less -->
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $details['username'];?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="../dist/img/avatar5.png" class="img-circle" alt="User Image">
                  <p>
                    <?php echo $details['username'];?>
                    <!--<small>Member since <?php echo date('F, Y', strtotime($details['reg_date'])); ?></small>-->
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                    <!--  <a href="change-password.php">Change Password</a>-->
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
<?php
                    if($details['role']=="ADMIN")
                    {
?>
                    <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
<?php
                    }
?>
                  </div>
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
	<?php if ($mainmenu=='SUPERADMIN'){?>
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <ul class="sidebar-menu">

          <li class="header" style="color:white;font-weight:bold;"><i class="fas fa-user-secret"></i>&nbsp;&nbsp;&nbsp;Administrator</li>
<!-------------------------Administrator Master-------------------------------------------------------->
          <li class="treeview active">
            <a href="#">
            <i class="fa fa-users"></i>
            <span>Administrator</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">

              <!--<li ><a href="add-amt.php"><i class="fa fa-circle-o"></i> Add Payment Type</a></li>-->

              <li <?php if($menu=="PREVCARD") echo "class='active'"; ?>><a href="prev-data.php"><i class="far fa-circle"></i> Customer </a></li>
              <li <?php if($menu=="INDUSTRY") echo "class='active'"; ?>><a href="industry.php"><i class="far fa-circle"></i> Industries </a></li>
              <li <?php if($menu=="REPORTS") echo "class='active'"; ?>><a href="reports.php"><i class="far fa-circle"></i> Upload Reports(CSV Upload) </a></li>
              <li <?php if($menu=="REPORTSE") echo "class='active'"; ?>><a href="ereports.php"><i class="far fa-circle"></i> Upload Reports(Excel Upload) </a></li>
             <li <?php if($menu=="REPORTSM") echo "class='active'"; ?>><a href="mreport.php"><i class="far fa-circle"></i> Upload Reports Manually</a></li>
			  <li <?php if($menu=="BLOG") echo "class='active'"; ?>><a href="blog.php"><i class="far fa-circle"></i> Upload Blog</a></li>
			  <li <?php if($menu=="PR") echo "class='active'"; ?>><a href="pr.php"><i class="far fa-circle"></i>Upload Press Releases</a></li>
			  <li <?php if($menu=="NEWS") echo "class='active'"; ?>><a href="news.php"><i class="far fa-circle"></i> Upload News</a></li>
			  <li <?php if($menu=="EXPORT") echo "class='active'"; ?>><a href="eexport.php"><i class="far fa-circle"></i> Export Reports(Excel)</a></li> 
            </ul>
          </li>
		</ul>
		
		</section>
		</aside>
		
	<?php }?>
		<!--SEO Menu-->
		<?php if($mainmenu=='SEO'){?>
		<aside class="main-sidebar">
		 <section class="sidebar">
		<ul class="sidebar-menu">

          <li class="header" style="color:white;font-weight:bold;"><i class="fas fa-user-secret"></i>&nbsp;&nbsp;&nbsp;SEO</li>
		<li class="treeview active">
            <a href="#">
            <i class="fa fa-users"></i>
            <span>SEO</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">

              <!--<li ><a href="add-amt.php"><i class="fa fa-circle-o"></i> Add Payment Type</a></li>-->

              <li <?php if($menu=="REPORTS") echo "class='active'"; ?>><a href="reports.php"><i class="far fa-circle"></i> Upload Reports(CSV Upload) </a></li>
              <li <?php if($menu=="REPORTSE") echo "class='active'"; ?>><a href="ereports.php"><i class="far fa-circle"></i> Upload Reports(Excel Upload) </a></li>
			 <li <?php if($menu=="PR") echo "class='active'"; ?>><a href="pr.php"><i class="far fa-circle"></i>Upload Press Releases</a></li>
			 <li <?php if($menu=="BLOG") echo "class='active'"; ?>><a href="blog.php"><i class="far fa-circle"></i> Upload Blog</a></li>
			 			  
            </ul>
          </li>
		
		</ul>
		</section>
		</aside>
<?php }?>
		<!--End of SEO Menu-->
		
		<!--Content Menu-->
		<?php if($mainmenu=='CONTENT') {?>
		<aside class="main-sidebar">
		 <section class="sidebar">
		<ul class="sidebar-menu">
		<li class="header" style="color:white;font-weight:bold;"><i class="fas fa-user-secret"></i>&nbsp;&nbsp;&nbsp;Content</li>
		 <li class="treeview active">
            <a href="#">
            <i class="fa fa-users"></i>
            <span>Content</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">

              

              
			  <li <?php if($menu=="BLOG") echo "class='active'"; ?>><a href="blog.php"><i class="far fa-circle"></i> Upload Blog</a></li>
			  <li <?php if($menu=="PR") echo "class='active'"; ?>><a href="pr.php"><i class="far fa-circle"></i>Upload Press Releases</a></li>
			  
            </ul>
          </li>
		
		
		</ul>
		</section>
		</aside>
		<?php }?>
		<!--End of content Menu-->
      

      <!-- /.sidebar -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
                 AMR Managment System 
        </h1>
        <ol class="breadcrumb">
          <li class="active"><a href="#"><?php echo $maintitle;?></a></li>
          <li class="active"><?php echo $title;?></li>
	   

        </ol>
      </section>


