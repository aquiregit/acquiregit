<?php
  $fa="fa-envelop";
  $maintitle="Email";
  $title="View Email";
  $mainmenu="ADMINISTRATOR";
  $menu="EMAILID";
  include "header.php";
 
  $istable=1;
?>

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"> Sender List
          </h3>
             &nbsp; <a href="add-email.php" class="btn btn-default pull-right">Add Sender</a>
        </div>
        <div class="box-body">
          <!----------------------------------------------------------------------------> 
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:1%;">#</th>
                    <th style="width:15%;">Email Id</th>
                    <th style="width:30%;">Name</th>
                    <th style="width:30%;">Edit</th>
                    <th style="width:3%;">Delete</th>

                  </tr>
                </thead>
<?php
                $cnt=1;
                $query2=@mysql_query("select * from Sender order by sender_id desc");
                while($row2=@mysql_fetch_array($query2))
                {
?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $row2['sender_email'];?></td>
                    <td><?php echo $row2['name'];?></td>
                    <td><a href="add-email.php?id=<?php echo $row2['sender_id'];?>">Edit</a> </td>
                    <td><a href="" onclick="deleteSender(<?php echo $row2['sender_id'];?>)">delete</a> </td>
                </tr>

<?php
                    $cnt++;
                }
?>
                <tbody>
                </tbody>
             </table> 

          <!---------------------------------------------------------------------------->  
        </div>
     </div>
  </div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
<script>
function deleteSender(id)
{
        id=id;
    //alert(id);
    if(confirm('Are you sure want to delete ?'))
    {

      var variable="1";
   $.post('ajax-sender.php',
      {
        id:id,isdelete:variable,

      },
      function(res)
      {
        var string = $.trim(res);
        alert(string);
        if (string == "true")
        {
          //alert('Registration Successfull');
          window.location = 'sender-list.php';
        }
        else
        {
          alert('Something wrong');
        }
        document.getElementById('save').disabled = false;
      });



    }

}
</script>
