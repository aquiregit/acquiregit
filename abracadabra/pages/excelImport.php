<?php

    $mainmenu='ADMINISTRATOR';
    $menu="ADD";
    $fa="fa-file-excel-o";
    $maintitle="Email";
    $title="Import Excel";
    //$_GET['id']="";
    include "header.php";

?>
<section class="content">
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Import Email Excel</h3>
          <!-- /.box-tools -->
            <a href="download/email-demo.xlsx" class="btn btn-default pull-right" download>Download Sample File</a> &nbsp;  
        </div>
        <!-- /.box-header -->
        <div class="box-body">


        <form id="regvalid" name="regvalid" method="POST" action="upload.php" class="form-horizontal form-label-left" enctype="multipart/form-data" >

            <div style="background-color:red;color:white" class="alert text-center square hide" id="msgblank">Please Select File</div>
            <p class="text-center" style="font-weight:bold"><br>You can Download the Sample excel file and upload it back by filling your data in it<br><br></p>
      <div class="form-group">
         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">
         Upload Excel File
         </label>
         <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
            <input id="file" name="file" required="required" class="form-control col-md-7 col-xs-12" type="FILE">
            <!--
            <span class="alert text-center square hide" id="msgname">Employee name should not blank</span>-->
         </div>
      </div>

      <div class="ln_solid"></div>
      <div class="form-group">
         <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="list.php"><button class="btn btn-primary" type="button">Cancel</button></a>
            <button class="btn btn-primary" type="reset">Reset</button>
            <button type="submit" onclick="return uploadexcel()" id="submit" name="submit" class="btn btn-success">Submit</button>
         </div>
      </div>
   </form>


        </div>
      </div>
    </div>
  </div>
</section>
</div>

<?php
    include "footer.php";
?>
<script>
function uploadexcel()
{
    flag=1;
      if (!$('#msgblank').hasClass("hide")) {
          $('#msgblank').addClass("hide");
      };


    document.getElementById('submit').disabled = true; 
    var file=$("#file").val().split('/').pop().split('\\').pop();//$.trim(document.getElementById('file').value);

    if(file=="")
    {
        flag=0;
        $('#msgblank').removeClass("hide");
    }

    if(flag==1)
    {
        document.forms["regvalid"].submit();


    }

    document.getElementById('submit').disabled = false; 
    return false;

}
</script>

