<?php
$fa="fa-user";
$maintitle="Customer";
$title="Customer List";
$mainmenu="ADMINISTRATOR";
$menu="PREVCARD";
include "header.php";
  $istable=1;
?>

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"> Customer List
          </h3>
        </div>
        <div class="box-body">
          <!----------------------------------------------------------------------------> 
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:3%;">Sr. No.</th>
                    <th style="width:10%;">Name</th>

                    <th style="width:10%;">Company Name</th>
                    <th style="width:10%;">Contact Number</th>
                    <th style="width:5%;">Amount</th>
                    <th style="width:3%;">View</th>
                  </tr>
                </thead>
<?php
                $cnt=1;
                $query2=mysqli_query($con,"select * from customer order by customer_id desc");
                while($row2=mysqli_fetch_array($query2))
                {
?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $row2['cust_fullname'];?></td>
                    <td><?php echo $row2['cust_company'];?></td>
                    <td><?php echo $row2['cust_phone'];?> </td>
                    <td><?php echo $row2['cust_price'];?> </td>
                    <td><a href="view.php?id=<?php echo $row2['customer_id'];?>">View</a></td>
                </tr>

<?php
                    $cnt++;
                }
?>
                <tbody>
                </tbody>
             </table> 

          <!---------------------------------------------------------------------------->  
        </div>
     </div>
  </div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>

