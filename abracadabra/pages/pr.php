<?php
  $fa="fa-newspaper-o";
  $maintitle="Press Release";
  $title="Create Press Release";
  $mainmenu="ADMINISTRATOR";
  $menu="PR";
  
    include "header.php";
 
  $istable=1;
  $emailsms=1;
  /*if($_GET['id']!="")
	{  
    $id=1;
    $sqlquery=mysqli_query($con,"select * from blog where blog_id=".$_GET['id']." ");
    $blog=mysqli_fetch_array($sqlquery);

    $date=date('Y-m-d', strtotime($blog['release_date']));
  }
  else
  {  
    $id=0;  
    $date=date('Y-m-d');
  }*/
  ?>

   <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <link rel="stylesheet" href=../"plugins/select2/select2.css">
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add PR</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<form action="ajax-pr.php" enctype="multipart/form-data" id="emailvalidefrm" method="POST" onsubmit="alert('PR Added Successfully!');">
		   <div class="form-group">
                    <label>PR Title</label>
                <input type="text" class="form-control" placeholder="PR Title" id="prtitle" value="" name="prtitle" required>
               
              
              </div>
			  
			 
			 <div class="form-group">
                    <label>Published Date</label>
                <input type="date" data-date="" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="pdate" name="pdate" required>
                
						<script>
						$("input").on("change", function() {
							this.setAttribute(
								"data-date",
								moment(this.value, "YYYY-MM-DD")
								.format( this.getAttribute("data-date-format") )
							)
						}).trigger("change")
						</script>
			 </div>

		    
		    
       
                

              <div class="form-group">
                    <label>Summary</label>
                    <textarea id="editor1" class="compose-textarea" name="editor1" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty" ></textarea>
                  <span id="msgsummary" class="alert hide">PR description should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->

              </div>


 
                    <div class="form-group"><label>Upload Image File:</label>
                <input type="file" class="form-control" placeholder="" id="imagefile" value="" name="imagefile[]" multiple>
            
              
              </div>

			  <div class="form-group">
                    <label>Tags </label>
                <input class="form-control" placeholder="tag" id="tag" value="" name="tag" required >
                <span id="msgwritten" class="alert hide">Add the Tags</span>
              
              </div>



            <!-- /.box-body -->
                  <small id="msgsuccess"  class="alert hide" style="color:#00A41E;">
                       <t class="text-center" ><strong>Success!</strong> PR Added.</t><br>
                  </small>

            <div class="box-footer">
              <div class="pull-right">
                <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
               <a href="pr-view.php?page=PR" class="btn btn-primary"  data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To View PR&nbsp;&nbsp;" ><i class="far fa-envelope"></i> View</a> 
			   <input type="submit" id="addpr" class="btn btn-primary" name="addpr" data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To Create A PR&nbsp;&nbsp;">

              </div>
              <!--<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
            </div>
            <!-- /.box-footer -->
          </div>
          </form>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js'></script>
<script>

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

  });


/*function saveblog()
{  

  flag=1;
  if (!$('#msgwritten').hasClass("hide"))
  {
    $('#msgwritten').addClass("hide");
  };
  if (!$('#msgblogtitle').hasClass("hide"))
  {
    $('#msgblogtitle').addClass("hide");
  };
  if (!$('#msgsummary').hasClass("hide"))
  {
    $('#msgsummary').addClass("hide");
  };
  if (!$('#msgpd').hasClass("hide"))
  {
    $('#msgpd').addClass("hide");
  };




  if (!$('#msgsuccess').hasClass("hide"))
  {
    $('#msgsuccess').addClass("hide");
  };

  var writtenby = $.trim(document.getElementById('writtenby').value);
  if(writtenby=="")
  {

  	flag=0;
    $('#msgwritten').removeClass("hide");  	
  }

  var blogtitle = $.trim(document.getElementById('blogtitle').value);
  if(blogtitle=="")
  {

  	flag=0;
    $('#msgblogtitle').removeClass("hide");  	
  }


  var date = $.trim(document.getElementById('pdate').value);

  var editor  = CKEDITOR.instances['editor1'].getData();
  Summary=editor;
  if(Summary=="")
  {

  	flag=0;
    $('#msgsummary').removeClass("hide");  	
  }




  var condition = $.trim(document.getElementById('condition').value);
  var blog_id = $.trim(document.getElementById('blog_id').value);

//alert(flag);
    if(flag==1)
    {
        //alert(writtenby+' '+blogtitle+' '+Summary+' ');

         $.post('ajax-blog.php',
          {

              
              blogtitle:blogtitle,
              date:date,
              Summary:Summary,    
			  tag:tag
          },
          function(res)
          {//alert(res);
            var string = $.trim(res);
            if (string == "true")
            {
              $("#msgsuccess").removeClass("hide");
            }
          });
    }

  document.getElementById('reportadd').disabled = false;
  return false;
}

function isInt(value) {
  if (isNaN(value)) {
    return false;
  }
  var x = parseFloat(value);
  return (x | 0) === x;
}*/
</script>

