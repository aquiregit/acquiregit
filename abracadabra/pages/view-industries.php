<?php
  $fa="fa-envelop";
  $maintitle="Industry";
  $title="View Industry";
  $mainmenu="ADMINISTRATOR";
  $menu="INDUSTRY";
  include "header.php";
 
  $istable=1;
?>

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
   
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"> Industries List
          </h3>
        </div>
        <div class="box-body">
          
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:1%;">Category Id</th>
                    <th style="width:5%;">Icons</th>
                    <th style="width:15%;">Industry</th>
                   
                    <th style="width:3%;">Edit</th>
                    <th style="width:3%;">Delete</th>

                  </tr>
                </thead>
<?php
                $cnt=1;
                $query2=mysqli_query($con,"SELECT * from categories");
                while($row2=mysqli_fetch_array($query2))
                {
?>
                <tr>
                    <td><?php echo $row2['cat_id'];?></td>
                    <td  class="text-center"><i class="fa fa-2x <?php echo $row2['fa_fa_font'];?>"></i></td>
                    <td><?php echo $row2['cat_name'];?></td>
                    <td><a href="edit-industry.php?id=<?php echo $row2['cat_id'];?>">Edit</a></td>

                    <td><a href="" onclick="deleteindustry(<?php echo $row2['cat_id'];?>)">Delete</a> </td>
                </tr>

<?php
                    $cnt++;
                }
?>
                <tbody>
                </tbody>
             </table> 
        </div>
     </div>
  </div>

</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
<script>
function deleteindustry(id)
{
        id=id;
    //alert(id);
    if(confirm('Are you sure want to delete this industry ?'))
    {

      var variable="-1";
   $.post('ajax-industry-add.php',
      {
       
     id:id,deletei:'-1'

      },
      function(res)
      {
        var string = $.trim(res);
        alert(string);
        if (string == "true")
        {
          //alert('Registration Successfull');
          window.location = 'view-industries.php';
        }
        else
        {
          alert('Something wrong');
        }
        document.getElementById('save').disabled = false;
      });



    }

}
</script>
