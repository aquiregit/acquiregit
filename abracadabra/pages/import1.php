<?php
error_reporting(E_ALL); 
ini_set('display_errors', TRUE); 
$fa        = "fa-envelop";
$maintitle = "Report";
$title     = "Create Report";
$mainmenu  = "ADMINISTRATOR";
$menu      = "REPORTS";
include "header.php";

$istable  = 1;
$emailsms = 1;

?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
<?php
        
            // $emapData[11] = 'Service & Software';
            // print_r($categories[$emapData[11]]['cat_id']);exit;
if (isset($_POST["Import"])) {
    $filename = $_FILES["file"]["tmp_name"];
    $v        = $_FILES["file"]["name"];
    if ($_FILES["file"]["size"] > 0) {
        $v1 = substr(strrchr($v, '.'), 1);
        if ($v1 == "csv") {
            $file = fopen($filename, "r");
            $i    = 1;
            
            $result = mysqli_query($con, "SELECT * from categories");
            while( $category = mysqli_fetch_assoc($result) ) {
                $categories[$category['cat_name']] = $category;
            }
                        
            while (($emapData = fgetcsv($file, 99999, ",")) != FALSE) {
                if(!$emapData[11]) {
                    continue;
                }

                if ($i != 1) {
                    $industry_id = "";
                    if(isset($categories[$emapData[11]])) {
                        $industry_id = $categories[$emapData[11]]['cat_id'];
                    }
                    
                    $title1               = addslashes(convert_ascii(normalize($emapData[0])));
                    $title1 = trim(preg_replace('!\s+!', ' ', $title1));
                    $pages                = addslashes($emapData[1]);
                    $no_taf               = $emapData[2];
                    $singleuser_price     = $emapData[3];
                    $multiuser_price      = $emapData[4];
                    $enterpriseuser_price = $emapData[5];
                    $published_date       = $emapData[6];
                    $summary              = addslashes(convert_ascii(normalize($emapData[7])));
                    $file_name            = $title1 . '.pdf';
                    $toc                  = addslashes(convert_ascii(normalize($emapData[9])));
                    $taf                  = addslashes(convert_ascii(normalize($emapData[10])));
                    $catlog               = addslashes($emapData[11]);
                    $publisher_code       = addslashes($emapData[12]);
                    $status               = "INSERTED";
                    
                    // Generate automatically 
                    //Meta-title
                    //$meta_title = $title;
                    //Meta-Description
                    $meta_description = $title1 . ' Published By :acquiremarketresearch.com';
                    //Meta-Keywords
                    $str              = stristr($title1, 'Market', true);
                    $meta_keywords    = ucwords($str . 'Market, Market research, market research reports, industry reports, Regions reports, industry analysis, business research');
                    //print_r($meta_keywords); die;
                    $sql = "INSERT INTO `reports`(`report_title`,`pages`,`no_taf`,`singleuser_price`,`multiuser_price`,`enterpriseuser_price`,`published_date`,`summary`,`file_name`,`toc`,`taf`,`catlog`,`cat_id`,`publisher_code`,`meta_keywords`,`meta_desc`,`status`) 
                    VALUES ('" . $title1 . "','" . $pages . "','" . $no_taf . "','" . $singleuser_price . "','" . $multiuser_price . "','" . $enterpriseuser_price . "','" . $published_date . "','" . $summary . "','" . $file_name . "', '" . $toc . "','" . $taf . "','" . $catlog . "'," . $industry_id . ",'" . $publisher_code . "','" . $meta_keywords . "','" . $meta_description . "' ,'INSERTED')";
                    // print_r($sql); exit;
                    $result = mysqli_query($con, $sql);
                    echo mysqli_error($con);
                

                    if (!$result) {
                        echo "<script type=\"text/javascript\">
                                alert(\"File has contains some Duplicates Reports. Please Kindly check File.\");
                                window.location = \"reports.php\"
                            </script>";
                    }
                }

                $i++;
                unset($emapData);
            }
        } else {
            echo "<script type=\"text/javascript\">alert(\"Invalid File / Extension! Please Upload CSV. Uploaded file's extension was : " . $v1 . ".\");
                     window.location = \"reports.php\";
                   </script>";
        }
        fclose($file);
        
        echo "<script type=\"text/javascript\">
                        alert(\"CSV File has been successfully Imported.\");
                        window.location = \"reports.php\"
                    </script>";
        
    }
}

function normalize ($string) {
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
    );
    
    return strtr($string, $table);
}
function convert_ascii($string) 
{ 
  $string = utf8_encode($string);
  // Replace Single Curly Quotes
  $search[]  = chr(226).chr(128).chr(152);
  $replace[] = "'";
  $search[]  = chr(226).chr(128).chr(153);
  $replace[] = "'";
  // Replace Smart Double Curly Quotes
  $search[]  = chr(226).chr(128).chr(156);
  $replace[] = '"';
  $search[]  = chr(226).chr(128).chr(157);
  $replace[] = '"';
  // Replace En Dash
  $search[]  = chr(226).chr(128).chr(147);
  $replace[] = '-';
  // Replace Em Dash
  $search[]  = chr(226).chr(128).chr(148);
  $replace[] = '-';
  // Replace Bullet
  $search[]  = chr(226).chr(128).chr(162);
  $replace[] = '';
  // Replace Middle Dot
  $search[]  = chr(194).chr(183);
  $replace[] = '';
  // Replace Ellipsis with three consecutive dots
  $search[]  = chr(226).chr(128).chr(166);
  $replace[] = '';
  // Apply Replacements
  $string = str_replace($search, $replace, $string);
  $string = str_replace('?', '', $string);
  // Remove any non-ASCII Characters
  $string = preg_replace("/[^\x01-\x7F]/","", $string);

  return $string;
}
?>         
      /*$sql         = mysqli_query($con, "SELECT * from categories where cat_name like '%" . $emapData[11] . "%' ");
                    $rows        = mysqli_num_rows($sql);
                    if ($rows > 0) {
                        $row         = mysqli_fetch_array($sql);
                        $industry_id = $row['cat_id'];
                    } else {
                        //$sql         = mysqli_query($con, "INSERT into categories(cat_name) values ('" . $emapData[10] . "')");
                        //$industry_id = mysqli_insert_id();
                    }
                   
                    $query = mysqli_query($con, "SELECT * from reports where report_title='" . $emapData[0] . "' ");
                    $rows2 = mysqli_num_rows($query);
                    if ($rows2 > 0) {
                        $updatesql = "UPDATE reports SET status='DUPLICATE' where report_title='" . $empData[0] . "'";
                        mysqli_query($con, $updatesql);
                    } else {
                    */      
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
include "footer.php";
?>