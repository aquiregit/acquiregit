<?php
$fa="fa-user";
$maintitle="View Customer ";
$title="View Customer";
$mainmenu="ADMINISTRATOR";
$menu="PREVCARD";
include "header.php";
$istable=1;
$query2=mysqli_query($con,"select * from  customer where customer.customer_id=".$_GET['id']." ");
$row2=mysqli_fetch_array($query2,MYSQLI_BOTH);
//echo "asd".$row2['fullname'];
?>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">View Data
          </h3>
        </div>
        <div class="box-body">
            <style>
                table td,th{
                    font-family:"Times New Roman";
                    font-weight:bold;
                }
            </style>
          <!----------------------------------------------------------------------------> 
            <div class="row">

        <form method="POST" action="">
         <div class="col-md-4">
            <div class="form-group">
               <label for="InputFullName">Full Name <span class="redstar">*</span></label>
               <input name="username" disabled id="username" value="<?php echo $row2['cust_fullname'];?>"  placeholder="Your Name" pattern="^[a-zA-Z ]+$" class="form-control" required="" type="text">

            </div>
            <div class="form-group">
               <label for="InputPhoneno">Phone No. (Pls. Affix Country Code) <span class="redstar">*</span></label>
               <input name="phone" id="phone" disabled value="<?php echo $row2['cust_phone'];?>" placeholder="Your Phone Number" maxlength="100" required="" pattern="^[0-9 ]+$" class="form-control" type="text">

            </div>
            <div class="form-group">
               <label for="Inputaddress">Company Name <span class="redstar">*</span></label>
               <input name="company_name" disabled value="<?php echo $row2['cust_company'];?>" id="company_name" placeholder="Your Company Name" class="form-control" required="" type="text">
            </div>



         </div>

         <div class="col-md-4">
            <div class="form-group">
               <label for="Inputaddress">Email <span class="redstar">*</span></label>
               <input name="email" id="email" disabled value="<?php echo $row2['cust_email'];?>" placeholder="Your Email Address" required="" pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$" class="form-control" type="text">
            </div>
            <div class="form-group">
               <label for="Inputaddress">Job Title <span class="redstar">*</span></label>
               <input disabled name="designation" value="<?php echo $row2['cust_desig'];?>" id="designation" placeholder="Job Title" maxlength="100" required="" class="form-control" type="text">
            </div>
            <div class="form-group">
               <label for="Inputaddress">Country <span class="redstar">*</span></label>
               <select disabled name="country" id="country" class="form-control">
                    <option><?php echo $row2['cust_country'];?></option>
               </select>
            </div>
         </div>
         <div class="col-md-4">

            <div class="form-group">
               <label for="Inputaddress">Report Price<span class="redstar">*</span></label>
				<input  readonly value="<?php echo $row2['cust_price'];?>" placeholder="Report Price" class="form-control" required="" type="text">				
            </div>
            
            

            <div class="form-group">
               <label for="InputPhoneno">Report Title <span class="redstar">*</span></label>
               <input  readonly value="<?php echo $row2['cust_report'];?>" placeholder="Report Title" class="form-control" required="" type="text">

            </div>
            
			
			<div class="form-group">
               <label for="InputPaymentOp">Payment Options <span class="redstar">*</span></label>
               <input  readonly value="<?php echo $row2['payment_option'];?>" placeholder="Payment Option" class="form-control" required="" type="text">

            </div>
            
         </div>

        



        </form>


            </div>
          <!----------------------------------------------------------------------------> 

        </div>

      </div>
    <!---------------------------------------------------------------------------->  
    </div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>

