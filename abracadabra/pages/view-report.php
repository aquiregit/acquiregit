<?php
  $fa="fa-envelop";
  $maintitle="Industry";
  $title="View Industry";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTSM";
  include "header.php";
 
  $istable=1;


   $limit = 15; 
    if(isset($_GET['page11']) & !empty($_GET['page11']))
    {
         $page11=$_GET['page11'];
    }
    else
    {
         $page11=1;
    }
    $start_from = ($page11-1) * $limit; 
?>

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    
    <div class="col-sm-12">

      <form class="navbar-form navbar-right " action="search-report.php?search=<?php echo rtrim(str_replace(" ","_",$_GET['search']));?>" method="GET" id="form_search" style="padding-right: 50px;">
                            <div class="input-group">
                                <style type="text/css">
                                    
                                    /*original css*/
                                    #searchreport
                                    {
                                    width:320px;
                                    }
                                    
                                </style>
                                <input type="text" class="form-control" placeholder="Search Research Reports | Report Title" name="search"   list="search-box1" id="searchreport" required>
                                <datalist id="search-box1">
                                    <option value="Agriculture"></option>
                                    <option value="Automobile & Transportation"></option>
                                    <option value="Chemical & Material"></option>
                                    <option value="Consumer Goods"></option>
                                    <option value="Electronics & Semiconductor"></option>
                                    <option value="Energy & Power"></option>
                                    <option value="Food & Beverages"></option>
                                    <option value="Internet & Communication"></option>
                                    <option value="Machinery & Equipment"></option>
                                    <option value="Medical Care"></option>
                                    <option value="Medical Devices & Consumables"></option>
                                    <option value="Pharmaceuticals & Healthcare"></option>
                                    <option value="Service & Software"></option>
                                </datalist>
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit" value="search">
                                    <i class="glyphicon glyphicon-search" style="height: 20px;"></i>
                                    </button> 
                                    
                                </div>
                            </div>
                        </form>








      <div class="box box-primary box-solid">
        <div class="box-header with-border">

          
          <h3 class="box-title"> Report List
          </h3>
        </div>
        <div class="box-body">
          
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:1%;">Sr No.</th>
                    <th style="width:15%;">Industry</th>
                    <th style="width:30%;">Report Title</th>
                    <th style="width:15%;">Status</th>
                    <th style="width:3%;">Edit</th>
                    <th style="width:3%;">Delete</th>

                  </tr>
                </thead>
<?php
                $cnt=1;
                $query2=mysqli_query($con,"SELECT * FROM categories,reports where categories.cat_id=reports.cat_id order by report_id DESC LIMIT $start_from, $limit ");
                while($row2=mysqli_fetch_array($query2))
                {
?>
                <tr>
                    <td><?php echo $row2['report_id'];?></td>
                    <td><?php echo $row2['cat_name'];?></td>
                    <td><?php echo $row2['report_title'];?></td>
                    <td><?php echo $row2['status'];?></td>
                    <td><a href="report_edit.php?id=<?php echo $row2['report_id'];?>">Edit</a></td>

                    <td><a href="" onclick="deletereport(<?php echo $row2['report_id'];?>)">Delete</a> </td>
                </tr>

<?php
                    $cnt++;
                }
?>
                <tbody>
                </tbody>
             </table> 

         
        </div>
     </div>
  </div>

</div>
<!-- /.row (main row) -->
</section>


        <style>
                .pagination {
                display: inline-block;
                padding-left: 100px;
                }
                .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
                transition: background-color .3s;
                border: 1px solid #ddd;
                }
                .pagination a.active   {
                background-color: #0077b5;
                color: white;
                border: 1px solid #4CAF50;
                }
                .pagination a:hover:not(.active) {background-color: #ddd;}
            </style>
            <?php  

            //SELECT COUNT(DISTINCT emp_name,emp_desig,emp_phn_local,emp_email,status)FROM employee_details WHERE emp_name LIKE '%".$setit."%' OR emp_desig LIKE '%".$setit."%' OR emp_phn_local LIKE '%".$setit."%' OR emp_email LIKE '%".$setit."%' OR status LIKE '%".$setit."%'

            //SELECT * FROM categories,reports where categories.cat_id=reports.cat_id
                $sql="SELECT COUNT(*)FROM reports";
                
                $rs_result = mysqli_query($con,$sql);  
                $row = mysqli_fetch_row($rs_result);  
                $total_records = $row[0];  
                $total_pages = ceil($total_records / $limit);  
                
                $current_page=$page11;
                 //$url=rtrim(str_replace(" ","_",$setit));
                
                if($page11==1)
                {
                    $pagLink = "<div ><ul  class='pagination' style='display:inline'><li style='display:inline' class='disabled'><a>Previous</a></li>";
                }
                else {
                $pagLink = "<div><ul  class='pagination' style='display:inline'><li style='display:inline' class='disabled'><a href='view-report.php?page11=".($page11-1)."' id='pp'>Previous</a></li>"; 
                }
                
                for ($i=max(1,$page11-4); $i<=min($page11+4,$total_pages); $i++) {  
                
                
                          $active='';
                        if(isset($_GET['page11']) && $i==$_GET['page11'])
                        {
                            $active='class="active"';
                        }
               
                          $pagLink .= "<li $active style='display:inline'>&nbsp;<a href='view-report.php?page11=".$i."' >".$i."&nbsp;</a></li>";
                          
                }; 
                if($page11<=$total_pages){            
                 echo $pagLink . "<li style='display:inline' class='disabled'><a>NEXT</a></li> </ul></div>";}
                        else {     
                     echo $pagLink ."<li style='display:inline'><a href='view-report.php?page11=".($page11+1)."' id='np'>NEXT</a></li> </ul></div>";
                  }    
                ?><br><br><br>
            <div><b>
                <?php
                    echo "Page No : ".$current_page." / ".$total_pages;
                    
                    ?></b>
            </div>
        
        <br><br><br><br>




<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
<script>
function deletereport(id)
{
        id=id;
    //alert(id);
    if(confirm('Are you sure want to delete Report ?'))
    {

      var variable="-1";
   $.post('ajax-reports.php',
      {
       
		 id:id,deletei:'-1'

      },
      function(res)
      {
        var string = $.trim(res);
        alert(string);
        if (string == "true")
        {
          //alert('Registration Successfull');
          window.location = 'view-report.php';
        }
        else
        {
          alert('Something wrong');
        }
        document.getElementById('save').disabled = false;
      });



    }

}
</script>
