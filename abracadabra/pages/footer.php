<?php  $istable=1;
  $emailsms=1;
  $upload=1;
  ?>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="https://www.acquiremarketresearch.com/" target="_blank">Acquire Market Research</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="../#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="../#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="../javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="../javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="../javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="../javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>

    <link rel="stylesheet" href="../dist/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="../dist/js/bootstrapValidator.js"  type="text/javascript" ></script>
    
<?php
    if($upload==1)
    {
?>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="jquery.form.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"  type="text/javascript" ></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"  type="text/javascript" ></script>
<!-- AdminLTE for demo purposes 
<script src="../dist/js/demo.js"  type="text/javascript" ></script>-->

<?php
    }
    else		 	
    if($istable!=1)
    {

?>

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"  type="text/javascript" ></script>
<!-- jQuery UI 1.11.4 -->
<script src="../dist/js/jquery.min.js"  type="text/javascript" ></script>
<!--
<script src="../https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"  type="text/javascript" ></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"  type="text/javascript" ></script>
<!-- Morris.js charts -->
<!--<script src="../https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"  type="text/javascript" ></script>

<script src="../plugins/morris/morris.min.js"  type="text/javascript" ></script>
<!-- Sparkline -->
<script src="../plugins/sparkline/jquery.sparkline.min.js"  type="text/javascript" ></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"  type="text/javascript" ></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"  type="text/javascript" ></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/knob/jquery.knob.js"  type="text/javascript" ></script>
<!-- daterangepicker -->
<!--<script src="../https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"  type="text/javascript" ></script>-->
<script src="../dist/js/moment.min.js"  type="text/javascript" ></script>

<script src="../plugins/daterangepicker/daterangepicker.js"  type="text/javascript" ></script>
<!-- datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"  type="text/javascript" ></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"  type="text/javascript" ></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"  type="text/javascript" ></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"  type="text/javascript" ></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"  type="text/javascript" ></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="../dist/js/pages/dashboard.js"  type="text/javascript" ></script>-->
<!-- AdminLTE for demo purposes -->
<!--<script src="../dist/js/demo.js"  type="text/javascript" ></script>-->

<link rel="stylesheet" href="../../dist/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="../../dist/js/bootstrapValidator.js"  type="text/javascript" ></script>

    <script src="../dist/js/ckeditor.js"></script>
<script>
   //$(function () {//alert();
  //  // Replace the <textarea id="editor1"> with a CKEditor
  //  // instance, using default configuration.
  //  CKEDITOR.replace('editor1');
 //   //bootstrap WYSIHTML5 - text editor
 //   $(".textarea").wysihtml5();
 // });
</script>
<?php
    }
    else if($emailsms==1)
    {
?>
    
<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"  type="text/javascript" ></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"  type="text/javascript" ></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"  type="text/javascript" ></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"  type="text/javascript" ></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"  type="text/javascript" ></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"  type="text/javascript" ></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"  type="text/javascript" ></script>
    <link rel="stylesheet" href="../dist/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="../dist/js/bootstrapValidator.js"  type="text/javascript" ></script>
<!-- Page Script -->
<script>
//    alert();
  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
</script>
<?php
    }
    else if($multipleselect==1)
    {
?>
<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"  type="text/javascript" ></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"  type="text/javascript" ></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<!-- date-range-picker -->
<script src="../dist/js/moment.min.js"  type="text/javascript" ></script>
<script src="../plugins/daterangepicker/daterangepicker.js"  type="text/javascript" ></script>
<!-- bootstrap datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"  type="text/javascript" ></script>
<!-- bootstrap color picker -->
<script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"  type="text/javascript" ></script>
<!-- bootstrap time picker -->
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"  type="text/javascript" ></script>
<!-- SlimScroll 1.3.0 -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"  type="text/javascript" ></script>
<!-- iCheck 1.0.1 -->
<script src="../plugins/iCheck/icheck.min.js"  type="text/javascript" ></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"  type="text/javascript" ></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"  type="text/javascript" ></script>


    <link rel="stylesheet" href="../../dist/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="../../dist/js/bootstrapValidator.js"  type="text/javascript" ></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>
<?php
    }
    else
    {
?>

<!-- /.content-wrapper 
<script src="../https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"  type="text/javascript"></script>
-->
<script src="../dist/js/jquery.min.js"  type="text/javascript" ></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript" ></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"  type="text/javascript" ></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"  type="text/javascript" ></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"  type="text/javascript" ></script>
<!-- daterangepicker -->
<script src="../dist/js/moment.min.js"  type="text/javascript" ></script>
<script src="../plugins/daterangepicker/daterangepicker.js"  type="text/javascript" ></script>
<!-- datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"  type="text/javascript" ></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"  type="text/javascript" ></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"  type="text/javascript" ></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"  type="text/javascript" ></script>
<!-- AdminLTE for demo purposes 
<script src="../dist/js/demo.js"  type="text/javascript" ></script>-->
<!-- page script -->
    <link rel="stylesheet" href="../../dist/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="../../dist/js/bootstrapValidator.js"  type="text/javascript" ></script>
<?php
    if($shwtable==1)
    {
?>
    
<script>
  $(function () {
    $("#example1").DataTable({
      "scrollX": true,
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
});
    $('#example2').DataTable({
      "scrollX": true,
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
    }
    else
    {
?>
<script>
  $(function () {
    $("#example1").DataTable({"scrollX": true});
    $('#example2').DataTable({
      "scrollX": true,
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
    }
    }
?>
<style>

</style>
</body>
</html>
