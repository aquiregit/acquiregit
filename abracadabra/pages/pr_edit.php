<?php
  $fa="fa-newspaper-o";
  $maintitle="Press Release";
  $title="Create Press Release";
  $mainmenu="ADMINISTRATOR";
  $menu="PR";
  
    include "header.php";
 
  $istable=1;
  $emailsms=1;
  if($_GET['id']!="")
	{  
    $id=1;
    $sqlquery=mysqli_query($con,"select * from press_releases where pr_id=".$_GET['id']." ");
    $pr=mysqli_fetch_array($sqlquery);

    $date=date('Y-m-d', strtotime($pr['published_date']));
  }
  else
  {  
    $id=0;  
    $date=date('Y-m-d');
  }
  ?>

   <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <link rel="stylesheet" href=../"plugins/select2/select2.css">
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add PR</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<form action="ajax-editpr.php"  id="emailvalidefrm" method="POST" onsubmit="alert('PR Edited Successfully!');">
			
			 <div class="form-group">
                    <label>PR Id</label>
                <input type="text" class="form-control" placeholder="PR Id" id="prid" value="<?php echo $pr['pr_id'];?>" name="prid" READONLY>
               
              
              </div>
			
		   <div class="form-group">
                    <label>PR Title</label>
                <input type="text" class="form-control" placeholder="PR Title" id="prtitle" value="<?php echo $pr['pr_title'];?>" name="prtitle" required>
               
              
              </div>
			  
			 
			 <div class="form-group">
                    <label>Published Date</label>
                <input type="date" data-date="" value="<?php echo $pr['published_date'];?>" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="pdate" name="pdate" required>
                
						<script>
						$("input").on("change", function() {
							this.setAttribute(
								"data-date",
								moment(this.value, "YYYY-MM-DD")
								.format( this.getAttribute("data-date-format") )
							)
						}).trigger("change")
						</script>
			 </div>

		    
		    
       
                

              <div class="form-group">
                    <label>Summary</label>
                    <textarea id="editor1" class="compose-textarea" name="editor1" value="" style="height: 300px" required><?php echo $pr['pr_desc'];?></textarea>
                  <span id="msgsummary" class="alert hide">PR description should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->

              </div>

             <div class="form-group">
 
                    <label>PR images</label>
                <input type="text" class="form-control" placeholder="Type name of image with extension" id="pr-img" name="pr-img" value=""> 
                <span id="pr-img" class="alert hide">Add the PR images</span>
              
              </div>

			  <div class="form-group">
                    <label>Tags </label>
                <input class="form-control" placeholder="tag" id="tag" value="<?php echo $pr['tags'];?>" name="tag" required >
                <span id="msgwritten" class="alert hide">Add the Tags</span>
              
              </div>



            <!-- /.box-body -->
                  <small id="msgsuccess"  class="alert hide" style="color:#00A41E;">
                       <t class="text-center" ><strong>Success!</strong> PR Edited.</t><br>
                  </small>

            <div class="box-footer">
              <div class="pull-right">
                <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
               
			   <input type="submit" class="btn btn-default" id="editpr" name="editpr" data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To Create A PR&nbsp;&nbsp;">

              </div>
              <!--<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
            </div>
            <!-- /.box-footer -->
          </div>
          </form>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js'></script>
<script>

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

  });




</script>

