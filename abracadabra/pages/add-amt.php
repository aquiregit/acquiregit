<?php
$fa="fa-user";
$maintitle="Amount";
$title="Add Amount";
$mainmenu="ADMINISTRATOR";
$menu="ADDCARD";
include "header.php";
$istable=1;
?>
<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Add Amount
          </h3>
        </div>
        <div class="box-body">
          <!---------------------------------------------------------------------------->  
          <div class="container">
            <!-- edit form column -->
            <div class="col-md-9 personal-info">
          <!------------------------------------------------------>  

           <form id="regform" class="form-horizontal"  method="POST"  role="form" data-bv-message="This value is not valid"
                      data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                      data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                      data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
                <div class="form-group">
                  <label class="col-lg-4 control-label">Payment Type:
                  </label>
                  <div class="col-lg-8">
                    <input class="form-control" id="ptype" name="ptype" type="text" value=""  >
                   <small id="msgtype"  class="alert hide"  >
                        <t  class="pull-left" style="color:red;"><strong>Warning !</strong> Payment type should not be blank....</t>
                   </small>
                   <small id="msgadd"  class="alert hide"  >
                        <t  class="pull-left" style="color:red;"><strong>Warning !</strong> Payment type already added....</t>
                   </small>

                  </div>
                </div>

                <div class="form-group">
                  <label class="col-lg-4 control-label">Payment Amount:
                  </label>
                  <div class="col-lg-8">
                    <input class="form-control" id="amt" name="amt" type="text" value=""  >
                   <small id="msgamt"  class="alert hide"  >
                        <t  class="pull-left" style="color:red;"><strong>Warning !</strong> Payment Amount should not be blank....</t>
                   </small>

                  </div>
                </div>





              <div class="form-group">
                <label class="col-md-4 control-label">
                </label>
                <div class="col-md-8">
                  <input class="btn btn-primary" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" type="submit" id="save" name="save" onclick="return regform();"> &nbsp; <a href="paydata.php" class="btn  btn-primary" >View Payment Types</a>
                  <span>
                  </span>
                  <br>
                   <small id="msgsuccess"  class="alert hide">
                        <t  class="pull-left" style="color:green;"><strong>Success !</strong> Data Updated.</t><br>
                   </small>
                      <small id="msgfail"  class="alert hide">
                        <t  class="pull-left"> <strong>Warning! </strong> Something is Wrong.</t>
                      </small>
                      <small id="msgpresent"  class="alert hide">
                        <t  class="pull-left"> <strong>Warning! </strong> Data already present.</t>
                      </small>
                </div>
              </div>


                <hr>


          <!------------------------------------------------------>  
          </form>
      </div>
    </div>
    <!---------------------------------------------------------------------------->  
  </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
<script>

flag=1;
function regform()
{
  document.getElementById('save').disabled = true;



  if (!$('#msgsuccess').hasClass("hide"))
  {
    $('#msgsuccess').addClass("hide");
  };
  if (!$('#msgfail').hasClass("hide"))
  {
    $('#msgfail').addClass("hide");
  };
  if (!$('#msgpresent').hasClass("hide"))
  {
    $('#msgpresent').addClass("hide");
  };


  if (!$('#msgamt').hasClass("hide"))
  {
    $('#msgamt').addClass("hide");
  };
  if (!$('#msgtype').hasClass("hide"))
  {
    $('#msgtype').addClass("hide");
  };
  if (!$('#msgadd').hasClass("hide"))
  {
    $('#msgadd').addClass("hide");
  };



  var ptype = $.trim(document.getElementById('ptype').value);
  var amt = $.trim(document.getElementById('amt').value);

  if(ptype=="")
  {
        flag=0;
        $('#msgtype').removeClass("hide");
  }
  else if(ptype!="" && amt!="")
  {
         $.ajaxSetup({async: false});

    $.post('ajax-update-data.php',
      {
        ptype: ptype,
        amt: amt,
        checkdata:1 

      },
      function(res)
      {
        var string = $.trim(res);
        if (string == "false")
        {
            flag=1;
        }
        else
        {
            $('#msgpresent').removeClass("hide");
            flag=0;
        }

      });

  }     

  if(amt=="")
  {
        flag=0;
        $('#msgamt').removeClass("hide");
  }   

  //alert(flag);  
  if(flag==1)
  {    
    $.post('ajax-update-data.php',
      {
        ptype: ptype,
        amt: amt,

      },
      function(res)
      {
        var string = $.trim(res);
        if (string == "false")
        {
          //alert('Registration Successfull');
            $('#msgfail').removeClass("hide");
        }
        else
        {
            $('#msgsuccess').removeClass("hide");
        }
        document.getElementById('save').disabled = false;
      });
   }
  document.getElementById('save').disabled = false;
  return false;
}

</script>
