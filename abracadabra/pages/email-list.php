<?php
  $fa="fa-envelop";
  $maintitle="Email";
  $title="View Email";
  $mainmenu="ADMINISTRATOR";
  $menu="EMAIL";
  include "header.php";
 
  $istable=1;
?>

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!------------------------------------------------------------------------------------------------------->
    <div class="col-sm-12">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"> Email List
          </h3>
             &nbsp; <a href="excelImport.php" class="btn btn-default pull-right">Import Excel</a>
        </div>
        <div class="box-body">
          <!----------------------------------------------------------------------------> 
              <table id="example1" class="table table-striped table-bordered no-footer dtr-inline dataTable" >
                <thead>
                  <tr>
                    <th style="width:1%;">#</th>
                    <th style="width:15%;">Email Id</th>
                    <th style="width:30%;">Name</th>
                    <th style="width:3%;">Status</th>
                    <th style="width:3%;">Delete</th>

                  </tr>
                </thead>
<?php
                $cnt=1;
                $query2=@mysql_query("select * from Emailid order by email_id desc");
                while($row2=@mysql_fetch_array($query2))
                {
?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $row2['email'];?></td>
                    <td><?php echo $row2['name'];?></td>
                    <td><?php if($row2['status']==0) echo "Not Send"; else echo "Send";?></td>

                    <td><a href="" onclick="deleteEmail(<?php echo $row2['email_id'];?>)">delete</a> </td>
                </tr>

<?php
                    $cnt++;
                }
?>
                <tbody>
                </tbody>
             </table> 

          <!---------------------------------------------------------------------------->  
        </div>
     </div>
  </div>
<!------------------------------------------------------------------------------------------------------->
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include "footer.php";
?>
<script>
function deletereport(id)
{
        id=id;
    //alert(id);
    if(confirm('Are you sure want to delete Report ?'))
    {

      var variable="-1";
   $.post('ajax-reports.php',
      {
        id:id,condition:variable,

      },
      function(res)
      {
        var string = $.trim(res);
        alert(string);
        if (string == "true")
        {
          //alert('Registration Successfull');
          window.location = 'view-report.php';
        }
        else
        {
          alert('Something wrong');
        }
        document.getElementById('save').disabled = false;
      });



    }

}
</script>
