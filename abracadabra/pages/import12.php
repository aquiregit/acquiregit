<?php
  $fa="fa-envelop";
  $maintitle="Report";
  $title="Create Report";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTS";
    include "header.php";
 
  $istable=1;
  $emailsms=1;

  ?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
<?php
if(isset($_POST["Import"]))
{
		 $filename=$_FILES["file"]["tmp_name"];
                 $v=$_FILES["file"]["name"];
		 if($_FILES["file"]["size"] > 0)
		 {
                    $v1=substr(strrchr($v,'.'),1);
                    if($v1=="csv"){
		  	$file = fopen($filename, "r");$i=1;

	         while (($emapData = fgetcsv($file, 99999999, ",")) !== FALSE)
	         {
	            if($i!=1)
                {
                    $industry_id=""; 
                        $sql=mysqli_query($con,"SELECT * from categories where cat_name like '%".$emapData[10]."%' ");
                        $rows=mysqli_num_rows($sql);
                        if($rows>0)
                        {
                            $row=mysqli_fetch_array($sql);
                            $industry_id=$row['cat_id']; 
                        }
                        else
                        {
                            $sql=mysqli_query($con,"INSERT into categories(cat_name) values ('".$emapData[10]."')");
                            $industry_id=mysqli_insert_id(); 
                        }
                    
                        $query=mysqli_query($con,"SELECT * from reports where report_title='".$emapData[0]."' ");
                        $rows2=mysqli_num_rows($query);
                        if($rows2>0)
                        {
                         $updatesql = "UPDATE reports SET status='DUPLICATE' where report_title='".$empData[0]."'";
                            mysqli_query($con, $updatesql);
                        }
                        else
                        {   
                          $title1 = addslashes($emapData[0]);
                                      $pages = addslashes($emapData[1]);
                                      $no_taf = addslashes($emapData[2]);
                                      $singleuser_price = addslashes($emapData[3]);
                                      $enterpriseuser_price = addslashes($emapData[4]);  
                                      $published_date = addslashes($emapData[5]);
                                      $summary = addslashes($emapData[6]); 
                                      $file_name = $title1.'.pdf';
                                      $toc = addslashes($emapData[8]);
                                      $taf = addslashes($emapData[9]);
                                      $catlog = addslashes($emapData[10]);
                                      $publisher_code = addslashes($emapData[11]);
                                      $status="INSERTED";
                          
                    // Generate automatically 
                          //Meta-title
                          //$meta_title = $title;
                          //Meta-Description
                          $meta_description = $title1 .' Published By :acquiremarketresearch.com';
                          //Meta-Keywords
                           $str = stristr($title1,'Market',true);
                          $meta_keywords = ucwords($str.'Market, Market research, market research reports, industry reports, Regions reports, industry analysis, business research');
                         //print_r($meta_keywords); die;

                            $result=mysqli_query($con,"INSERT INTO `reports`(`report_title`,`pages`,`no_taf`,`singleuser_price`,`enterpriseuser_price`,`published_date`,`summary`,`file_name`,`toc`,`taf`,`catlog`,`cat_id`,`publisher_code`,`meta_keywords`,`meta_desc`,`status`,`slug`, `time`) VALUES ('".$title1."','".$pages."','".
                              $no_taf."','".$singleuser_price."','".$enterpriseuser_price."','".$published_date."','".$summary."','".$file_name."','".$toc."','".$taf."','". $catlog."',".$industry_id.",'".$publisher_code."','". $meta_keywords."','".$meta_description."' ,'INSERTED','','')");
                        
                        }
						
				    if(! $result )
				    {
                           echo "<script type=\"text/javascript\">
							    alert(\"File has contains some Duplicates Reports. Please Kindly check File.\");
							    window.location = \"reports.php\"
						    </script>";
				    }
                }$i++;
                unset($emapData);
                  }
	         }
                  else{
                      echo "<script type=\"text/javascript\">alert(\"Invalid File / Extension! Please Upload CSV. Uploaded file's extension was : ".$v1.".\");
                     window.location = \"reports.php\";
                   </script>";
                   }
	         fclose($file);
	         
	         echo "<script type=\"text/javascript\">
						alert(\"CSV File has been successfully Imported.\");
						window.location = \"reports.php\"
					</script>";

		 }
	}	 

?>		 
            
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>

