<?php
  $fa="fa-envelop";
  $maintitle="Report";
  $title="Manually Create Report";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTSM";
    include "header.php";
 
  $istable=1;
  $reportadd=1;

  if($_GET['id']!="")
  {  
    $id=1;
    $sqlquery=mysql_query("select * from reports where rep_id=".$_GET['id']." ");
    $rid=mysql_fetch_array($sqlquery);
  }
  else
    $id=0;  

  ?>
   <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <link rel="stylesheet" href=../"plugins/select2/select2.css">
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Report Manually</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form id="emailvalidefrm" method="POST" role="form">
              <input type="hidden" id="condition" name="condition" value="<?php echo $id;?>">  
              <input type="hidden" id="rep_id" name="rep_id" value="<?php echo $rid['rep_id'];?>">  

              <div class="form-group">
                    <label>Catalog</label>
                <select class="form-control" id="industryid" name="industryid" required >
                <?php
                    $sql=mysql_query("select * from industries");
                    while($ind=mysql_fetch_array($sql))
                    {
                        echo "<option value='".$ind['c_id']."' >".$ind['industry']."</option>";
                    }         
                ?>
                </select>
                <span id="msgind" class="alert hide">Select Industry</span>
              
              </div>


              <div class="form-group">
                    <label>Report Title</label>
                <input class="form-control" placeholder="Report Title" id="reporttitle" name="reporttitle" required >
                <span id="msgreporttitle" class="alert hide">Name should not be blank</span>
              
              </div>

              <div class="form-group">
                    <label>Pages Count</label>
                <input type="tel" class="form-control" placeholder="Pages" id="pages" name="pages" required >
                <span id="msgPages" class="alert hide">Pages Count should not be blank</span>
                <span id="msgPagesNo" class="alert hide">Pages Count should be number only</span>
              
              </div>


              <div class="form-group">
                    <label>Single Price</label>
                <input type="tel" class="form-control" placeholder="" id="sprice" name="sprice" required >
                <span id="msgsp" class="alert hide">Single price should not be blank</span>

              
              </div>

              <div class="form-group">
                    <label>Enterprise Price</label>
                <input type="tel" class="form-control" placeholder="" id="eprice" name="eprice" required >
                <span id="msgep" class="alert hide">Enterprise Price should not be blank</span>

              
              </div>


              <div class="form-group">
                    <label>Published Date</label>
                <input type="date" data-date="" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="pdate" name="pdate" required value="<?php echo date('Y-m-d');?>">
                <span id="msgpd" class="alert hide">Published Date should not be blank</span>
<script>
$("input").on("change", function() {
    this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
        .format( this.getAttribute("data-date-format") )
    )
}).trigger("change")
</script>
              
              </div>


              <div class="form-group">
                    <label>Summary</label>
                    <textarea id="editor1" class="compose-textarea" name="editor1" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty" ></textarea>
                  <span id="msgsummary" class="alert hide">Summary should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->

              </div>



              <div class="form-group">
                    <label>Table of Contents</label>
                    <textarea id="editor2" class="compose-textarea" name="editor2" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty" ></textarea>
                  <span id="msgtoc" class="alert hide">Table Of Contents should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->

              </div>

              <div class="form-group">
                    <label>Tables and Figures</label>
                    <textarea id="editor3" class="compose-textarea" name="editor3" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty" ></textarea>
                  <span id="msgtaf" class="alert hide">Tables and Figures should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->

              </div>



            <!-- /.box-body -->
                  <small id="msgsuccess"  class="alert hide" style="color:#00A41E;">
                       <t class="text-center" ><strong>Success!</strong> Report Added.</t><br>
                  </small>
                  <small id="msgrepeat"  class="alert hide"  >
                       <t  class="pull-left"><strong>Warning!</strong> Industry May be Repeated.</t><br>
                  </small>

            <div class="box-footer">
              <div class="pull-right">
                <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
               <a href="view-industries.php" class="btn btn-primary"  data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To View Industry&nbsp;&nbsp;" ><i class="fa fa-envelope-o"></i> View</a> <button type="button" class="btn btn-primary" id="reportadd" data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To Create Industry&nbsp;&nbsp;" onclick="return saveindustry();"><i class="fa fa-envelope-o"></i> Save</button>

              </div>
              <!--<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
            </div>
            <!-- /.box-footer -->
          </div>
          </form>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js'></script>
<script>

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();


  });

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor2');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

  });

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor3');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

  });



  function saveindustry()
{  

  flag=1;
  if (!$('#msgind').hasClass("hide"))
  {
    $('#msgind').addClass("hide");
  };
  if (!$('#msgreporttitle').hasClass("hide"))
  {
    $('#msgreporttitle').addClass("hide");
  };
  if (!$('#msgPages').hasClass("hide"))
  {
    $('#msgPages').addClass("hide");
  };

  if (!$('#msgPagesNo').hasClass("hide"))
  {
    $('#msgPagesNo').addClass("hide");
  };

  if (!$('#msgsp').hasClass("hide"))
  {
    $('#msgsp').addClass("hide");
  };
  if (!$('#msgep').hasClass("hide"))
  {
    $('#msgep').addClass("hide");
  };

  if (!$('#msgpd').hasClass("hide"))
  {
    $('#msgpd').addClass("hide");
  };
  if (!$('#msgsummary').hasClass("hide"))
  {
    $('#msgsummary').addClass("hide");
  };
  if (!$('#msgtoc').hasClass("hide"))
  {
    $('#msgtoc').addClass("hide");
  };
  if (!$('#msgtaf').hasClass("hide"))
  {
    $('#msgtaf').addClass("hide");
  };





  if (!$('#msgsuccess').hasClass("hide"))
  {
    $('#msgsuccess').addClass("hide");
  };

/*
  var industryid = $.trim(document.getElementById('industryid').value);
  if(industryid=="")
  {

  	flag=0;
    $('#msgind').removeClass("hide");  	
  }

  var reporttitle = $.trim(document.getElementById('reporttitle').value);
  if(reporttitle=="")
  {

  	flag=0;
    $('#msgreporttitle').removeClass("hide");  	
  }

  var pages = $.trim(document.getElementById('pages').value);
  if(pages=="")
  {

  	flag=0;
    $('#msgPages').removeClass("hide");  	
  }
  if(!isInt(pages) && pages!="")
  {

  	flag=0;
    $('#msgPagesNo').removeClass("hide");  	
  }

  var sprice = $.trim(document.getElementById('sprice').value);
  if(sprice=="")
  {

  	flag=0;
    $('#msgsp').removeClass("hide");  	
  }



  var eprice = $.trim(document.getElementById('eprice').value);
  if(eprice=="")
  {

  	flag=0;
    $('#msgep').removeClass("hide");  	
  }

  var date = $.trim(document.getElementById('pdate').value);

  var editor  = CKEDITOR.instances['editor1'].getData();
  Summary=editor;
  if(Summary=="")
  {

  	flag=0;
    $('#msgsummary').removeClass("hide");  	
  }



  var editor  = CKEDITOR.instances['editor2'].getData();
  toc=editor;
  if(toc=="")
  {

  	flag=0;
    $('#msgtoc').removeClass("hide");  	
  }


  var editor  = CKEDITOR.instances['editor3'].getData();
  taf=editor;
  if(taf=="")
  {

  	flag=0;
    $('#msgtaf').removeClass("hide");  	
  }

  var condition = $.trim(document.getElementById('condition').value);
  var rep_id = $.trim(document.getElementById('rep_id').value);
*/

    if(flag==1)
    {
         $.post('ajax-reports.php',
          {
             /* industryid:industryid, 
              reporttitle:reporttitle,
              pages:pages, 
              sprice:sprice,
              eprice:eprice,
              date:date, 
              Summary:Summary,
              toc:toc,
              taf:taf,
              condition:condition*/
          },
          function(res)
          {
            var string = $.trim(res);
            if (string == "true")
            {
              $("#msgsuccess").removeClass("hide");
            }
          });
    }

  document.getElementById('reportadd').disabled = false;
  return false;
}

function isInt(value) {
  if (isNaN(value)) {
    return false;
  }
  var x = parseFloat(value);
  return (x | 0) === x;
}
</script>

