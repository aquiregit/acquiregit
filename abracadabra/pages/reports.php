<?php
  $fa="fa-envelop";
  $maintitle="Report";
  $title="Create Report";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTS";
    include "header.php";
 
  $istable=1;
  $emailsms=1;

  ?>
		<link rel="stylesheet" href="bootstrap-custom.css">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<form class="form-horizontal well" action="import1.php" method="POST" name="upload_excel" enctype="multipart/form-data">
					<fieldset>
						<legend>Import CSV File</legend>
						<div class="control-group">
							<div class="control-label pull-left">
								<label>CSV File:</label>
							</div>
							<div class="controls">
								<input type="file" name="file" id="file" class="form-control" required>
                                <span id="msgblank" class="alert hide">Please select the file</span>
							</div>
						</div>
						
						<div class="control-group">
							<div class="controls"><br>
							<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading..." >Upload</button>
							</div>
						</div>
					</fieldset>
				</form>

           
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>
<script>
/*
function checkdata()
{
  flag=1;
  if (!$('#msgblank').hasClass("hide"))
  {
    $('#msgblank').addClass("hide");
  };

  document.getElementById('submit').disabled = true;
  var file = $.trim(document.getElementById('file').value);
  if(file=="")
  {
      $('#msgblank').removeClass("hide");
      document.getElementById('submit').disabled = false;
      return false;
  }    
  else
  {
      $("#regvalid").submit();
      return true;

  }  

  

}
*/
</script>

