<?php
    $title="Home";
    include "header.php";
?>
<!-- Content Wrapper. Contains page content -->
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary box-solid">
		<div style="margin-bottom:10px;">
            <center><t class="welcome">Welcome to AMR Managment System </t></center>
            <!-- /.box-tools -->
		</div>
          <!-- /.box-header -->
          <div class="box-body">
            <!----------------------------------------------------------------------------------------->
<style>
.welcome
{
	text-decoration: underline;
	font-weight:bold;
	font-size:32px;
}
.hello
{
	font-size:21px;
	font-weight:bold;
    text-decoration: underline;
}
.maincontent
{
	font-size:17px;
}
#olmenu .menu
{
	font-size:19px;
	font-weight:bold;
	margin-bottom:1%;
}
.submenu
{ 
	font-size:17px;
	font-weight:bold;
}
.content
{
	font-weight:normal;
	font-size:15px;
}

</style>
		<div style="margin-bottom:10px;">
		<t class="hello" >Hello  Admin,</t><br>
			<div style="margin-top:1%;">
			<t class="maincontent">For easy navigation of  System, we have provided many tabs on Left Side of this page. Details of the tabs are as follows,</t><br>
			</div>
		</div>
		<ol type="1" id="olmenu">
<!--------------------Administrator------------------------------------------->
			<li class="menu" style="margin-top:1%;"><u><t>Administrator</t></u>
				<ol type="1"><!--
					<li class="submenu"><u>Missed call Number</u><t class="content">: Here you can add multiple Numbers for your organization to use Missed call System.</t>
					</li>--><!--
					<li class="submenu"><u>Branch Master</u><t class="content">: Here you can create Multiple Branches for your organization and  also view branch wise details.</t>
					</li>
					<li class="submenu"><u>Manager</u><t class="content">: Here you can create Multiple Manager for your branches and view working details.</t>
					-->
					<!---</li>
					<li class="submenu"><u>Executive</u><t class="content">: Here you can create executive logins for specific branch or branches and also view login details.<br>Note: Default account of Branch Master, Manager and Executive is created by the system.Which you can de-activate later.</t>
					</li>
					<li class="submenu"><u>Upload ID Proof</u><t class="content">Here you can <b>Browse</b> and upload Gov. ID Proof.</t>
					</li>
					<li class="submenu"><u>Surrender Account</u><t class="content">: You can surrender your account if you want. Please contact Lsoft Technologies support team any time for the reactivation of your account.</t>
					</li>
                    -->

				</ol>
			</li>
<!--------------------Template------------------------------------------->
<!--
			<li class="menu"><u><t >Template</t></u>
				<ol type="1">
					<li class="submenu"><u>Create Sender ID</u><t class="content">: Here you can create 6 digit sender id.(alphanumeric)</t>
					</li>
					<li class="submenu"><u>SMS Template</u><t class="content">: Here you can create and view multiple template for SMS.</t>
					</li>
					<li class="submenu"><u>Email Template</u><t class="content">: You can create and view multiple Email Template.</t>
					</li>

				</ol>
			</li>
-->
<!--------------------History------------------------------------------->
<!--
			<li class="menu"><u><t >History</t></u>
				<t class="content">: Here you can view all the call,SMS transactions and email transaction details with date and time.</t>
			</li>
<!--------------------CRM------------------------------------------->
<!--			<li class="menu"><u><t >CRM</t></u>
				<t class="content">: Here you can create and modify groups for saving customer numbers.You can view and maintain the follow up reports / Call details.</t>
			</li>
<!--------------------Billing & Payment-------------------------------------><!--
			<li class="menu"><u><t >Billing & Payment</t></u>
				<t class="content">: Here you can view and Download all bill details for your  related services.</t>
			</li>-->
<!--------------------Service Features------------------------------------->
<!--
			<li class="menu"><u><t >Service Features</t></u>
				<t class="content">: You can take backup your account from here and save backup file in safe place for recovery of your account at anytime. You can block any Customer Number and also get daily Email statement.</t>
			</li>-->
		</ol>
            <!----------------------------------------------------------------------------------------->
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
  </div>

<?php
    include "footer.php";
?>

