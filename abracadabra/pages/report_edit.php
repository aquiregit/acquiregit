<?php
  $fa="fa-envelop";
  $maintitle="Industry";
  $title="Create Industry";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTSM";
    include "header.php";
 
  $istable=1;
  $emailsms=1;
   if($_GET['id']!="")
	{  
    $id=1;
    $sqlquery=mysqli_query($con,"SELECT * from reports where report_id=".$_GET['id']." ");
    $report=mysqli_fetch_array($sqlquery);

    $date=date('Y-m-d', strtotime($report['published_date']));
  }
  else
  {  
    $id=0;  
    $date=date('Y-m-d');
  }
  ?>
   <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <link rel="stylesheet" href=../"plugins/select2/select2.css">
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"  type="text/javascript" ></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"  type="text/javascript" ></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"  type="text/javascript" ></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.6.0/js-yaml.min.js'></script>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form id="emailvalidefrm" method="POST" role="form" action="ajax-editreport.php" onsubmit="alert('Report Added Successfully!');">


            <div class="form-group">
                    <label>Report Id</label>
                <input type="text" class="form-control" placeholder="Report Id" id="id" value="<?php echo $report['report_id'];?>" name="id" readonly>
               
              
              </div>

              <div class="form-group">
                    <label>Report Title</label>
                <input class="form-control" placeholder="Report Title" id="reporttitle" value="<?php echo $report['report_title'];?>" name="reporttitle" required >
                <span id="msgreporttitle" class="alert hide">Name should not be blank</span>
              
              </div>

              <div class="form-group">
                    <label>Pages</label>
                <input type="tel" class="form-control" placeholder="Pages" id="pages" name="pages" value="<?php echo $report['pages'];?>" required >
                <span id="msgPages" class="alert hide">Pages Count should not be blank</span>
                <span id="msgPagesNo" class="alert hide">Pages Count should be number only</span>
              
              </div>
		
				<div class="form-group">
                    <label>Numbers of Tables and Figures</label>
                <input type="tel" class="form-control" placeholder="t&f" id="taf" name="taf" value="<?php echo $report['no_taf'];?>" required >
                <span id="msgtaf" class="alert hide">Numbers of Tables and Figures Count should not be blank</span>
                <span id="msgtaf" class="alert hide">Numbers of Tables and Figures Count should be number only</span>
              
              </div>

              <div class="form-group">
                    <label>Single User Price</label>
                <input type="tel" class="form-control" value="<?php echo $report['singleuser_price'];?>" placeholder="" id="sprice" name="sprice" required >
                <span id="msgsp" class="alert hide">Single price should not be blank</span>

              
              </div>

              <div class="form-group">
                    <label>Enterprise User Price</label>
                <input type="tel" class="form-control" value="<?php echo $report['enterpriseuser_price'];?>" placeholder="" id="eprice" name="eprice" required >
                <span id="msgep" class="alert hide">Enterprise Price should not be blank</span>

              
              </div>


             <div class="form-group">
                    <label>Published Date</label>
                <input type="date" value="<?php echo $report['published_date'];?>" data-date="" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="pdate" name="pdate" required>
                
						<script>
						$("input").on("change", function() {
							this.setAttribute(
								"data-date",
								moment(this.value, "YYYY-MM-DD")
								.format( this.getAttribute("data-date-format") )
							)
						}).trigger("change")
						</script>
			 </div>
			 
			 <div class="form-group">
                    <label>Summary</label>
                    <textarea id="editor1" class="compose-textarea" name="editor1" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty"><?php echo $report['summary'];?></textarea>
                  <span id="msgsummary" class="alert hide">Summary should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->
			<script>
					CKEDITOR.replace('editor1');
					 $("#editor1").wysihtml5();
			</script>
              </div>
		
				<div class="form-group">
                    <label>File Name</label>
                <input class="form-control" placeholder="File Name" id="filename" value="<?php echo $report['file_name'];?>" name="filename" required >
                <span id="msgreporttitle" class="alert hide">Name should not be blank</span>
              
              </div>


              <div class="form-group">
                    <label>Table of Contents</label>
                    <textarea id="editor2"  class="compose-textarea" name="editor2" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty"><?php echo $report['toc'];?></textarea>
                  <span id="msgtoc" class="alert hide">Table Of Contents should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->
				<script>
					CKEDITOR.replace('editor2');
					 $("#editor2").wysihtml5();
			</script>
              </div>

              <div class="form-group">
                    <label>Tables and Figures</label>
                    <textarea id="editor3" class="compose-textarea" name="editor3" style="height: 300px" required data-bv-notempty-message="The Body is required and cannot be empty"><?php echo $report['taf'];?></textarea>
                  <span id="msgtaf" class="alert hide">Tables and Figures should not be blank</span>
               <!--   <textarea id="compose-textarea" class="form-control" style="height: 300px" required></textarea>-->
               
          <!-- /.box -->
					<script>
					CKEDITOR.replace('editor3');
					 $("#editor3").wysihtml5();
			</script>
              </div>
			  <div class="form-group">
                    <label>Catalog</label>
                <select class="form-control" id="industry" name="industry" required >
                <?php
                    $sql=mysqli_query($con,"SELECT * FROM categories");
                    while($ind=mysqli_fetch_array($sql))
                    {
                            if($report['cat_id']==$ind['cat_id'])
                                echo "<option value='".$ind['cat_id']."' selected>".$ind['cat_name']."</option>";
                            else
                                echo "<option value='".$ind['cat_id']."' >".$ind['cat_name']."</option>";
                    }         
                ?>
                </select>
                <span id="msgind" class="alert hide">Select Industry</span>
              
              </div>

				<div class="form-group">
                    <label>Publisher Code</label>
                <input type="tel" class="form-control" placeholder="Publisher Code" id="publishercode" name="publishercode" value="<?php echo $report['publisher_code'];?>" required >
                <span id="msgpublisher" class="alert hide">Publisher Count should not be blank</span>
                <span id="msgpublisher" class="alert hide">Publisher Count should be number only</span>
              
              </div>
				
				<div class="form-group">
                    <label>Meta keywords</label>
                <input class="form-control" placeholder="Meta Keywords" id="metakey" value="<?php echo $report['meta_keywords'];?>" name="metakey" required >
                <span id="msgkey" class="alert hide">Keywords should not be blank</span>
              
              </div>

			  
			   <div class="form-group">
                    <label>Meta Description</label>
                <input class="form-control" placeholder="Meta Description" id="metadesc" value="<?php echo $report['meta_desc'];?>" name="metadesc" required >
                <span id="metadesc" class="alert hide">Description should not be blank</span>
              
              </div>
				
            <!-- /.box-body -->
                  <small id="msgsuccess"  class="alert hide" style="color:#00A41E;">
                       <t class="text-center" ><strong>Success!</strong> Report Edited.</t><br>
                  </small>
                  


            <div class="box-footer">
              <div class="pull-right">
                <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>-->
                
			   <input type="submit" class="btn btn-primary" id="mreport" name="mreport" data-toggle="tooltip" title="&nbsp;&nbsp; Click Here To Add a Report&nbsp;&nbsp;">

              </div>
              <!--<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>-->
            </div>
            <!-- /.box-footer -->
          </div>
          </form>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>

<script>

  /*$(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $("#editor1").wysihtml5();

  });
    
	*/
	
/*

  function saveindustry()
{  

  flag=1;
  if (!$('#msgind').hasClass("hide"))
  {
    $('#msgind').addClass("hide");
  };
  if (!$('#msgreporttitle').hasClass("hide"))
  {
    $('#msgreporttitle').addClass("hide");
  };
  if (!$('#msgPages').hasClass("hide"))
  {
    $('#msgPages').addClass("hide");
  };

  if (!$('#msgPagesNo').hasClass("hide"))
  {
    $('#msgPagesNo').addClass("hide");
  };

  if (!$('#msgsp').hasClass("hide"))
  {
    $('#msgsp').addClass("hide");
  };
  if (!$('#msgep').hasClass("hide"))
  {
    $('#msgep').addClass("hide");
  };

  if (!$('#msgpd').hasClass("hide"))
  {
    $('#msgpd').addClass("hide");
  };
  if (!$('#msgsummary').hasClass("hide"))
  {
    $('#msgsummary').addClass("hide");
  };
  if (!$('#msgtoc').hasClass("hide"))
  {
    $('#msgtoc').addClass("hide");
  };
  if (!$('#msgtaf').hasClass("hide"))
  {
    $('#msgtaf').addClass("hide");
  };
  if (!$('#msgp').hasClass("hide"))
  {
    $('#msgp').addClass("hide");
  };





  if (!$('#msgsuccess').hasClass("hide"))
  {
    $('#msgsuccess').addClass("hide");
  };

  var industryid = $.trim(document.getElementById('industryid').value);
  if(industryid=="")
  {

  	flag=0;
    $('#msgind').removeClass("hide");  	
  }

  var reporttitle = $.trim(document.getElementById('reporttitle').value);
  if(reporttitle=="")
  {

  	flag=0;
    $('#msgreporttitle').removeClass("hide");  	
  }

  var pages = $.trim(document.getElementById('pages').value);
  if(pages=="")
  {

  	flag=0;
    $('#msgPages').removeClass("hide");  	
  }
  if(!isInt(pages) && pages!="")
  {

  	flag=0;
    $('#msgPagesNo').removeClass("hide");  	
  }

  var sprice = $.trim(document.getElementById('sprice').value);
  if(sprice=="")
  {

  	flag=0;
    $('#msgsp').removeClass("hide");  	
  }



  var eprice = $.trim(document.getElementById('eprice').value);
  if(eprice=="")
  {

  	flag=0;
    $('#msgep').removeClass("hide");  	
  }

  var date = $.trim(document.getElementById('pdate').value);

  var editor  = CKEDITOR.instances['editor1'].getData();
  Summary=editor;
  if(Summary=="")
  {

  	flag=0;
    $('#msgsummary').removeClass("hide");  	
  }



  var editor  = CKEDITOR.instances['editor2'].getData();
  toc=editor;
  if(toc=="")
  {

  	flag=0;
    $('#msgtoc').removeClass("hide");  	
  }


  var editor  = CKEDITOR.instances['editor3'].getData();
  taf=editor;
/*
  if(taf=="")
  {

  	flag=0;
    $('#msgtaf').removeClass("hide");  	
  }
*/

/*  var condition = $.trim(document.getElementById('condition').value);
  var rep_id = $.trim(document.getElementById('rep_id').value);


    if(flag==1)
    {
         $.post('ajax-reports.php',
          {
              industryid:industryid, 
              reporttitle:reporttitle,
              pages:pages, 
              sprice:sprice,
              eprice:eprice,
              date:date, 
              Summary:Summary,
              toc:toc,
              taf:taf,
              condition:condition,rep_id:rep_id
          },
          function(res)
          {
            var string = $.trim(res);
            if (string == "true")
            {
              $("#msgsuccess").removeClass("hide");
            }
            else if(string == "PRESENT")
            {
              $("#msgp").removeClass("hide");            	
            }
          });
    }

  document.getElementById('reportadd').disabled = false;
  return false;
}

function isInt(value) {
  if (isNaN(value)) {
    return false;
  }
  var x = parseFloat(value);
  return (x | 0) === x;
}*/
</script>

