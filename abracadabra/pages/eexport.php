<?php
if(isset($_POST['Export'])) {
    ini_set('memory_limit', '-1');
    ini_set("display_errors", "1");
    error_reporting(E_ALL);

    include '../lib/connect.php';
    $base_url = "https://www.acquiremarketresearch.com/";
    $sample_url = $base_url . "sample-request/";
    $enquire_url = $base_url . "enquire-before/";
    $discount_url = $base_url . "discount-request/";
    function keepXLines($str, $num=10) {
        $lines = explode("\n", $str);
        $firsts = array_slice($lines, 0, $num);
        return implode("\n", $firsts);
    }

    $fromDate = $_POST['from_date'];
    $toDate   = $_POST['to_date'];
    
    // OR time = '0000-00-00 00:00:00'
    $sql = "SELECT * FROM reports WHERE time BETWEEN '$fromDate' AND '$toDate'  ORDER BY report_id";
    $results = mysqli_query($con, $sql);
    
    require "../PHPExcel-1.8/Classes/PHPExcel.php";
    require "../PHPExcel-1.8/Classes/PHPExcel/Writer/Excel5.php"; 

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Report Id');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Report Title');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Summary');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'TOC');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Report Url');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Sample url');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Enquire url');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Discount url');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Publisher Code');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Uploaded Datetime');
     $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Category');
    
    $rowCount = 2;
    while($row = mysqli_fetch_assoc($results)) {
        //print_r($row['summary']); echo "<br><br><br>"; str_replace('–', '-', 
        //print_r(keepXLines($row['summary'], 40));exit;
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $row['report_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, utf8_encode($row['report_title']));
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, utf8_encode( $row['summary']));
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, keepXLines(utf8_encode( $row['toc'] ), 40));
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $row['slug']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $sample_url .  $row['report_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $enquire_url .  $row['report_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $discount_url .  $row['report_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $row['publisher_code']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $row['time']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $row['catlog']);
        $rowCount++;
    }
    
    //Report Title	Summary/ Description	Request sample URL	Main Report URL	Enquire before buying URL	TOC	Request Discount URL		
    //$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    header('Content-type: application/vnd.ms-excel');

    // It will be called file.xls
    header('Content-Disposition: attachment; filename="file.xls"');
    
    // Write file to the browser
    $objWriter->save('php://output');
}
?>

<?php
  $fa="fa-envelop";
  $maintitle="Report";
  $title="Export Report";
  $mainmenu="ADMINISTRATOR";
  $menu="REPORTS";
    include "header.php";
 
  $istable=1;
  $emailsms=1;

  ?>
		<link rel="stylesheet" href="bootstrap-custom.css">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Report Export</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<form class="form-horizontal well" action="eexport.php" method="POST" name="upload_excel">
					<fieldset>
						<legend>Export Excel File</legend>
						
						<div class="form-group">
                            <label>From Date</label>
                            <input type="date" data-date="" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="from_date" name="from_date" required>
            			</div>
            			
            			<div class="form-group">
                            <label>To Date</label>
                            <input type="date" data-date="" data-date-format="DD MM YYYY" class="form-control" placeholder="" id="to_date" name="to_date" required>
            			</div>
						
						<div class="control-group">
							<div class="controls"><br>
							<button type="submit" id="submit" name="Export" class="btn btn-primary button-loading" data-loading-text="Loading..." value="Export">Export</button>
							</div>
						</div>
					</fieldset>
				</form>
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
  include "footer.php";
?>

<script>
	$("input").on("change", function() {
		this.setAttribute(
			"data-date",
			moment(this.value, "YYYY-MM-DD")
			.format( this.getAttribute("data-date-format") )
		)
	}).trigger("change")
</script>