<?php
$fa        = "fa-envelop";
$maintitle = "Report";
$title     = "Create Report";
$mainmenu  = "ADMINISTRATOR";
$menu      = "REPORTS";
include "header.php";

$istable  = 1;
$emailsms = 1;

?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Industry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
<?php
if (isset($_POST["Import"])) {
    $filename = $_FILES["file"]["tmp_name"];
    $v        = $_FILES["file"]["name"];
    if ($_FILES["file"]["size"] > 0) {
        $v1 = substr(strrchr($v, '.'), 1);
        if ($v1 == "csv") {
            $file = fopen($filename, "r");
            $i    = 1;
            
            while (($emapData = fgetcsv($file, 99999, ",")) != FALSE) {
                if(!$emapData[11]) {
                    continue;
                }

                if ($i != 1) {
                    $industry_id = "";
                    $sql         = mysqli_query($con, "SELECT * from categories where cat_name like '%" . $emapData[11] . "%' ");
                    $rows        = mysqli_num_rows($sql);
                    if ($rows > 0) {
                        $row         = mysqli_fetch_array($sql);
                        $industry_id = $row['cat_id'];
                    } 

                        $reportId             = $emapData[0];
                        $title1               = addslashes($emapData[1]);
                        $pages                = addslashes($emapData[2]);
                        $no_taf               = addslashes($emapData[3]);
                        $singleuser_price     = addslashes($emapData[4]);
                        $enterpriseuser_price = addslashes($emapData[5]);
                        $published_date       = addslashes($emapData[6]);
                        $summary              = addslashes($emapData[7]);
                        $file_name            = $title1 . '.pdf';
                        $toc                  = addslashes($emapData[9]);
                        $taf                  = addslashes($emapData[10]);
                        $catlog               = addslashes($emapData[11]);
                        $publisher_code       = addslashes($emapData[12]);
                        $status               = "INSERTED";
                        
                        // Generate automatically 
                        //Meta-title
                        //$meta_title = $title;
                        //Meta-Description
                        $meta_description = $title1 . ' Published By :acquiremarketresearch.com';
                        //Meta-Keywords
                        $str              = stristr($title1, 'Market', true);
                        $meta_keywords    = ucwords($str . 'Market, Market research, market research reports, industry reports, Regions reports, industry analysis, business research');
                        //print_r($meta_keywords); die;
                        $sql = "INSERT INTO `reports`(`report_id`,`report_title`,`pages`,`no_taf`,`singleuser_price`,`enterpriseuser_price`,`published_date`,`summary`,`file_name`,`toc`,`taf`,`catlog`,`cat_id`,`publisher_code`,`meta_keywords`,`meta_desc`,`status`) 
                        VALUES (".$reportId.",'" . $title1 . "','" . $pages . "','" . $no_taf . "','" . $singleuser_price . "','" . $enterpriseuser_price . "','" . $published_date . "','" . $summary . "','" . $file_name . "', '" . $toc . "','" . $taf . "','" . $catlog . "'," . $industry_id . ",'" . $publisher_code . "','" . $meta_keywords . "','" . $meta_description . "' ,'INSERTED')";
                        
                        $result = mysqli_query($con, $sql);
                        echo mysqli_error($con);

                    if (!$result) {
                        echo "<script type=\"text/javascript\">
                                alert(\"File has contains some Duplicates Reports. Please Kindly check File.\");
                                window.location = \"fill_deleted_ids.php\"
                            </script>";
                    }
                }

                $i++;
                unset($emapData);
            }
        } else {
            echo "<script type=\"text/javascript\">alert(\"Invalid File / Extension! Please Upload CSV. Uploaded file's extension was : " . $v1 . ".\");
                     window.location = \"fill_deleted_ids.php\";
                   </script>";
        }
        fclose($file);
        
        echo "<script type=\"text/javascript\">
                        alert(\"CSV File has been successfully Imported.\");
                        window.location = \"fill_deleted_ids.php\"
                    </script>";
        
    }
}

?>         
            
            <form class="form-horizontal well" action="fill_deleted_ids.php" method="POST" name="upload_excel" enctype="multipart/form-data">
					<fieldset>
						<legend>Import CSV File</legend>
						<div class="control-group">
							<div class="control-label pull-left">
								<label>CSV File:</label>
							</div>
							<div class="controls">
								<input type="file" name="file" id="file" class="form-control" required>
                                <span id="msgblank" class="alert hide">Please select the file</span>
							</div>
						</div>
						
						<div class="control-group">
							<div class="controls"><br>
							<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading..." >Upload</button>
							</div>
						</div>
					</fieldset>
				</form>
				
            </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <!-- /.content -->
  </div>
<?php
include "footer.php";
?>