<!DOCTYPE html>
<?php
    $title="Acquire";
    $subtitle="Admin Login";
    include "menu.php";
    $istable=1;
?>
<html>
<!--
<script type="text/javascript" src="http://latex.codecogs.com/latexit.js"></script>
<script type="text/javascript">
LatexIT.add('p',true);
</script>
-->
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index2.html"><b>Admin</b>&nbsp;&nbsp; AMR</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

<!--
<p>Dividing $x^2+1$ by $y^2$ gives \[\frac{x^2+1}{y^2}\]</p>

<p>This equation <span lang="latex"> \frac{1+sin(x)}{x^3}  \begin{bmatrix} 1 & 1\\ 1 & 1\\ 1 & 1 \end{bmatrix}</span>
appears on the same line at the text.</p>\left ( 1 2 \right )
<img src="http://latex.codecogs.com/gif.latex?\begin{bmatrix} 1 & 1\\ 1 & 1\\ 1 & 1 \end{bmatrix}" border="0"/>--->
    <p class="login-box-msg">Sign in to start your session </p>

		<form  class="form-horizontal" id="loginvalid" method="POST"  role="form" data-bv-message="This value is not valid"
                      data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                      data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                      data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

      <div class="form-group has-feedback">

        <!--
        <input type="text" class="form-control" name="username"  id="username" placeholder="Username" required data-bv-notempty-message="The Usernam is required and cannot be empty"  data-bv-callback="true" data-bv-callback-callback="Checkuser" data-bv-callback-message="The Username Not Valid" onkeyup="myUsername()">-->

                  <input class="form-control" type="text"placeholder="Username" name="username"  id="username"  required data-bv-notempty-message="The Usernam is required and cannot be empty" data-bv-callback="true" data-bv-callback-callback="Checkusername" data-bv-callback-message="The Username Not Valid" onblur="myUsername()">

         <span id="user" class="glyphicon glyphicon-envelope form-control-feedback hide"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password"  class="form-control" placeholder="Password" required data-bv-notempty-message="The Password is required and cannot be empty" data-bv-callback="true" data-bv-callback-callback="CheckPass" data-bv-callback-message="The Password Not Valid" onblur="myPass()">
        <span id="pass" class="glyphicon glyphicon-lock form-control-feedback hide"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">

        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit" id="submit" onclick="return loginvalidate()">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
    <!-- /.social-auth-links -->
<!--
    <a href="#">I forgot my password</a><br>
    <a href="registration.php" class="text-center">Register a new membership</a>-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
<?php
    include "footer.php";
?>

  <script type="text/javascript">


function myUsername()
{
//    alert();
  if (!$('#user').hasClass("hide"))
  {
    $('#user').addClass("hide");
  };

    var username=$.trim(document.getElementById('username').value);
    if(username=="")
    {
        $('#user').addClass("hide");
        $('#username').removeClass("glyphicon-remove");
        $('#username').removeClass("glyphicon-ok");
    }
}
function myPass()
{
  if (!$('#pass').hasClass("hide"))
  {
    $('#pass').addClass("hide");
  };

    var username=$.trim(document.getElementById('password').value);
    if(username=="")
    {
        $('#pass').addClass("hide");
        $('#password').removeClass("glyphicon-remove");
        $('#password').removeClass("glyphicon-ok");
    }
}



$(document).ready(function(){
        $('#user').removeClass("hide");
        $('#pass').removeClass("hide");

    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
    function loginvalidate()
    { 

        var bv = $('#loginvalid').data('bootstrapValidator');
bv.destroy();

$('#loginvalid').data('bootstrapValidator', null);
$('#loginvalid').bootstrapValidator();
bv = $('#loginvalid').data('bootstrapValidator');


        bv.validate();
        if (!bv.isValid()) 
        {
            return;
        }

    var flag = 0;
    document.getElementById('submit').disabled = true;

    $.post('ajaxcall/ajax_login_check.php',
      {
            username:document.getElementById('username').value,
            password:document.getElementById('password').value
      },
      function(res)
      {
        var string = $.trim(res);
//        alert(string);
        if (string == "true")
        {
          alert(' Login Successful!');
          window.location='pages/index.php';
        }
        else if(string="DISABLED")
        {
            alert("	Error: Your Account is not Active please contact to Administrator");
        }
      });
           
              document.getElementById('submit').disabled = false;
              return false;

    }
    
  $(document).ready(function() {
    $('#loginvalid').bootstrapValidator();
    });

function Checkusername(value, validator) {

//            alert(value);
            var returnval=0;
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
                if (value === '') {
                        returnval=1;
                };
                if (returnval===0)
                {
                    $.ajaxSetup({async: false});
                    $.post('ajaxcall/ajax_check_user_login.php',
                    {
                        username: value,
                        password:password,username:username
                    },
                    function(res)
                    {
                            var string = $.trim(res);
                 
                            if (string === "true")
                            {   
                                returnval=1;
                            }
                            else 
                            {
                                
                               returnval=0;
                            }
                    });

                };
                
                if (returnval===1)
                {

                        return {
                            valid: true,    // or false
                            message: ''
                        }
                }
                else 
                {

                        return {
                            valid: false,    // or false
                            message: 'The Username is Not Valid'
                        }
                }
            
    };

function CheckPass(value, validator) {

//            alert(value);
            var returnval=0;
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
                if (value === '') {
                        returnval=1;
                };
                if (returnval===0)
                {
                    $.ajaxSetup({async: false});
                    $.post('ajaxcall/ajax_check_user_login.php',
                    {
                        username: value,
                        password:password,username:username
                    },
                    function(res)
                    {
                            var string = $.trim(res);
                 
                            if (string === "true")
                            {   
                                returnval=1;
                            }
                            else 
                            {
                                
                               returnval=0;
                            }
                    });

                };
                
                if (returnval===1)
                {

                        return {
                            valid: true,    // or false
                            message: ''
                        }
                }
                else 
                {

                        return {
                            valid: false,    // or false
                            message: 'The Password is Not Valid'
                        }
                }
            
    };


</script>
