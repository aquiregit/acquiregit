-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 30, 2017 at 10:11 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `id_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `delegate_master`
--

CREATE TABLE IF NOT EXISTS `delegate_master` (
  `del_id` int(11) NOT NULL AUTO_INCREMENT,
  `del_title` text NOT NULL,
  `del_fee` int(11) NOT NULL,
  PRIMARY KEY (`del_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `delegate_master`
--

INSERT INTO `delegate_master` (`del_id`, `del_title`, `del_fee`) VALUES
(1, 'Doctor', 3000),
(2, 'Advocate', 3000),
(3, 'Engineer', 3000),
(4, 'Professor', 3000),
(5, 'Officer', 3000),
(6, 'Businessmen', 3000),
(7, 'Traders', 3000),
(8, 'Bamcef employees', 2500),
(9, 'RMMS employees', 2500),
(10, 'RMMS Housewife', 2000),
(11, 'BMM', 2000),
(12, 'BVM', 1500),
(13, 'BYM', 1500),
(14, 'BBM', 1500);

-- --------------------------------------------------------

--
-- Table structure for table `imageupload`
--

CREATE TABLE IF NOT EXISTS `imageupload` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_id` int(11) DEFAULT NULL,
  `img_path` text,
  PRIMARY KEY (`img_id`),
  KEY `reg_id` (`reg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `prefix_master`
--

CREATE TABLE IF NOT EXISTS `prefix_master` (
  `pf_id` int(11) NOT NULL AUTO_INCREMENT,
  `pf_title` text NOT NULL,
  PRIMARY KEY (`pf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prefix_master`
--

INSERT INTO `prefix_master` (`pf_id`, `pf_title`) VALUES
(1, 'Mr'),
(2, 'Mrs'),
(3, 'Miss');

-- --------------------------------------------------------

--
-- Table structure for table `registration_form`
--

CREATE TABLE IF NOT EXISTS `registration_form` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `pf_id` int(11) NOT NULL,
  `del_id` int(11) NOT NULL,
  `reg_name` text NOT NULL,
  `reg_gender` text NOT NULL,
  `reg_yob` int(11) NOT NULL,
  `reg_age` text NOT NULL,
  `reg_add` text NOT NULL,
  `reg_pincode` text NOT NULL,
  `reg_taluka` text NOT NULL,
  `reg_dist` text NOT NULL,
  `reg_state` text NOT NULL,
  `reg_mobile` text NOT NULL,
  `reg_email` text NOT NULL,
  `reg_uuid` text NOT NULL,
  `u_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` text NOT NULL,
  `email` text,
  `password` text NOT NULL,
  `u_mobile` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `u_name`, `email`, `password`, `u_mobile`, `user_id`) VALUES
(1, 'Suraj', 'suraj@gmail.com', 'suraj@123', '9999999999', 1),
(2, 'Rohit Savant', 'rohit@gmail.com', 'rohit@123', '7777777777', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE IF NOT EXISTS `user_master` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_title` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`user_id`, `user_title`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `imageupload`
--
ALTER TABLE `imageupload`
  ADD CONSTRAINT `imageupload_ibfk_1` FOREIGN KEY (`reg_id`) REFERENCES `registration_form` (`reg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
