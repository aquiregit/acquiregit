<?php session_start(); ?>
<html>
<head>
<title> Non-Seamless-kit</title>
</head>
<body>
<center>
<?php 

	error_reporting(E_ALL);
	require('Crypto.php');
	require("credential.php");
	require('../connect.php');

	$working_key=WORKING_KEY;//Shared by CCAVENUES
	$access_code=ACCESS_CODE;//Shared by CCAVENUES

	//get product details from db and send it to PG
	$prodId = $_SESSION['prod_id'];
	$sql 	= "SELECT * FROM `reports` WHERE `report_id` = ".$prodId;
	$result	= mysqli_query($con,$sql);
	$product= mysqli_fetch_assoc($result);
	if (!$product) {
		echo "Error: Product not found.";
		exit;
	}

	$fname = mysqli_real_escape_string($con, $_POST['billing_name']);
	$email = mysqli_real_escape_string($con, $_POST['billing_email']);
	$cname = mysqli_real_escape_string($con, $_POST['cname']);
	$desig = mysqli_real_escape_string($con, $_POST['desig']);
	$ccode = mysqli_real_escape_string($con, $_POST['country_code']);
	$telno = mysqli_real_escape_string($con, $_POST['billing_tel']);
	$country = mysqli_real_escape_string($con, "India");
	$prod_id = mysqli_real_escape_string($con, $_POST['r_id']);
	$prod_des = mysqli_real_escape_string($con, $_POST['prod']);
	// $prod_pri = mysqli_real_escape_string($con, $_POST['prod_p']);
	$paym_opt = mysqli_real_escape_string($con, "Credit Card");

	$sql = "INSERT INTO `customer`(`customer_id`, `cust_fullname`, `cust_email`, `cust_company`, `cust_desig`, `cust_ccode`, `cust_phone`, `cust_country`, `cust_report_id`, `cust_report`, `cust_price`, `payment_option`, `payment_status`) VALUES(null, '".$fname."', '".$email."', '".$cname."', '".$desig."', '".$ccode."', ".$telno.", '".$country."', '".$prod_id."', '".$prod_des."', '".$product['prod_price']."', '".$paym_opt."', 'Initiated');";

	if(!mysqli_query($con,$sql)) {
		echo "Error: Something went wrong..Please try again." . mysqli_error($con);
		exit;
	}
	
	$cust_id = mysqli_insert_id($con);

    if($_POST['price_type'] == "single") {
        $price = $product['singleuser_price'];
    } else if($_POST['price_type'] == "multiple") {
        $price = $product['multiuser_price'];
    } else {
        $price = $product['enterpriseuser_price'];
    }
    
	$requiredFields = [
		"merchant_id" 	=> MERCHANT_ID,
		"currency" 		=> "USD",
		"language" 		=> "EN",
		"redirect_url" 	=> "https://www.acquiremarketresearch.com/thankyou.php",
		"cancel_url" 	=> "https://www.acquiremarketresearch.com/thankyou.php",
		"order_id" 		=> $_POST['r_id'],
		"amount" 		=> $price,
		"merchant_param1" => $cust_id
	];

	$requiredData = "";
	foreach ($requiredFields as $key => $value){
		$requiredData.=$key.'='.$value.'&';
	}

	unset($_POST['payment_option']);

	$merchant_data='';
		
	foreach ($_POST as $key => $value){
		$merchant_data.=$key.'='.$value.'&';
	}
	$merchant_data.=$requiredData;
//print_r($merchant_data); exit;
	$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.

?>
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 
<?php
echo "<input type=hidden name=encRequest value=$encrypted_data>";
echo "<input type=hidden name=access_code value=$access_code>";
?>
</form>
</center>
<script language='javascript'>document.redirect.submit();</script>
</body>
</html>

