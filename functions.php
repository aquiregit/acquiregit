<?php
    define('SHORT_URL_DATE', '2019-03-16');
    
    function limit_slug($string) {
       return trim(strstr( str_replace( 'global', '', strtolower( $string ) ), "market", true) . "market");
    }
    
    function checkSlugDate($time) {
        if( empty($time) || $time == '0000-00-00 00:00:00' ) {
            return 0;
        }

        $reportDate = date('Y-m-d', strtotime($time));
        if(strtotime($reportDate) >= strtotime(SHORT_URL_DATE)) {
            return 1; 
        } else {
            return 0;
        }
    }
    
    function prepareSlugUrl($time, $report_title) {
        if(checkSlugDate( $time ) ) {
            $slugUrl = limit_slug($report_title);
        } else {
            $slugUrl = limit_words(strtolower($report_title),8);
        }
        return utf8_encode(str_replace(' ','-',$slugUrl));
    }
?>