<?php 
    $pagetitle="Contact Us | Acquire Market Research";
    $desc="Contact Us | Acquire Market Research";
    $key="Contact Us, information, Contact, Market Research, Market Analysis";
    include 'header_file.php';?>
<!--start of container div-->
<style type="text/css">
    #contact_form 
    {
    font-size: 15px;
    }
 #request_form input[type=submit]
    {
        background-color: gray;
    }
    #request_form input[type=submit]:hover
    {
        background-color: gray;
    }
   #request_form #msg
   {
    color: gray;
   }
   #request_form span.isvalid:after
{
  padding-left: 10px;
  content: '✓';
  color: green;
  font-weight: bold;
}
#request_form span.isvalid input[type="text"]
{
    border-color: green;
    border-width: 2px;
}
#request_form span.isvalid input[type="email"]
{
    border-color: green;
    border-width: 2px;
}
 
  @media screen and (min-width: 760px) and (max-width: 980px){
 
            #phnum{width:157%; margin-top:5px;}
             
             #ccode{width:95%;}
}

@media screen and (min-width: 300px) and (max-width: 760px)
            {
             #phnum{width:117%; margin-top:5px;}
             
             #ccode{width:72%;}
             
            }
 
 .col-md-3 label:after {content:" * ";color:red;font-size:19px;}
    .input-padding{padding:4px;}
    #country_code{width:120%;}
    input:focus::-webkit-input-placeholder { color:transparent; }
    input:focus:-moz-placeholder { color:transparent; } /* FF 4-18 */
    input:focus::-moz-placeholder { color:transparent; } /* FF 19+ */
    input:focus:-ms-input-placeholder { color:transparent; } /* IE 10+ */
</style>
<div class="container" id="contact_form">
    <div class="col-md-8">
        <!--For form-->
        <br>
        <!--<p class="center">
            <span style="color:#0077b5;font-size:30px;font-weight:bold;">Contact</span>&nbsp;&nbsp;
            <span style="color:#87cefa;font-size:30px; font-weight:bold;">Us</span> <span style="font-size:30px; font-weight:bold;"> – Thank you for your interest</span><br>		
        </p>-->
        <br>	
      
           <form id="request_form" action="https://www.acquiremarketresearch.com/thank-you/" method="POST">
            <input type='hidden' value='contact-us' name='action'>
            <div class="row">
                <div class="col-md-3">
                    <label for="fullname">Full Name</label>
                </div>
                <div class="col-md-9 input-padding">
                    <span class="notvalid"><input type="text" id="fullname" name="fullname" placeholder="" onkeypress="return IsAlpha(event);" ondrop="return false;" onpaste="return false;" style="background-color: #f0f0f0;width:70%" required></span><span id="error_name" style="color: Red; display: none">* Input digits (0 - 9)</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label for="lname">Business Email</label>
                </div>
                <div class="col-md-9 input-padding">
                    <span class="notvalid"><input type="email" id="bemail12" name="bemail12" placeholder="Please enter company email id" style="background-color: #f0f0f0;width:70%" required></span>
                </div>
            </div>
              
            <div class="row">
                <div class="col-md-3">
                    <label for="cname">Company Name</label>
                </div>
                <div class="col-md-9 input-padding">
                    <span class="notvalid"><input type="text" id="cname" name="cname" placeholder="" style="background-color: #f0f0f0;width:70%" required></span>
                </div>
            </div>
            
            
            
             <div class="row">
                <div class="col-md-3">
                    <label for="desig">Designation</label>
                </div>
                <div class="col-md-9 input-padding">
                    <span class="notvalid"><input type="text" id="desig" name="desig" placeholder="" style="background-color: #f0f0f0;width:70%" required></span>
                </div>
            </div>
            
            
            
            
            
            
            
             <div class="row">
                <div class="col-md-3">
                    <label for="ph">Phone Number</label>
                </div>
                    <div class="col-sm-9">
                    <div class="row">
                        <div class="col-md-3 col-sm-12" id="ccode" style="margin:0px;">
                            <?php include'countrycode.php';?>
                        </div>
                        <div id="phnum" class="col-md-9 col-sm-12" style="margin-left:0px;">
                            <span class="notvalid"><input type="text" id="ph" name="ph" placeholder="" style="background-color: #f0f0f0;width:59%;" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" maxlength="15" required></span><span id="error" style="color: Red; display: none">* Input digits (0 - 9)</span>
                        </div>
                    </div></div>
                
            </div> 
        
            <div class="row">
                <div class="col-md-3">
                    <p for="subject" style="font-weight:bold;">Message</p>
                </div>
                <div class="col-md-9 input-padding">
                    <textarea id="subject" name="subject" placeholder="Type here..." style="height:80px;background-color: #f0f0f0;width:70%"></textarea>
                </div>
            </div>

      

            <div class="row">
                <div class="col-md-3">
                    <label for="captcha">Captcha</label>
                    
                </div>
                
              
<div class="col-md-6">
<?php
echo "<br><div class='col-md-3' style='padding:0px;'><p style='font-size:1.5em;padding:2px 0px 10px 15px;letter-spacing:5px;border: 1px solid #0077b5;height:30px;width:80px;background-color:#e0e0e0;'><b> ".$num1= rand(100,1000);
echo "</b></p></div>";
$c=$num1;

echo '<style>.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}</style><div class="col-md-8"><span class="notvalid"><input type="text" style="width:70%" placeholder="Enter Captcha" id="BotBootInputDr" name="res" oninput="ValidBotBootDr();" onkeypress="return IsNumericcap(event);" ondrop="return false;" onpaste="return false;" maxlength="3" size="3" required/></span><span id="error_nocap" style="color: Red; display: none;">Invalid Input</span><p id="error_in" style="color: Red;" class="blink"></p></div>';?>

</div>
               

            </div>
             <div class="col-md-6" > 
            <div class="row">
                <input type="submit" value="Submit" id="submit" name="submit" disabled>
        <p id="msg">(please do not close the browser till the thank you page appears)
        </p>
            </div>
        </div>
        </form>
    </div>
    <!--End of form div-->
    <div class="container col-md-4" style="padding-left: 50px;">
        <style>
            .w3-card,.w3-card-2{
            box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
            }
            .w3-card-4,.w3-hover-shadow:hover{
            box-shadow:0 4px 10px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19);
            }
            .w3-container{
            padding:0.01em 16px;
            }
            #all_report_card
            {
            width:90%;
            background-color:#e0e0e0;
            margin-top: 90px;
            }
            #all_report_card h3
            {
            border-bottom: 1px solid gray; padding-bottom:10px;
            }
            #all_report_card a
            {
            color:#0B3C5D;
            font-size:1em;
            }
            #all_report_card img
            {
            height:120px;
            }
            /* Desktop */
            @media screen and (min-width: 980px) and (max-width: 1024px)
            {
            #all_report_card
            {
            margin-left: 50px;
            }
            }
            /* Tablet */
            @media screen and (min-width: 760px) and (max-width: 980px)
            {
            #all_report_card
            {
            margin-left: 50px;
            }
            }
            /* Mobile HD*/ 
            @media screen and (min-width: 350px) and (max-width: 760px)
            {
            #all_report_card 
            {
            margin-top: 10px;
            width: 100%;
            }
            }
            /* Mobile LD */
            @media screen and (max-width: 350px)
            {
            #all_report_card 
            {
            margin-top: 10px;
            width: 100%;
            }
            }
        </style>
        <!--div class="w3-card-4" style="" id="all_report_card">
            <header class="w3-container w3"  >
                <h3 style="">
                    <p class="center"><b>Happy To Assist You 24/7</b></p>
                </h3>
            </header>
            <div class="w3-container center">
                
                    <img src="<?php echo $base_url;?>images/success-support.png" title="Happy to assist you" alt="Contact us" style="" class="img-responsive center"/>
                    <b>
                        <i class="fas fa-envelope" style="color:#0B3C5D;"></i>
                        <script TYPE="text/javascript">
                            emailE=('sales@' + 'acquiremarketresearch.com')
                            document.write('<a href="mailto:' + emailE + '">' + emailE + '</a>')

                        </script>
                        <div><br></div>
                        <i class="glyphicon glyphicon-phone-alt" style="color:#0B3C5D"></i><a href="skype:+1-800-663-5579 call" style="">&nbsp;+1-800-663-5579</a>
                    </b>
               
            </div>
        </div-->
        <div class="w3-card-4" style="margin-top: 40px;background-color: #FFF" id="all_report_card">
            <header class="w3-container w3">
                <h3 style="">
                    <p class="center"><b>Global Headquarters</b></p>
                </h3>
            </header>
            <div class="w3-container">
                <b>
                    <p style="text-align: center;font-size: 1.2em;color: #0077b5">
                    <span class="glyphicon glyphicon-map-marker" style="color: #E6751E"></span>
                    <u style="color:#0077b5;text-decoration:none;"> USA </u></p>
                    <p>555 Madison Avenue, 5th Floor, Manhattan, New York, 10022 USA</p>
                    
                    
                                        
                    <div><br></div>
                </b>
            </div>
        </div>
         <div class="w3-card-4" style="margin-top: 22px;background-color: #FFF" id="all_report_card">
            <header class="w3-container w3">
                <h3 style="">
                    <p class="center"><b>Asia Pacific</b></p>
                </h3>
            </header>
            <div class="w3-container">
                <b>
                    <p style="text-align: center;font-size: 1.2em;color: #0077b5">
                    <span class="glyphicon glyphicon-map-marker" style="color: #E6751E"></span>
                    <u style="color:#0077b5;text-decoration:none;"> INDIA </u></p>
                    <p>Cerebrum IT Park, Office No.4A, 2nd Floor, B3 Building No. 3, Kalyani Nagar, Pune, Maharashtra 411014</p>
                    
                    
                                        
                    <div><br></div>
                </b>
            </div>
        </div>
        <br><br>
    </div>
</div>
<!--End of Container div-->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<script language='JavaScript' type='text/javascript'>
    function refreshCaptcha()
    {
        var img = document.images['captchaimg'];
        img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
    }


$("form").submit(function(event) {

   var recaptcha = $("#g-recaptcha-response").val();
   if (recaptcha === "") {
      event.preventDefault();
      alert("Please check the recaptcha");
   }
});

    var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            function IsNumeric(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error").style.display = ret ? "none" : "inline";
                return ret;
            }

             function IsNumericcap(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_nocap").style.display = ret ? "none" : "inline";
                
                return ret;
            }
        
            function IsAlpha(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123) || keyCode==32 || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_name").style.display = ret ? "none" : "inline";
                return ret;
            }
    
</script>

       <script type="text/javascript">
 
 $(document).ready(function() {
    document.getElementById('submit').style.cursor='not-allowed';
$('form[id="request_form"]').validate({
  rules: {
    fullname: {
    required: true,
    minlength:2, 
},
    cname:{
    required: true,
    minlength:3,},
   
   desig:{
    required: true,
    minlength:2,},

    ph:{
    required: true,
    minlength:7,},
    dvFlag: 'required',
    CountryList: 'required',
    BotBootInputEb: 'required',
    bemail12: {
      required: true,
      minlength: 8,
      email: true,
    },
    BotBootInputDr: {
      required: true,
      minlength: 3,
    }
  },
  messages: {
    fullname: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*** This field is required ***',
    cname: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*** This field is required ***',
    bemail12: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*** Enter a valid email ***',
     
      ph: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This field is required',
      desig: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*** This field is required ***',
      
      BotBootInputDr: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*** This field is required ***',
    BotBootInputDr: {
      minlength: 'Please enter valid captcha.'
    }
  },
  submitHandler: function(form) {
    form.submit();
    document.getElementById('submit').style.cursor='pointer';
  }
});

});
 </script>

   <script type="text/javascript">
        var valid_count=0; var countf=0; var countb=0; var countc=0; var countd=0; var countp=0; var countcap=0;
        
        function add()
        {
          valid_count=countf+countb+countc+countd+countp+countcap;
        }

     $('[name=fullname]').keyup(function(){

    if($('[name=fullname]').valid()){
         $(this).parent().removeClass().addClass('isvalid');
         document.getElementById('fullname').style.borderColor="green";
           document.getElementById('fullname').style.borderWidth="2px";
         countf=1;
         add();
         if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{

        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
    }
     }else{
      countf=0;
      document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
             $(this).parent().removeClass().addClass('notvalid');
        document.getElementById('fullname').style.borderColor="#ff0000";
           document.getElementById('fullname').style.borderWidth="4px";
           
     }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
})
     $('[name=bemail12]').keyup(function(){

    if($('[name=bemail12]').valid()){
        document.getElementById('bemail12').style.borderColor="green";
           document.getElementById('bemail12').style.borderWidth="2px";
         $(this).parent().removeClass().addClass('isvalid');
         countb=1;
         add();
         if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{

        document.getElementById('submit').disabled=true;
        
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
    }
     }else{
      countb=0;
      document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';

             $(this).parent().removeClass().addClass('notvalid');
           document.getElementById('bemail12').style.borderColor="#ff0000";
           document.getElementById('bemail12').style.borderWidth="4px";
         
     }

})

    $('[name=cname]').keyup(function(){

    if($('[name=cname]').valid()){
         $(this).parent().removeClass().addClass('isvalid');
         document.getElementById('cname').style.borderColor="green";
           document.getElementById('cname').style.borderWidth="2px";
       countc=1;
       add();
        if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{

        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
    }
     }else{
      countc=0;
      document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
             $(this).parent().removeClass().addClass('notvalid');
        document.getElementById('cname').style.borderColor="#ff0000";
           document.getElementById('cname').style.borderWidth="4px";
           
     }

})
    $('[name=desig]').keyup(function(){

    if($('[name=desig]').valid()){
         $(this).parent().removeClass().addClass('isvalid');
         document.getElementById('desig').style.borderColor="green";
           document.getElementById('desig').style.borderWidth="2px";
         countd=1;
         add();
         if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{
        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
       
    }
     }else{
            countd=0;
             $(this).parent().removeClass().addClass('notvalid');
        document.getElementById('desig').style.borderColor="#ff0000";
           document.getElementById('desig').style.borderWidth="4px";
           document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
     }

})

    $('[name=ph]').keyup(function(){

    if($('[name=ph]').valid()){
         $(this).parent().removeClass().addClass('isvalid');
         document.getElementById('ph').style.borderColor="green";
           document.getElementById('ph').style.borderWidth="2px";
         countp=1;
         add();
         if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{

        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
    }
     }else{
      countp=0;
      document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
             $(this).parent().removeClass().addClass('notvalid');
        document.getElementById('ph').style.borderColor="#ff0000";
           document.getElementById('ph').style.borderWidth="4px";
           
     }

})
    
function ValidBotBootDr(){
        var d = document.getElementById('BotBootInputDr').value;
        var res="<?php echo $c;?>";
        
        if (d == res){
            $('#BotBootInputDr').parent().removeClass().addClass('isvalid');
            countcap=1;

            add();
             if(valid_count >= 5)
    {

        document.getElementById('submit').disabled=false;
     document.getElementById('submit').style.cursor='pointer';
    document.getElementById('submit').style.backgroundColor='#0077b5';
    }   
else{

        document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
    }

            document.getElementById('BotBootInputDr').style.borderColor="#4CAF50";
            document.getElementById('BotBootInputDr').style.borderWidth="3px";
            document.getElementById("error_in").innerHTML="";
            return true;
           
        }        
        
        else
          { 
          countcap=0;  
            document.getElementById('submit').disabled=true;
       document.getElementById('submit').style.cursor='not-allowed';
       document.getElementById('submit').style.backgroundColor='gray';
          $('#BotBootInputDr').parent().removeClass().addClass('notvalid');
       document.getElementById('BotBootInputDr').style.borderColor="#ff0000";
       document.getElementById('BotBootInputDr').style.borderWidth="3px";
       document.getElementById("error_in").innerHTML="Please enter correct captcha.";
          
                   return false;
               }

    }
 </script>
    
<?php include 'footer_file.php';?>