<?php
$pagetitle = "Advanced Search | Acquire Market Research";
$key       = "Advanced Search | Acquire Market Research, AMR";
$desc      = "Advanced Search offers option to funnel down search results for more specific or complex searches. Discover Market Research Reports that offer comprehensive analysis to cater to specific research needs you may have for different verticals across the globe at Acquire Market Research.";
include "header_file.php";
?>
<div class="container">
    <div class="row"></div>
    <br><br>
</div>
<style type="text/css">input:focus::-webkit-input-placeholder{color:transparent}input:focus:-moz-placeholder{color:transparent}input:focus::-moz-placeholder{color:transparent}input:focus:-ms-input-placeholder{color:transparent}#adv thead th{padding-bottom:10px}#adv .well{padding:10px 0 10px 10px;font-size:1.3em}.well table{width:75%;margin:0 auto 2.5em}#filter_col2 td,#filter_col3 td,#filter_col5 td,#filter_col6 td{padding:2px}#filter_col4 #price{padding:2px 2px 2px 190px}#filter_col4 #price input{width:74px}#filter_global input{width:20%}#error_p,#error_pubc,#error_rd{color:Red;display:none}#col_filter{margin-left:3px}
</style>
<div class="container" id="adv">
<div class="well well-lg">    
<table  cellpadding="3" cellspacing="0" border="0">
        <thead>
            <tr>
                <th>What you can Search for : </th>
            </tr>
        </thead>
        <tbody>
           <form action="<?php echo $base_url;?>advancedresult/" method="POST">
           <tr id="filter_col2" data-column="1">
                <td> Report ID : </td>
                <td align="center"><input type="text" name="ri" class="column_filter" id="col_filter" onkeypress="return IsNumeric(event);"> <!--ondrop="return false;" onpaste="return false;"-->
<br><span id="error_rd">* Input digits (0 - 9)</span></td>
            </tr>
            <tr id="filter_col2" data-column="1">
                <td> Report Title (Any Keyword): </td>
                <td align="center"><input type="text" name="rt" placeholder="(keyword)" class="column_filter" id="col1_filter"></td>
            </tr>
            <tr id="filter_col3" data-column="2">
                <td>Industry : </td>
                <td align="center"><input type="text" class="column_filter" placeholder="(category)" name="it" id="col2_filter"></td>
            </tr>
            <tr id="filter_col5" data-column="4">
                <td>Published Date : </td>
                <td align="center"><input type="text" class="column_filter" name="pd" placeholder="yyyy-mm-dd" id="col4_filter"></td>
            </tr>
            <tr id="filter_col6" data-column="5">
                <td> Publisher Code : </td>
                <td align="center"><input type="text" class="column_filter" name="pc" id="col5_filter" onkeypress="return IsNumericpc(event);"><br><span id="error_pubc">* Input digits (0 - 9)</span></td>
            </tr>
            <tr id="filter_col4" data-column="3">
            <td> Single User Price : </td>
            <td id="price"> $ <input type="text" class="column_filter" placeholder="(min)" id="min" name="min" onkeypress="return IsNumericp(event);"> - <input type="text" class="column_filter" placeholder="(max)" id="max" name="max" onkeypress="return IsNumericp(event);"><br><span id="error_p">* Input digits (0 - 9)</span></td>
        </tr>
        <tr id="filter_global">
            <td></td>
                <td align="center" ><br><input type="submit" class="btn btn-info" placeholder="(keywords)" value="Submit"></td>
        </tr>
        </form>
        </tbody>
    </table>
</div>
<br><br>
</div>
<script language='JavaScript' type='text/javascript'>
   var specialKeys = new Array();
            specialKeys.push(8);
            function IsNumericpc(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) ||(keyCode==13) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_pubc").style.display = ret ? "none" : "inline";
                return ret;
            }
            function IsNumericp(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode==13) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_p").style.display = ret ? "none" : "inline";
                return ret;
            }
            function IsNumeric(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57)|| (keyCode==13) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_rd").style.display = ret ? "none" : "inline";
                return ret;
            }
            function IsAlpha(e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode > 64 && keyCode < 91)|| (keyCode==32) || (keyCode > 96 && keyCode < 123) ||specialKeys.indexOf(keyCode) != -1);
                document.getElementById("error_name").style.display = ret ? "none" : "inline";
                return ret;
            }
</script>
<?php
function limit_words($string, $word_limit)
{
    $words = str_replace(",", " ", $string);
    $words = explode(" ", $words);
    
    return implode(" ", array_splice($words, 0, $word_limit));
}

?>
<?php
include "footer_file.php";
?>