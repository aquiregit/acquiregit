<?php
 ini_set('max_execution_time', 900);
    include 'connect.php';
$base_url = "https://www.acquiremarketresearch.com/";

$xml=new DOMDocument("1.0","utf-8");
        $xml->formatOutput=true;
            /*urlset*/
          $urlset=$xml->createElement("urlset");
          $xml->appendChild($urlset);
          $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
            /*index.php*/
          $url=$xml->createElement("url");
          $urlset->appendChild($url);
          $loc=$xml->createElement("loc",$base_url);
          $url->appendChild($loc);
          $prt=$xml->createElement("priority","1.0");
          $url->appendChild($prt);
		  
  $qucm="SELECT * FROM reports WHERE catlog='Chemical & Material' AND report_id<=72830";
 
      $resultcm=$con->query($qucm);
	   
      $ticm="";
      if($resultcm->num_rows > 0)
      {
		  while($rowcm = $resultcm->fetch_assoc()) 
         {
			
$urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowcm['report_title']),8)));
             $urltitle= str_replace("--","-",$urltitle);
           $urltitle= str_replace("---","-",$urltitle);
             $urltitle= str_replace("(", "-",$urltitle);
              $urltitle= str_replace(")", "-",$urltitle);
              $urltitle= str_replace("?", "-",$urltitle);
                                    $urltitle= str_replace("!", "-",$urltitle);
                                    $urltitle= str_replace("@", "-",$urltitle);
                                    $urltitle= str_replace("#", "-",$urltitle);
                                    $urltitle= str_replace("$", "-",$urltitle);
                                    $urltitle= str_replace("%", "-",$urltitle);
                                    $urltitle= str_replace("^", "-",$urltitle);
                                    $urltitle= str_replace("*", "-",$urltitle);
                                    $urltitle= str_replace("'", "-",$urltitle);
                                    $urltitle= str_replace("+", "-",$urltitle);
                                    $urltitle= str_replace("&","and",$urltitle);
                                    $urltitle=str_replace("/","-",$urltitle);
                                    $urltitle=str_replace(":","-",$urltitle);
                                    $urltitle=str_replace(".","-",$urltitle);
                                    $urltitle=str_replace("<","-",$urltitle);
                                    $urltitle=str_replace(">","-",$urltitle);
                                    $urltitle=str_replace("'","-",$urltitle);
                                    $urltitle=str_replace("|","-",$urltitle);
                                    $urltitle=str_replace("]","-",$urltitle);
                                    $urltitle=str_replace("[","-",$urltitle);
                                    $urltitle=str_replace("}","-",$urltitle);
                                    $urltitle=str_replace("{","-",$urltitle);
                                    $urltitle=str_replace(";","-",$urltitle);
                                    $urltitle=str_replace("_","-",$urltitle);
                                    $urltitle=str_replace("_x000D_","-",$urltitle);
                                    $urltitle= str_replace("--","-",$urltitle);
                                    $urltitle1= str_replace("---","-",$urltitle);
                                     $reportcm=$xml->createElement("url");
                                      $urlset->appendChild($reportcm);
                                     $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowcm['report_id']);
                                     $reportcm->appendChild($loc);
                     
                                     $prt=$xml->createElement("priority","0.9");
                                     $reportcm->appendChild($prt);
                                     $ticm=str_replace("&","and", $rowcm['catlog']);
                                      $ticm=str_replace(" ","-", $ticm);
                                      $ticm=strtolower($ticm);
                                     
         }
 $xml->save($ticm."-market-reports.xml");
      } 
      function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
}     
?>