<?php 
/*error_reporting(E_ALL);
ini_set("display_errors", "1");*/

header( "Content-type: text/xml");
echo'<?xml version="1.0" encoding="utf-8" ?>' . "\n"; 
$base_url = "https://www.acquiremarketresearch.com/";
$feed_name = "acquiremarketresearch.com/";
$feed_url = "https://www.acquiremarketresearch.com/feed";
$page_description = "Leading provider of market research news, market research reports and industry analysis by products, markets, Regions, industries around the global.";
$page_language = "en-US";
$creator_email = "sales@acquiremarketresearch.com";
include 'connect.php';

/*Get press releases*/
$sql    = "SELECT * FROM press_releases";
$pressReleases = mysqli_query($con, $sql);

/*Get blogs*/
$sql    = "SELECT * FROM blog";
$blogs  = mysqli_query($con, $sql);

/*Get reports*/
$sql    = "SELECT report_id, report_title, published_date, summary, time FROM reports ORDER BY report_id DESC LIMIT 1000"; // limit 43, 1
$reports = mysqli_query($con, $sql);

function limit_words($string, $word_limit) {
    $words=str_replace(","," ",$string);  
    $words = explode(" ",$words);

    return implode(" ", array_splice($words, 0, $word_limit));
}
function getPRUrl($url, $id, $base_url, $pr = true) {
        $prtitle     = utf8_encode((str_replace(" ", "-", strtolower($url))));
        $prtitle1    = str_replace("&", "and", $prtitle);
        $prtitle1    = str_replace("(", "-", $prtitle1);
        $prtitle1    = str_replace(")", "-", $prtitle1);
        $prtitle1    = str_replace("?", "-", $prtitle1);
        $prtitle1    = str_replace("!", "-", $prtitle1);
        $prtitle1    = str_replace("@", "-", $prtitle1);
        $prtitle1    = str_replace("#", "-", $prtitle1);
        $prtitle1    = str_replace("$", "-", $prtitle1);
        $prtitle1    = str_replace("%", "-", $prtitle1);
        $prtitle1    = str_replace("^", "-", $prtitle1);
        $prtitle1    = str_replace("*", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("+", "-", $prtitle1);
        $prtitle1    = str_replace("/", "-", $prtitle1);
        $prtitle1    = str_replace(":", "-", $prtitle1);
        $prtitle1    = str_replace(".", "-", $prtitle1);
        $prtitle1    = str_replace("<", "-", $prtitle1);
        $prtitle1    = str_replace(">", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("|", "-", $prtitle1);
        $prtitle1    = str_replace("]", "-", $prtitle1);
        $prtitle1    = str_replace("[", "-", $prtitle1);
        $prtitle1    = str_replace("}", "-", $prtitle1);
        $prtitle1    = str_replace("{", "-", $prtitle1);
        $prtitle1    = str_replace(";", "-", $prtitle1);
        $prtitle1    = str_replace("_", "-", $prtitle1);
        $prtitle1    = str_replace("_x000D_", "-", $prtitle1);
        $prtitle1    = str_replace("---", "-", $prtitle1);
        $prtitle1    = str_replace("--", "-", $prtitle1);
        
        $finalStrprv = ( ( $pr == true) ? "press-release/" : "blog/" );
        return $base_url . $finalStrprv . $prtitle1 . '/' . $id . '/';
}
function getReportUrl($time, $url, $id, $base_url) {
    include_once'functions.php';
    
    $finalStr="industry-reports/";
    $urltitle = prepareSlugUrl($time, $url);
    $urltitle= str_replace(" ", "-",$urltitle);
    $urltitle= str_replace("(", "-",$urltitle);
    $urltitle= str_replace(")", "-",$urltitle);
    $urltitle= str_replace("?", "-",$urltitle);
    $urltitle= str_replace("!", "-",$urltitle);
    $urltitle= str_replace("@", "-",$urltitle);
    $urltitle= str_replace("#", "-",$urltitle);
    $urltitle= str_replace("$", "-",$urltitle);
    $urltitle= str_replace("%", "-",$urltitle);
    $urltitle= str_replace("^", "-",$urltitle);
    $urltitle= str_replace("*", "-",$urltitle);
    $urltitle= str_replace("'", "-",$urltitle);
    $urltitle= str_replace("+", "-",$urltitle);
    $urltitle= str_replace("&","and",$urltitle);
    $urltitle=str_replace("/","-",$urltitle);
    $urltitle=str_replace(":","-",$urltitle);
    $urltitle=str_replace(".","-",$urltitle);
    $urltitle=str_replace("<","-",$urltitle);
    $urltitle=str_replace(">","-",$urltitle);
    $urltitle=str_replace("'","-",$urltitle);
    $urltitle=str_replace("|","-",$urltitle);
    $urltitle=str_replace("]","-",$urltitle);
    $urltitle=str_replace("[","-",$urltitle);
    $urltitle=str_replace("}","-",$urltitle);
    $urltitle=str_replace("{","-",$urltitle);
    $urltitle=str_replace(";","-",$urltitle);
    $urltitle=str_replace("_","-",$urltitle);
    $urltitle=str_replace("_x000D_","-",$urltitle);
    
    $urltitle=str_replace("--","-",$urltitle);
    $urltitle1=str_replace("---","-",$urltitle);

    return $base_url . $finalStr . $urltitle1 . '/' . $id . '/';
}

function convert_ascii($string) 
{ 
  $string = utf8_encode($string);
  $string = str_replace('?', '', $string);
  $string = str_replace("&", "and", $string);
  $string = str_replace("<", "", $string);
  $string = str_replace(">", "", $string);
  // Remove any non-ASCII Characters
  $string = preg_replace("/[^\x01-\x7F]/","", $string);

  return $string;
}
?>

<rss version="2.0"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:admin="http://webns.net/mvcb/"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:content="http://purl.org/rss/1.0/modules/content/">  

    <channel>

        <title><?php echo $feed_name; ?> </title> 
        <link><?php echo $feed_url; ?> </link> 
        <description><?php echo $page_description; ?></description>  
        <dc:language><?php echo $page_language; ?></dc:language>  
        <dc:creator><?php echo $creator_email; ?></dc:creator>
        <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>  

        <admin:generatorAgent rdf:resource="https://www.acquiremarketresearch.com/">
        <!-- Press Releases Start -->
        <?php while($pressRelease = $pressReleases->fetch_assoc()): ?>  
            <?php $prUrl = getPRUrl($pressRelease['pr_title'], $pressRelease['pr_id'], $base_url); ?>
            <item>  
                <title><?php echo $pressRelease['pr_title']; ?></title> 
                <link><?php echo $prUrl; ?></link>
                <guid><?php echo $prUrl; ?></guid>
                <description><![CDATA[<?php echo substr($pressRelease['meta_desc'], 0, 200); ?>]]></description>
                <pubDate><?php echo  $pressRelease['published_date'];?></pubDate>
            </item>  
        <?php endwhile; ?>
        <!-- Press Releases End -->
        
        <!-- Blogs Start -->
        <?php while($blog = $blogs->fetch_assoc()): ?>  
            <?php $blogUrl = getPRUrl($blog['blog_title'], $blog['blog_id'], $base_url, false); ?>
            <item>  
                <title><?php echo $blog['blog_title']; ?></title> 
                <link><?php echo $blogUrl; ?></link>
                <guid><?php echo $blogUrl; ?></guid>
                <description><![CDATA[<?php echo substr($blog['blog_desc'], 0, 200); ?>]]></description>
                <pubDate><?php echo  $blog['published_date'];?></pubDate>
            </item>  
        <?php endwhile; ?>
        <!-- Blogs End -->
        
        <!-- Reports Start -->
        <?php while($report = $reports->fetch_assoc()): ?>  
            <?php $reportUrl = getReportUrl($report['time'], $report['report_title'], $report['report_id'], $base_url); ?>
            <item>  
                <title><?php echo convert_ascii(substr($report['report_title'], 0, 100)); ?></title> 
                <link><?php echo $reportUrl; ?></link>
                <guid><?php echo $reportUrl; ?></guid>
                <description><![CDATA[<?php echo convert_ascii( substr($report['summary'], 0, 200) ) ; ?>]]></description>
                <pubDate><?php echo  $report['published_date'];?></pubDate>
            </item>  
        <?php endwhile; ?>
        <!-- Reports End -->
        </admin:generatorAgent>

    </channel>

</rss>
