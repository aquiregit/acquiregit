<?php
  ini_set('max_execution_time', 900);
    include 'connect.php';
  $base_url = "https://www.acquiremarketresearch.com/";

  $qumc="SELECT * FROM reports WHERE catlog='Medical Devices & Consumables'";
      $resultmc=$con->query($qumc);
      $timc="";
      if($resultmc->num_rows > 0)
      {
        $xml=new DOMDocument("1.0","utf-8");
        $xml->formatOutput=true;
            /*urlset*/
          $urlset=$xml->createElement("urlset");
          $xml->appendChild($urlset);
          $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
            /*index.php*/
          $url=$xml->createElement("url");
          $urlset->appendChild($url);
          $loc=$xml->createElement("loc",$base_url);
          $url->appendChild($loc);
          $prt=$xml->createElement("priority","1.0");
          $url->appendChild($prt);
          while($rowmc = $resultmc->fetch_assoc()) 
         {
            $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowmc['report_title']),8)));
            $urltitle= str_replace("--","-",$urltitle);
           $urltitle= str_replace("---","-",$urltitle);
             $urltitle= str_replace("(", "-",$urltitle);
              $urltitle= str_replace(")", "-",$urltitle);
              $urltitle= str_replace("?", "-",$urltitle);
                                    $urltitle= str_replace("!", "-",$urltitle);
                                    $urltitle= str_replace("@", "-",$urltitle);
                                    $urltitle= str_replace("#", "-",$urltitle);
                                    $urltitle= str_replace("$", "-",$urltitle);
                                    $urltitle= str_replace("%", "-",$urltitle);
                                    $urltitle= str_replace("^", "-",$urltitle);
                                    $urltitle= str_replace("*", "-",$urltitle);
                                    $urltitle= str_replace("'", "-",$urltitle);
                                    $urltitle= str_replace("+", "-",$urltitle);
                                    $urltitle= str_replace("&","and",$urltitle);
                                    $urltitle=str_replace("/","-",$urltitle);
                                    $urltitle=str_replace(":","-",$urltitle);
                                    $urltitle=str_replace(".","-",$urltitle);
                                    $urltitle=str_replace("<","-",$urltitle);
                                    $urltitle=str_replace(">","-",$urltitle);
                                    $urltitle=str_replace("'","-",$urltitle);
                                    $urltitle=str_replace("|","-",$urltitle);
                                    $urltitle=str_replace("]","-",$urltitle);
                                    $urltitle=str_replace("[","-",$urltitle);
                                    $urltitle=str_replace("}","-",$urltitle);
                                    $urltitle=str_replace("{","-",$urltitle);
                                    $urltitle=str_replace(";","-",$urltitle);
                                    $urltitle=str_replace("_","-",$urltitle);
                                    $urltitle=str_replace("_x000D_","-",$urltitle);
                                    $urltitle= str_replace("--","-",$urltitle);
                                    $urltitle1= str_replace("---","-",$urltitle);
                                     $reportmc=$xml->createElement("url");
                                      $urlset->appendChild($reportmc);
                                     $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowmc['report_id']);
                                     $reportmc->appendChild($loc);
                     
                                     $prt=$xml->createElement("priority","0.9");
                                     $reportmc->appendChild($prt);
                                     $timc=str_replace("&","and", $rowmc['catlog']);
                                      $timc=str_replace(" ","-", $timc);
                                      $timc=strtolower($timc);
                                     
         }
 $xml->save($timc."-market-reports.xml");
      } 
         function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
}
?>