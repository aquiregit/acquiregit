<?php 
ini_set("display_errors", "1");
error_reporting(E_ALL);

include 'connect.php';
$query= "SELECT * FROM reports WHERE slug = '' OR slug IS null OR slug = ' '";
$result = $con->query($query);
// $row['report_title'] = "global test !\"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~ test — andrés – - test market";
$counter = 0;
if ($result->num_rows > 0) {
    $baseUrl   = "https://www.acquiremarketresearch.com/";
    $reportPath = "industry-reports/";
    
    while($row = $result->fetch_assoc()) {
        $title = create_slug($row['report_title']);

        $slug="".$baseUrl."".$reportPath."".$title."/".$row['report_id']."/";         
        $query1=mysqli_query($con,"UPDATE `reports` SET `slug`='".$slug."' WHERE `report_id`=".$row['report_id']."");
    
        if($query1) {
            $counter++;
        }
    }
}

echo "<strong>Slug generated for $counter reports.</strong>";

function create_slug($string) {
   $string = normalize($string);
   $string = limit_slug($string);
   $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   $slug = trim($slug, '-');
   $slug = preg_replace('~-+~', '-', $slug);
   return $slug;
}
function limit_slug($string) {
   return trim(strstr( str_replace( 'global', '', strtolower( trim($string) ) ), "market", true) . "market");
}
function normalize ($string) {
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
    );
    
    return strtr($string, $table);
}
?>