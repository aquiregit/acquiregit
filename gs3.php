<?php
   include 'connect.php';
   
   $base_url = "https://www.acquiremarketresearch.com/";

  /*category.php*/
   $qu="SELECT * FROM categories";
   $result=$con->query($qu);
   
        if ($result->num_rows > 0) 
        {                
          while($row = $result->fetch_assoc()) 
          {
               switch ($row['cat_name']) 
               {

                  case 'Agriculture': $sqlagri="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                                       $resultagri=$con->query($sqlagri);
                                      $tiagri="";
                                      if ($resultagri->num_rows > 0) 
                                      {
                                          $xml=new DOMDocument("1.0","utf-8");
                                          $xml->formatOutput=true;
                                               /*urlset*/
                                           $urlset=$xml->createElement("urlset");
                                           $xml->appendChild($urlset);
                                           $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
                                                        /*index.php*/
                                           $url=$xml->createElement("url");
                                           $urlset->appendChild($url);
                                           $loc=$xml->createElement("loc",$base_url);
                                           $url->appendChild($loc);
                                           $prt=$xml->createElement("priority","1.0");
                                           $url->appendChild($prt);
                     
                                        while($rowagri = $resultagri->fetch_assoc()) 
                                        {
                                          $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowagri['report_title']),8)));
                                          $urltitle= str_replace("(", "-",$urltitle);
                                          $urltitle= str_replace(")", "-",$urltitle);
                                          $urltitle= str_replace("?", "-",$urltitle);
                                          $urltitle= str_replace("!", "-",$urltitle);
                                          $urltitle= str_replace("@", "-",$urltitle);
                                          $urltitle= str_replace("#", "-",$urltitle);
                                          $urltitle= str_replace("$", "-",$urltitle);
                                          $urltitle= str_replace("%", "-",$urltitle);
                                          $urltitle= str_replace("^", "-",$urltitle);
                                          $urltitle= str_replace("*", "-",$urltitle);
                                          $urltitle= str_replace("'", "-",$urltitle);
                                          $urltitle= str_replace("+", "-",$urltitle);
                                          $urltitle= str_replace("&","and",$urltitle);
                                          $urltitle=str_replace("/","-",$urltitle);
                                          $urltitle=str_replace(":","-",$urltitle);
                                          $urltitle=str_replace(".","-",$urltitle);
                                          $urltitle=str_replace("<","-",$urltitle);
                                          $urltitle=str_replace(">","-",$urltitle);
                                          $urltitle=str_replace("'","-",$urltitle);
                                          $urltitle=str_replace("|","-",$urltitle);
                                          $urltitle=str_replace("]","-",$urltitle);
                                          $urltitle=str_replace("[","-",$urltitle);
                                          $urltitle=str_replace("}","-",$urltitle);
                                          $urltitle=str_replace("{","-",$urltitle);
                                          $urltitle=str_replace(";","-",$urltitle);
                                          $urltitle=str_replace("_","-",$urltitle);
                                          $urltitle=str_replace("_x000D_","-",$urltitle);
                                          
                                          $urltitle= str_replace("--","-",$urltitle);
                                          $urltitle1= str_replace("---","-",$urltitle);
                                          
                                          $report=$xml->createElement("url");
                                         $urlset->appendChild($report);
                                         $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowagri['report_id']);
                                         $report->appendChild($loc);
                         
                                         $prt=$xml->createElement("priority","0.9");
                                         $report->appendChild($prt);
                                         $tiagri=str_replace("&","and", $rowagri['catlog']);
                                          $tiagri=str_replace(" ","-", $tiagri);
                                          $tiagri=strtolower($tiagri);
                                     if($rowagri['catlog']=='Agriculture')            
                                             $xml->save($tiagri."-market-reports.xml");
                                        }
                                      }
                                      break;

                
            case 'Machinery & Equipment':$qume="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
                      $resultme=$con->query($qume);
                      $time="";
                    if ($resultme->num_rows > 0) 
                    {     $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);
              
                       while($rowme = $resultme->fetch_assoc()) 
                       {
                        $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowme['report_title']),8)));
                        $urltitle= str_replace("(", "-",$urltitle);
                        $urltitle= str_replace(")", "-",$urltitle);
                        $urltitle= str_replace("?", "-",$urltitle);
                        $urltitle= str_replace("!", "-",$urltitle);
                        $urltitle= str_replace("@", "-",$urltitle);
                        $urltitle= str_replace("#", "-",$urltitle);
                        $urltitle= str_replace("$", "-",$urltitle);
                        $urltitle= str_replace("%", "-",$urltitle);
                        $urltitle= str_replace("^", "-",$urltitle);
                        $urltitle= str_replace("*", "-",$urltitle);
                        $urltitle= str_replace("'", "-",$urltitle);
                        $urltitle= str_replace("+", "-",$urltitle);
                        $urltitle= str_replace("&","and",$urltitle);
                        $urltitle=str_replace("/","-",$urltitle);
                        $urltitle=str_replace(":","-",$urltitle);
                        $urltitle=str_replace(".","-",$urltitle);
                        $urltitle=str_replace("<","-",$urltitle);
                        $urltitle=str_replace(">","-",$urltitle);
                        $urltitle=str_replace("'","-",$urltitle);
                        $urltitle=str_replace("|","-",$urltitle);
                        $urltitle=str_replace("]","-",$urltitle);
                        $urltitle=str_replace("[","-",$urltitle);
                        $urltitle=str_replace("}","-",$urltitle);
                        $urltitle=str_replace("{","-",$urltitle);
                        $urltitle=str_replace(";","-",$urltitle);
                        $urltitle=str_replace("_","-",$urltitle);
                        $urltitle=str_replace("_x000D_","-",$urltitle);
                        
                       
                
                        $urltitle= str_replace("--","-",$urltitle);
                        $urltitle1= str_replace("---","-",$urltitle);
                        
                         $reportme=$xml->createElement("url");
                         $urlset->appendChild($reportme);
                         $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowme['report_id']);
                         $reportme->appendChild($loc);
         
                         $prt=$xml->createElement("priority","0.9");
                         $reportme->appendChild($prt);
                         $time=str_replace("&","and", $rowme['catlog']);
                         $time=str_replace(" ","-", $time);
                         $time=strtolower($time);
                          if($rowme['catlog']=='Machinery & Equipment')            
                                  $xml->save($time."-market-reports.xml");
                       }
                    }
                                          break;
              
            case 'Service & Software':$quss="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
               $resultss=$con->query($quss);
                $tiss="";
              if ($resultss->num_rows > 0) 
              {    $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);            
                 while($rowss = $resultss->fetch_assoc()) 
                 {
                    $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowss['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);

                    $reportss=$xml->createElement("url");
                   $urlset->appendChild($reportss);
                   $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowss['report_id']);
                   $reportss->appendChild($loc);
   
                   $prt=$xml->createElement("priority","0.9");
                   $reportss->appendChild($prt);
                    $tiss=str_replace("&","and", $rowss['catlog']);
                    $tiss=str_replace(" ","-", $tiss);
                    $tiss=strtolower($tiss);
               if($rowss['catlog']=='Service & Software')            
                       $xml->save($tiss."-market-reports.xml");
                 }
              }
                      break;

            case 'Energy & Power':$quep="SELECT * FROM reports,categories WHERE catlog='".$row['cat_name']."' AND reports.cat_id=categories.cat_id";
               $resultep=$con->query($quep);
                $tiep="";
                if ($resultep->num_rows > 0) 
                {  
                      $xml=new DOMDocument("1.0","utf-8");
                       $xml->formatOutput=true;
                       /*urlset*/
   
                        $urlset=$xml->createElement("urlset");
                       $xml->appendChild($urlset);
                       $xml->createAttributeNS("http://www.sitemaps.org/schemas/sitemap/0.9","xmlns");
   
                       /*index.php*/
                       $url=$xml->createElement("url");
                        $urlset->appendChild($url);
                       $loc=$xml->createElement("loc",$base_url);
                       $url->appendChild($loc);
                       $prt=$xml->createElement("priority","1.0");
                       $url->appendChild($prt);              
                  while($rowep = $resultep->fetch_assoc()) 
                  {
                    $urltitle=utf8_encode(str_replace(' ','-',limit_words(strtolower($rowep['report_title']),8)));
                    $urltitle= str_replace("(", "-",$urltitle);
                    $urltitle= str_replace(")", "-",$urltitle);
                    $urltitle= str_replace("?", "-",$urltitle);
                    $urltitle= str_replace("!", "-",$urltitle);
                    $urltitle= str_replace("@", "-",$urltitle);
                    $urltitle= str_replace("#", "-",$urltitle);
                    $urltitle= str_replace("$", "-",$urltitle);
                    $urltitle= str_replace("%", "-",$urltitle);
                    $urltitle= str_replace("^", "-",$urltitle);
                    $urltitle= str_replace("*", "-",$urltitle);
                    $urltitle= str_replace("'", "-",$urltitle);
                    $urltitle= str_replace("+", "-",$urltitle);
                    $urltitle= str_replace("&","and",$urltitle);
                    $urltitle=str_replace("/","-",$urltitle);
                    $urltitle=str_replace(":","-",$urltitle);
                    $urltitle=str_replace(".","-",$urltitle);
                    $urltitle=str_replace("<","-",$urltitle);
                    $urltitle=str_replace(">","-",$urltitle);
                    $urltitle=str_replace("'","-",$urltitle);
                    $urltitle=str_replace("|","-",$urltitle);
                    $urltitle=str_replace("]","-",$urltitle);
                    $urltitle=str_replace("[","-",$urltitle);
                    $urltitle=str_replace("}","-",$urltitle);
                    $urltitle=str_replace("{","-",$urltitle);
                    $urltitle=str_replace(";","-",$urltitle);
                    $urltitle=str_replace("_","-",$urltitle);
                    $urltitle=str_replace("_x000D_","-",$urltitle);
                    
                   
            
                    $urltitle= str_replace("--","-",$urltitle);
                    $urltitle1= str_replace("---","-",$urltitle);
                    
                    $reportep=$xml->createElement("url");
                    $urlset->appendChild($reportep);
                    $loc=$xml->createElement("loc",$base_url."industry-reports/".$urltitle1."/".$rowep['report_id']);
                    $reportep->appendChild($loc);
   
                    $prt=$xml->createElement("priority","0.9");
                    $reportep->appendChild($prt);
                    $tiep=str_replace("&","and", $rowep['catlog']);
                    $tiep=str_replace(" ","-", $tiep);
                    $tiep=strtolower($tiep);
                    if($rowep['catlog']=='Energy & Power')            
                         $xml->save($tiep."-market-reports.xml");

                  }
                }
                      break;
          }
        }

}
function limit_words($string, $word_limit)
       {
           $words=str_replace(","," ",$string);  
           $words = explode(" ",$words);
       
           return implode(" ", array_splice($words, 0, $word_limit));
       }
?>