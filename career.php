<?php 
    $pagetitle="Careers opportunities | Acquire Market Research
    ";
    $desc="Acquire Market Research warmly welcomes talent aboard to mold careers with us. We aim at building innovative teams who strive to provide 24/7 assistance to our clients.
    ";
    $key="Acquire career, business research career, research analyst, business research jobs, research consulting careers 
    ";
    include 'header_file.php';?>
<style type="text/css">
    #car_page p
    {
    font-family: 'Lato';
    font-size: 1.5em;
    }
</style>
<div class="container" id="car_page">
    <br>
    <div class="container col-md-12">
        <br>
    </div>
    <p class="center">
        <span style="color:#0077b5;font-size:30px;font-weight:bold;" >Careers</span>
    </p>
    <br>
    <p>
        We at Acquire warmly welcome talent aboard to mold careers with us. We aim at building innovative teams who strive to provide 24/7 assistance to our clients.
    </p>
    <p>
        We are a team of smart, talented and riveting folks. But what really makes Acquire special is the willingness to do our best at every engagement.
    </p>
    <p>
        We have worked hard to get where we are and if you believe you are excited about your career and believe in setting new records every day, working towards perfection, then we would love to hear from you to select from the plethora of opportunities we have that we would want you to make the most of.
    </p>
    <p>
        Inquisitive candidates kindly mail us your bio-data at <a href="mailto:careers@acquiremarketresearch.com" style="color: blue;">careers@acquiremarketresearch.com.</a>
        We look forward to hearing from you and having you share our success story with us.
    </p>
</div>
<div class="container">
    <br><br>
</div>
<div class="container">
    <br><br>
</div>
<?php include 'footer_file.php';?>