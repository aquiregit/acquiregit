<?php
  
    $pagetitle="Thank You | Acquire Market Research";
    $key="Thank You";
    $desc="Acquire would like to take this opportunity to thank each and every people associated with us.";
    ?>
    
<?php include 'header_file.php';?>
<style type="text/css">
  #reqdiv{margin-top:50px}#reqtext{font-size:1.5em}#tcon{margin-bottom:20px}
</style>
<div class="row"></div>
<div class="container center" id="reqdiv">
    <h2>Thank you for your interest.</h2>
    <h3><?php echo ucwords($_POST['billing_name']) ;?></h3>
    <?php
        $finalstrrstempby=utf8_encode(str_replace(' ','-',limit_words(strtolower($_POST['report']),8)));
        $finalstrrstempby= str_replace("(", "-", $finalstrrstempby);
        $finalstrrstempby= str_replace(")", "-", $finalstrrstempby);
        $finalstrrstempby= str_replace("?", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("&", "and",  $finalstrrstempby);
        $finalstrrstempby= str_replace("'", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("!", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("@", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("#", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("$", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("%", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("^", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("*", "-",  $finalstrrstempby);
        $finalstrrstempby= str_replace("+", "-",  $finalstrrstempby);
        $finalstrrstempby=str_replace("/","-", $finalstrrstempby);
        $finalstrrstempby=str_replace(":","-", $finalstrrstempby);
        $finalstrrstempby=str_replace(".","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("<","-", $finalstrrstempby);
        $finalstrrstempby=str_replace(">","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("'","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("|","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("]","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("[","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("}","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("{","-", $finalstrrstempby);
        $finalstrrstempby=str_replace(";","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("_","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("_x000D_","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("--","-", $finalstrrstempby);
        $finalstrrstempby=str_replace("---","-", $finalstrrstempby);
        
         $query2=mysqli_query($con,"SELECT * from reports where report_title ='".$_POST['report']."'");
                while($row2=mysqli_fetch_array($query2))
                {
                  $finalStrbn = "industry-reports/";
        ?>
    <p id="reqtext" class="center">
        It would be a privilege to assist you with your inquiry - <a href="<?php echo $base_url;?><?php echo $finalStrbn;?><?php echo $finalstrrstempby;?>/<?php echo $row2['report_id'];?>"><br><br>
        <b><?php $tempbn=str_replace("?"," ", $row2['report_title']); echo $tempbn=str_replace("_x000D_"," ", $row2['report_title']); ?></b></a><br><br>
        The Acquire Market Research analyst appropriate to your query will get back to you at the earliest. <br>
        In the interim, if there is anything else we can do to assist, please do not hesitate to call +1800 663 5579.<br>
    </p>
    <?php } ?>
</div>
<div class="container" id="tcon">
    <br><br>
</div>
<?php

    include "connect.php";
    $email_code='';
    if($_POST && $_POST['billing_name']!="")
    {
     $fullname=mysqli_real_escape_string($con, $_POST['billing_name']);
     $bemail=mysqli_real_escape_string($con, $_POST['billing_email']);
     $test=explode("@",$bemail);
     $test1=$test[1];
     $test1=explode(".", $test1);
     $test1=$test1[0];
     $company=mysqli_real_escape_string($con, $_POST['cname']);
     // $designation=mysqli_real_escape_string($con, $_POST['desig']);
     $country_code=mysqli_real_escape_string($con, $_POST['country_code']);
     $phone=mysqli_real_escape_string($con, $_POST['billing_tel']);
     $report_id=mysqli_real_escape_string($con, $_POST['r_id']);
     $report=mysqli_real_escape_string($con, $_POST['report']);
     $report_p=mysqli_real_escape_string($con, $_POST['report_p']);
     $payment_op=mysqli_real_escape_string($con, $_POST['payment_option']);
    
    if($test1=='gmail'||$test1=='hotmail'||$test1=='yahoomail'||$test1=='aol'||$test1=='yahoo'||$test1=='rediffmail'){
       $email_code='C2';
      
    }else
    $email_code='C1';
    
    $temp=ltrim($report_id,"AMR-");
    // $sql="INSERT INTO `buy_now`(`customer_id`,`cust_fullname`, `cust_email`,`email_code`,`cust_company`, `cust_designation`,`cust_ccode`,`cust_phone`,`cust_report_id`,`cust_report`,`cust_price`,`payment_option`) VALUES (' ','".$fullname."','".$bemail."','".$email_code."','".$company."',' ','".$country_code."','".$phone."','".$report_id."','".$report."','".$report_p."','".$payment_op."')";   

    $sql="INSERT INTO `customer`(`customer_id`, `cust_fullname`, `cust_email`, `cust_company`, `cust_ccode`, `cust_phone`, `cust_report_id`, `cust_report`, `cust_price`, `payment_option`) VALUES (' ','".$fullname."','".$bemail."','".$company."','".$country_code."','".$phone."','".$report_id."','".$report."','".$report_p."','".$payment_op."')";  
    
    $query=mysqli_query($con,$sql);
  
    if($query)
    {    
     $subject='You have new purchase order for  '.$_POST['report'];
     $bname=str_replace(' ','_',$_POST['billing_name']);
     $msg="User Name: ".$_POST['billing_name']."<br><br>Business Email: ".$_POST['billing_email']."<br><br>Contact Number: ".$_POST['country_code']." ".$_POST['billing_tel']."<br><br>Company Name: ".$_POST['cname']."<br><br>Report ID : ".$_POST['r_id']."<br><br>Report Title: ".$_POST['report']."<br><br>Report Price: ".$_POST['report_p']."<br><br>Payment Option: ".$_POST['payment_option']." ";
    
      $email='sales@acquiremarketresearch.com';
      

        sendmail($email, 'User', $subject, $bname, $msg, $email);
    
        $emailid2=$_POST['billing_email'];
        $subject2='Thank you! Your order summary is as follows.';
        $bname2="sales@acquiremarketresearch.com";
       
        $amr='<a href="mailto:sales@acquiremarketresearch.com">sales@acquiremarketresearch.com</a>';
        $amr_ws='<a href="http://www.acquiremarketresearch.com">www.acquiremarketresearch.com</a>';
		$imgmail="<a href='http://www.acquiremarketresearch.com'><img src='https://www.acquiremarketresearch.com/images/230x134.png' style='float:left;'></a>";
		
        $msg2="Dear ".$fullname.",<br><br>Greetings!<br><br>Thank you for your interest in Market Research Report for <b>".$report."</b>.<br><br> This email is to confirm that your message has been received by us.<br><br>We will get back to you shortly with the sample report.<br><br>Thank You<br><br>Best regards,<br>Team AMR<br><br>".$imgmail."<br><br><b>Acquire Market Research</b><br><br>Telephone: +1800 663 5579<br>Email: ".$amr."<br>Website: ".$amr_ws."<br><br>*** This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited. Sharing the contents of this email with any third party is not allowed*** ";
        $email2='sales@acquiremarketresearch.com';
     sendmail($emailid2, 'User', $subject2, $bname2, $msg2, $email2);
    
    }    
    else
        echo "false";
    
    }
    
    function sendmail($to1,$toname1,$subject1,$from1,$text1,$frmname)
    {
    $to=$to1;
    $toname=$toname1;
    $subject=$subject1;
    $from=$from1;
    $fromname=$frmname;
    
    $url = 'https://api.sendgrid.com/';
    $user= 'acquiremarketresearch@123';
    $pass= 'acquire@123';
    
    
    $params = array(
         'api_user'  => $user,
         'api_key'   => $pass,
         'to'        => $to,
         'subject'   => $subject,
         'html'      => $text1,
         'from'      => $from
    );
    
    $request =  $url.'api/mail.send.json';
    $session = curl_init($request);
    curl_setopt ($session, CURLOPT_POST, true);
    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    }
    ?>
<?php
    function limit_words($string, $word_limit)
    {
        $words=str_replace(","," ",$string);  
        $words = explode(" ",$words);
    
        return implode(" ", array_splice($words, 0, $word_limit));
    }
    ?>
<?php include 'footer_file.php';?>