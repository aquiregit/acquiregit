<style> p {
    padding: 0;
    margin: 0;
}
channel {
    word-break: break-all;
}
</style>
<?php 
/*error_reporting(E_ALL);
ini_set("display_errors", "1");*/

// header( "Content-type: text/xml");
$base_url = "https://www.acquiremarketresearch.com/";
$feed_name = "acquiremarketresearch.com/";
$feed_url = "https://www.acquiremarketresearch.com/feed";
$page_description = "Leading provider of market research news, market research reports and industry analysis by products, markets, Regions, industries around the global.";
$page_language = "en-US";
$creator_email = "sales@acquiremarketresearch.com";
include 'connect.php';

/*Get press releases*/
$sql    = "SELECT * FROM press_releases";
$pressReleases = mysqli_query($con, $sql);

/*Get blogs*/
$sql    = "SELECT * FROM blog";
$blogs  = mysqli_query($con, $sql);

/*Get reports*/
$sql    = "SELECT report_id, report_title, published_date, summary, time FROM reports ORDER BY report_id DESC LIMIT 10000"; // limit 43, 1
$reports = mysqli_query($con, $sql);

function limit_words($string, $word_limit) {
    $words=str_replace(","," ",$string);  
    $words = explode(" ",$words);

    return implode(" ", array_splice($words, 0, $word_limit));
}
function getPRUrl($url, $id, $base_url, $pr = true) {
        $prtitle     = utf8_encode((str_replace(" ", "-", strtolower($url))));
        $prtitle1    = str_replace("&", "and", $prtitle);
        $prtitle1    = str_replace("(", "-", $prtitle1);
        $prtitle1    = str_replace(")", "-", $prtitle1);
        $prtitle1    = str_replace("?", "-", $prtitle1);
        $prtitle1    = str_replace("!", "-", $prtitle1);
        $prtitle1    = str_replace("@", "-", $prtitle1);
        $prtitle1    = str_replace("#", "-", $prtitle1);
        $prtitle1    = str_replace("$", "-", $prtitle1);
        $prtitle1    = str_replace("%", "-", $prtitle1);
        $prtitle1    = str_replace("^", "-", $prtitle1);
        $prtitle1    = str_replace("*", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("+", "-", $prtitle1);
        $prtitle1    = str_replace("/", "-", $prtitle1);
        $prtitle1    = str_replace(":", "-", $prtitle1);
        $prtitle1    = str_replace(".", "-", $prtitle1);
        $prtitle1    = str_replace("<", "-", $prtitle1);
        $prtitle1    = str_replace(">", "-", $prtitle1);
        $prtitle1    = str_replace("'", "-", $prtitle1);
        $prtitle1    = str_replace("|", "-", $prtitle1);
        $prtitle1    = str_replace("]", "-", $prtitle1);
        $prtitle1    = str_replace("[", "-", $prtitle1);
        $prtitle1    = str_replace("}", "-", $prtitle1);
        $prtitle1    = str_replace("{", "-", $prtitle1);
        $prtitle1    = str_replace(";", "-", $prtitle1);
        $prtitle1    = str_replace("_", "-", $prtitle1);
        $prtitle1    = str_replace("_x000D_", "-", $prtitle1);
        $prtitle1    = str_replace("---", "-", $prtitle1);
        $prtitle1    = str_replace("--", "-", $prtitle1);
        
        $finalStrprv = ( ( $pr == true) ? "press-release/" : "blog/" );
        return $base_url . $finalStrprv . $prtitle1 . '/' . $id . '/';
}
function getReportUrl($time, $url, $id, $base_url) {
    include_once'functions.php';
    
    $finalStr="industry-reports/";
    $urltitle = prepareSlugUrl($time, $url);
    $urltitle= str_replace(" ", "-",$urltitle);
    $urltitle= str_replace("(", "-",$urltitle);
    $urltitle= str_replace(")", "-",$urltitle);
    $urltitle= str_replace("?", "-",$urltitle);
    $urltitle= str_replace("!", "-",$urltitle);
    $urltitle= str_replace("@", "-",$urltitle);
    $urltitle= str_replace("#", "-",$urltitle);
    $urltitle= str_replace("$", "-",$urltitle);
    $urltitle= str_replace("%", "-",$urltitle);
    $urltitle= str_replace("^", "-",$urltitle);
    $urltitle= str_replace("*", "-",$urltitle);
    $urltitle= str_replace("'", "-",$urltitle);
    $urltitle= str_replace("+", "-",$urltitle);
    $urltitle= str_replace("&","and",$urltitle);
    $urltitle=str_replace("/","-",$urltitle);
    $urltitle=str_replace(":","-",$urltitle);
    $urltitle=str_replace(".","-",$urltitle);
    $urltitle=str_replace("<","-",$urltitle);
    $urltitle=str_replace(">","-",$urltitle);
    $urltitle=str_replace("'","-",$urltitle);
    $urltitle=str_replace("|","-",$urltitle);
    $urltitle=str_replace("]","-",$urltitle);
    $urltitle=str_replace("[","-",$urltitle);
    $urltitle=str_replace("}","-",$urltitle);
    $urltitle=str_replace("{","-",$urltitle);
    $urltitle=str_replace(";","-",$urltitle);
    $urltitle=str_replace("_","-",$urltitle);
    $urltitle=str_replace("_x000D_","-",$urltitle);
    
    $urltitle=str_replace("--","-",$urltitle);
    $urltitle1=str_replace("---","-",$urltitle);

    return $base_url . $finalStr . $urltitle1 . '/' . $id . '/';
}
function convert_ascii($string) { 
  $string = utf8_encode($string);
  $string = str_replace('?', '', $string);
  $string = str_replace("&", "and", $string);
  $string = str_replace("<", "", $string);
  $string = str_replace(">", "", $string);
  // Remove any non-ASCII Characters
  $string = preg_replace("/[^\x01-\x7F]/","", $string);

  return $string;
}

$data = '<?xml version="1.0" encoding="utf-8" ?>' . PHP_EOL; 
$data .= '<rss version="2.0"'. PHP_EOL;
$data .= '    xmlns:dc="http://purl.org/dc/elements/1.1/"'. PHP_EOL;
$data .= '    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"'. PHP_EOL;
$data .= '    xmlns:admin="http://webns.net/mvcb/"'. PHP_EOL;
$data .= '    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'. PHP_EOL;
$data .= '    xmlns:content="http://purl.org/rss/1.0/modules/content/">'. PHP_EOL;

$data .= '    <channel>'. PHP_EOL;

$data .= '         <title>' . $feed_name .'</title>'. PHP_EOL;
$data .= '         <link>' .$feed_url .'</link>'. PHP_EOL; ; 
$data .= '         <description>' . $page_description.'</description>  '. PHP_EOL;
$data .= '         <dc:language>' . $page_language.'</dc:language>  '. PHP_EOL;
$data .= '         <dc:creator>' . $creator_email.'></dc:creator>'. PHP_EOL;
$data .= '         <dc:rights>Copyright ' . gmdate("Y", time()) .'</dc:rights>  '. PHP_EOL;

$data .= '         <admin:generatorAgent rdf:resource="https://www.acquiremarketresearch.com/" />'. PHP_EOL;
echo '<pre>';
        $data . '<!-- Press Releases -->';
        while($pressRelease = $pressReleases->fetch_assoc()) { 
            $prUrl = getPRUrl($pressRelease['pr_title'], $pressRelease['pr_id'], $base_url);
            $data .= '              <item>'. PHP_EOL;  
    		$data .= '                <title>' . $pressRelease['pr_title'] . ' </title>'. PHP_EOL; 
    		$data .= '                <link>' . $prUrl . ' </link>'. PHP_EOL; 
    		$data .= '                <guid>' . $prUrl . ' </guid>'. PHP_EOL; 
    		$data .= '                <description><![CDATA[' . substr($pressRelease['meta_desc'], 0, 200) . ' ]]></description>'. PHP_EOL; 
    		$data .= '                <pubDate>' .  $pressRelease['published_date'] .'</pubDate>'. PHP_EOL; 
	        $data .= '              </item>'. PHP_EOL; 
        }
       /* <!-- Press Releases End -->*/

        /*<!-- Blogs Start -->*/
        while($blog = $blogs->fetch_assoc()) {
            $blogUrl = getPRUrl($blog['blog_title'], $blog['blog_id'], $base_url, false);
            $data .= '              <item>'. PHP_EOL;  
            $data .= '                   <title>' . $blog['blog_title']  . '</title>'. PHP_EOL; 
            $data .= '                   <link>' . $blogUrl . '</link>'. PHP_EOL;
            $data .= '                   <guid>' . $blogUrl . '</guid>'. PHP_EOL;
            $data .= '                   <description><![CDATA[' . substr($blog['blog_desc'], 0, 200)  . ']]></description>'. PHP_EOL;
            $data .= '                   <pubDate>' .  $blog['published_date'] . '></pubDate>'. PHP_EOL;
            $data .= '              </item>'. PHP_EOL;
        }
        /*<!-- Blogs End -->*/
       
        /*<!-- Reports Start -->*/
        while($report = $reports->fetch_assoc()) {
            $reportUrl = getReportUrl($report['time'], $report['report_title'], $report['report_id'], $base_url);
            $data .= '              <item>  '. PHP_EOL;
                $data .= '                   <title>' . convert_ascii(substr($report['report_title'], 0, 100)) . '</title> '. PHP_EOL;
                $data .= '                   <link>' . $reportUrl . '</link>'. PHP_EOL;
                $data .= '                   <guid>' . $reportUrl . '</guid>'. PHP_EOL;
                $data .= '                   <description><![CDATA[' . convert_ascii( substr($report['summary'], 0, 200) ) . ' ]]></description>'. PHP_EOL;
                $data .= '                   <pubDate>' .  $report['published_date'] . '</pubDate>'. PHP_EOL;
            $data .= '              </item>  '. PHP_EOL;
        }
        /*<!-- Reports End -->*/

    $data .= '    </channel>'. PHP_EOL;

$data .= '</rss>'. PHP_EOL;
echo htmlentities( $data );
?> 